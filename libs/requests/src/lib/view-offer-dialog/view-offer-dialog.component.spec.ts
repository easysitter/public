import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOfferDialogComponent } from './view-offer-dialog.component';

describe('ViewOfferDialogComponent', () => {
  let component: ViewOfferDialogComponent;
  let fixture: ComponentFixture<ViewOfferDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewOfferDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOfferDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
