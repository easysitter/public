import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NannyRequest } from '@es/requests';

@Component({
  selector: 'es-view-offer-dialog',
  templateUrl: './view-offer-dialog.component.html',
  styleUrls: ['./view-offer-dialog.component.css']
})
export class ViewOfferDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ViewOfferDialogComponent, 'accept' | 'refuse'>,
    @Inject(MAT_DIALOG_DATA) public nannyRequest: NannyRequest
  ) {
  }

  refuse() {
    this.closeWithResult('refuse');
  }

  accept() {
    this.closeWithResult('accept');
  }

  private closeWithResult(result: 'accept' | 'refuse') {
    this.dialogRef.close(result);
  }
}
