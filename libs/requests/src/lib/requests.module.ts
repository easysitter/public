import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestCardComponent } from './request-card/request-card.component';
import { LayoutModule } from '@es/layout';
import { HttpClientModule } from '@angular/common/http';
import { RequestCardActionsComponent } from './request-card-actions/request-card-actions.component';
import { OpenOfferDialogDirective } from './open-offer-dialog.directive';
import { CreateOfferDialogComponent } from './create-offer-dialog/create-offer-dialog.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { ReactiveFormsModule } from '@angular/forms';
import { ViewOfferDirective } from './view-offer.directive';
import { ViewOfferDialogComponent } from './view-offer-dialog/view-offer-dialog.component';

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        HttpClientModule,
        NgxMaterialTimepickerModule,
        ReactiveFormsModule
    ],
    exports: [
        RequestCardComponent,
        RequestCardActionsComponent,
        OpenOfferDialogDirective,
        ViewOfferDirective
    ],
    declarations: [
        RequestCardComponent,
        RequestCardActionsComponent,
        OpenOfferDialogDirective,
        CreateOfferDialogComponent,
        ViewOfferDirective,
        ViewOfferDialogComponent
    ]
})
export class RequestsModule {
}
