export interface ParentRequest {
  id: number;
  parent: { id: number, name: string, photo: string };
  date: Date;
  fromTime: string;
  toTime: string;
  children: number[];
  address: string;
  wishes: string;
  trial: boolean;
}
