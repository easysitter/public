import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'es-request-card-actions',
  template: `
      <div class="pt-3">
          <div class="form-row">
              <ng-content></ng-content>
          </div>
      </div>
  `,
  styles: []
})
export class RequestCardActionsComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
