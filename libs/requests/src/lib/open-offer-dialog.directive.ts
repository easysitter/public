import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  CreateOfferDialogComponent,
  CreateOfferDialogResult
} from './create-offer-dialog/create-offer-dialog.component';
import { NannyRequest } from './nanny-request';

@Directive({
  selector: '[esOpenOfferDialog]'
})
export class OpenOfferDialogDirective {

  @Input('esOpenOfferDialog') request: NannyRequest;
  @Output() offer = new EventEmitter<CreateOfferDialogResult>();

  constructor(
    private dialog: MatDialog
  ) {
  }

  @HostListener('click')
  onClick() {
    this.dialog.open<CreateOfferDialogComponent, NannyRequest, CreateOfferDialogResult>(
      CreateOfferDialogComponent,
      {
        maxWidth: '100vw',
        panelClass: ['modal-container'],
        data: this.request
      }
    )
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.offer.emit(result);
        }
      });
  }

}
