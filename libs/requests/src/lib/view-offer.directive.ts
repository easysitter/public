import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { NannyRequest } from '@es/requests';
import { MatDialog } from '@angular/material/dialog';
import { ViewOfferDialogComponent } from './view-offer-dialog/view-offer-dialog.component';

@Directive({
  selector: '[esViewOffer]'
})
export class ViewOfferDirective {

  @Input('esViewOffer') request: NannyRequest;
  @Output() action = new EventEmitter<'accept' | 'refuse'>();

  constructor(
    private dialog: MatDialog
  ) {
  }

  @HostListener('click')
  onClick() {
    this.dialog.open<ViewOfferDialogComponent, NannyRequest, 'accept' | 'refuse'>(
      ViewOfferDialogComponent,
      {
        maxWidth: '100vw',
        panelClass: ['modal-container'],
        data: this.request
      }
    )
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.action.emit(result);
        }
      });
  }
}
