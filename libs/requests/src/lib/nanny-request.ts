import { ParentRequest } from '@es/requests';

export enum NannyRequestStatus {
  New = 'new', // created
  Accepted = 'accepted', // nanny accepted
  Closed = 'closed'
}

export interface NannyRequest {
  id: string;
  status: NannyRequestStatus;
  offer?: number | any;
  rate: number;
  nanny?: any;
  request: ParentRequest
}
