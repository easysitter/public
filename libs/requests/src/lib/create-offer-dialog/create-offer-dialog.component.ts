import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NgxMaterialTimepickerTheme } from 'ngx-material-timepicker';

import { FormBuilder } from '@angular/forms';
import { ParentRequest } from '../parent-request';
import { NannyRequest } from '../nanny-request';

export interface OfferChanges {
  fromTime: ParentRequest['fromTime'],
  toTime: ParentRequest['toTime'],
  rate: NannyRequest['rate']
}

export interface CreateOfferDialogResult {
  changes: OfferChanges,
  request: NannyRequest
}

@Component({
  selector: 'es-create-offer-dialog',
  templateUrl: './create-offer-dialog.component.html',
  styleUrls: ['./create-offer-dialog.component.css']
})
export class CreateOfferDialogComponent implements OnInit {
  form = this.fb.group({
    fromTime: this.nannyRequest.request.fromTime,
    toTime: this.nannyRequest.request.toTime,
    rate: this.nannyRequest.rate
  });

  theme: NgxMaterialTimepickerTheme = {
    clockFace: {
      clockFaceBackgroundColor: 'var(--white)',
      clockHandColor: 'var(--secondary)',
      clockFaceTimeActiveColor: 'var(--white)'
      // clockFaceBackgroundColor: '#5fa1cb'
    },
    dial: {
      dialBackgroundColor: 'var(--secondary)'
    },
    container: {
      buttonColor: 'var(--secondary)'
    }
  };

  constructor(
    public dialogRef: MatDialogRef<CreateOfferDialogComponent, CreateOfferDialogResult>,
    @Inject(MAT_DIALOG_DATA) public nannyRequest: NannyRequest,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
  }

  offer() {
    const { fromTime, toTime, rate } = this.form.value;
    this.dialogRef.close({
      changes: { fromTime, toTime, rate: +rate } as OfferChanges,
      request: this.nannyRequest
    });
  }
}
