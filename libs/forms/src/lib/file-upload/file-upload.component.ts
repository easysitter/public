import {gql, Apollo} from 'apollo-angular';
import { Component, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { exhaustMap, pluck, tap } from 'rxjs/operators';

import { Subject } from 'rxjs';

@Component({
  selector: 'es-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: FileUploadComponent, multi: true }
  ],
  exportAs: 'fileUpload'
})
export class FileUploadComponent implements ControlValueAccessor, OnInit {
  fileUrl: string;
  isDisabled = false;
  private change: any;
  private touched: any;
  private loading = false;

  private uploadSubject = new Subject();

  constructor(
    private apollo: Apollo
  ) {
  }

  ngOnInit() {
    this.uploadSubject.pipe(
      tap(console.log),
      tap(() => this.loading = true),
      exhaustMap(({ file, type }) => this.upload(file, type))
    ).subscribe(
      (value) => {
        console.log(value);
        this.loading = false;
        if (this.change) {
          this.change(value);
        }
      }
    );
  }

  isLoading() {
    return this.loading;
  }

  registerOnChange(fn: any): void {
    this.change = fn;
  }

  registerOnTouched(fn: any): void {
    this.touched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.fileUrl = obj;
  }

  uploadDocument(event) {
    console.log(event);
    event.preventDefault();
    event.stopPropagation();
    const { target } = event as { target: HTMLInputElement };
    const { validity, files } = target;
    if (!validity.valid) return null;
    if (!files.length) return null;
    const file = files.item(0);
    this.uploadSubject.next({ file, type: 'file' });
    // this.upload(file, 'file').subscribe((src) => {
    //   if (this.change) {
    //     this.change(src);
    //   }
    // });
  }

  private upload(file: File, resourceType: string) {
    return this.apollo.mutate({
      mutation: gql`mutation upload($file: Upload!, $resourceType: String) {
        uploadFile(file: $file, resourceType: $resourceType) {
          public_url
        }
      }`,
      variables: { file, resourceType },
      context: {
        useMultipart: true
      }
    }).pipe(pluck('data', 'uploadFile', 'public_url'));
  }
}
