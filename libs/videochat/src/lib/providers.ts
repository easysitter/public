import { InjectionToken } from '@angular/core';

export const OPENTOK_API_KEY = new InjectionToken<string>('OPENTOK_API_KEY');
