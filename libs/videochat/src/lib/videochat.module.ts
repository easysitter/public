import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideochatContainerComponent } from './videochat-container/videochat-container.component';
import { OPENTOK_API_KEY } from './providers';
import { VideochatPublisherComponent } from './videochat-publisher/videochat-publisher.component';
import { VideochatSubscriberComponent } from './videochat-subscriber/videochat-subscriber.component';

@NgModule({
  imports: [CommonModule],
  declarations: [VideochatContainerComponent, VideochatPublisherComponent, VideochatSubscriberComponent],
  exports: [VideochatContainerComponent]
})
export class VideochatModule {
  static forRoot(apiKey: string): ModuleWithProviders<VideochatModule> {
    return {
      ngModule: VideochatModule,
      providers: [
        { provide: OPENTOK_API_KEY, useValue: apiKey }
      ]
    };
  }
}
