import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideochatPublisherComponent } from './videochat-publisher.component';

describe('VideochatPublisherComponent', () => {
  let component: VideochatPublisherComponent;
  let fixture: ComponentFixture<VideochatPublisherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VideochatPublisherComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideochatPublisherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
