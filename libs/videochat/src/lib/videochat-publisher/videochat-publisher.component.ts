import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges
} from '@angular/core';
import { initPublisher } from '@opentok/client';

@Component({
  selector: 'videochat-videochat-publisher',
  templateUrl: './videochat-publisher.component.html',
  styleUrls: ['./videochat-publisher.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideochatPublisherComponent implements OnChanges {
  @Input() session: OT.Session;

  @Output() errored = new EventEmitter<OT.OTError>();

  constructor(
    private elementRef: ElementRef
  ) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('session' in changes && this.session) {
      this.initPublisher();
    }
  }

  private initPublisher() {
    const otPublisher = initPublisher(this.elementRef.nativeElement, {
      insertMode: 'append',
      width: '100%',
      height: '100%',
      insertDefaultUI: true,
      fitMode: 'contain'
    }, err => {
      if (err) {
        this.errored.next(err);
      }
    });
    this.session.publish(otPublisher, err => {
      if (err) {
        this.errored.next(err);
      }
    });
  }
}
