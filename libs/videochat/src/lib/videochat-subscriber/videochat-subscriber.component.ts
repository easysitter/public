import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'videochat-videochat-subscriber',
  template: `{{ message }}`,
  styleUrls: ['./videochat-subscriber.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VideochatSubscriberComponent implements OnChanges {
  @Input() session: OT.Session;
  @Output() errored = new EventEmitter();
  @Output() setup = new EventEmitter<void>();
  message = 'Ожидание';

  constructor(
    private elRef: ElementRef,
    private zone: NgZone,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if ('session' in changes && this.session) {
      this.init();
    }
  }

  private init() {
    this.zone.runOutsideAngular(() => {
      console.log({ subscriberSession: this.session });

      this.session.on('streamDestroyed', () => {
        this.zone.run(() => {
          this.message = 'Неполадки в соединении';
          this.cdr.markForCheck();
        });
      });
      this.session.on('streamCreated', (event) => {
        console.log({
          eventName: 'streamCreated',
          event,
        });
        this.message = 'Подключение';
        this.cdr.markForCheck();
        this.session.subscribe(
          event.stream,
          this.elRef.nativeElement,
          {
            insertMode: 'append',
            width: '100%',
            height: '100%',
            fitMode: 'contain',
          },
          (err) => {
            console.log({
              eventName: 'subscribeStream',
            });
            this.zone.run(() => {
              this.message = '';
              if (err) {
                this.errored.next(err);
                this.message = err.message;
              }
              this.cdr.markForCheck();
            });
          }
        );
      });
      this.setup.next();
    });
  }
}
