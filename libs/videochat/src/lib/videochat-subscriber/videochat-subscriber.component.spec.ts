import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideochatSubscriberComponent } from './videochat-subscriber.component';

describe('VideochatSubscriberComponent', () => {
  let component: VideochatSubscriberComponent;
  let fixture: ComponentFixture<VideochatSubscriberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VideochatSubscriberComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideochatSubscriberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
