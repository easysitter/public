import { Inject, Injectable, NgZone } from '@angular/core';
import { initSession } from '@opentok/client';
import { Observable } from 'rxjs';
import { OPENTOK_API_KEY } from './providers';

@Injectable({
  providedIn: 'root',
})
export class OpentokService {
  constructor(
    @Inject(OPENTOK_API_KEY)
    private apiKey: string,
    private zone: NgZone
  ) {}

  initSession(sessionId) {
    return initSession(this.apiKey, sessionId);
  }

  connect(session: OT.Session, token: string): Observable<boolean> {
    return new Observable((observer) => {
      this.zone.runOutsideAngular(() => {
        session.connect(token, (err) => {
          this.zone.run(() => {
            if (err) {
              observer.error(err);
            } else {
              observer.next(true);
            }
          });
        });
        session.on('signal:ended', (e) => {
          this.zone.run(() => {
            observer.complete();
          });
        });
        session.on('signal:terminated', (e) => {
          this.zone.run(() => {
            observer.next(false);
          });
        });
      });
      return () => {
        session.disconnect();
      };
    });
  }

  disconnect(session: OT.Session) {
    if (!session.connection) return;
    session.signal(
      {
        type: 'ended',
      },
      () => {}
    );
  }
}
