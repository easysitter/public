import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideochatContainerComponent } from './videochat-container.component';

describe('VideochatContainerComponent', () => {
  let component: VideochatContainerComponent;
  let fixture: ComponentFixture<VideochatContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VideochatContainerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideochatContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
