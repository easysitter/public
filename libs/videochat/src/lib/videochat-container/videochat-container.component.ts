import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { OpentokService } from '../opentok.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'videochat-container',
  templateUrl: './videochat-container.component.html',
  styleUrls: ['./videochat-container.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VideochatContainerComponent implements OnInit, OnChanges {
  @Input() session: string;
  @Input() token: string;
  @Output() callEnded = new EventEmitter();

  endCall = new Subject();

  otSession: OT.Session;
  error: string;
  connected = false;
  terminated = false;

  constructor(private ot: OpentokService, private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.endCall.subscribe(() => {
      this.callEnded.emit();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.session && this.token) {
      this.otSession = this.ot.initSession(this.session);
    }
  }

  destroy() {
    this.ot.disconnect(this.otSession);
  }

  handlePublisherError($event: OT.OTError) {
    this.error = $event.message;
    alert(this.error);
    this.destroy();
  }

  connect(session) {
    console.log({ connect: session });

    this.ot.connect(this.otSession, this.token).subscribe({
      next: (value) => {
        if (value) this.connected = true;
        else this.terminated = true;
        this.cdr.markForCheck();
      },
      error: (err: OT.OTError) => {
        this.error = err.message;
        alert(this.error);
      },
      complete: () => this.endCall.next(true),
    });
  }
}
