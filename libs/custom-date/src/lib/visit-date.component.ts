import { Component, Input } from '@angular/core';

@Component({
  selector: 'es-visit-date',
  template: `{{date | friendlyDate:'d LLLL, EEEE'}}`,
  styles: [`
    :host {
      display: inline-block;
    }

    :host::first-letter {
      text-transform: capitalize;
    }
  `]
})
export class VisitDateComponent {

  @Input() date: any;

}
