import { format, parse } from "date-fns";

export function formatTime(time: string, date: string | Date) {
  if (time.includes("+") || time.endsWith('Z')) {
    return format(
      parse(time, "HH:mmX", new Date(date)),
      "HH:mm"
    );
  } else {
    return time;
  }
}
