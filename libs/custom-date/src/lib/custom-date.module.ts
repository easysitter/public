import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FriendlyDatePipe } from './friendly-date.pipe';
import { VisitDateComponent } from './visit-date.component';

@NgModule({
  imports: [CommonModule],
  declarations: [FriendlyDatePipe, VisitDateComponent],
  exports: [FriendlyDatePipe, VisitDateComponent]
})
export class CustomDateModule {
}
