import { Injectable } from '@angular/core';
import { merge, ReplaySubject, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  mapTo,
  pluck,
  scan,
  startWith,
} from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  readonly history = new ReplaySubject<History>();
  private goBack = new Subject<void>();

  constructor(
    private readonly router: Router,
    private readonly location: Location
  ) {
    merge(
      this.router.events.pipe(
        filter((event) => event instanceof NavigationEnd),
        pluck('urlAfterRedirects'),
        startWith(this.location.path()),
        map((url: string) => ({ direction: 'forward' as const, url }))
      ),
      this.goBack.pipe(mapTo({ direction: 'backward' as const }))
    )
      .pipe(
        filter((history) => {
          if (history.direction === 'backward') return true;
          return !history.url.includes('login');
        }),
        scan(
          (
            history: History,
            {
              direction,
              url,
            }: { direction: 'backward' | 'forward'; url?: string }
          ) => {
            if (direction === 'forward') {
              return history.forward(url);
            } else {
              return history.backward();
            }
          },
          new History(this.router)
        ),
        distinctUntilChanged(
          (x: History, y: History) => x.toArray() === y.toArray()
        )
      )
      .subscribe(this.history);
  }

  back() {
    this.goBack.next();
  }
}

class History {
  private stack: string[] = [];

  constructor(private router: Router) {
    const savedStack = sessionStorage.getItem('history_stack');
    if (savedStack) {
      this.stack = JSON.parse(savedStack);
    }
  }

  isEmpty(): boolean {
    return (
      this.stack.length === 0 ||
      (this.stack.length === 1 &&
        ['/requests', '/search'].includes(this.stack[0]))
    );
  }

  forward(url) {
    const logdata = { url, empty: this.isEmpty(), current: this.current() };
    this.log(`before forward ${JSON.stringify(logdata)}`);
    if (!this.isCurrent(url)) {
      this.stack = [...this.stack, url];
    }
    this.log(`after forward ${JSON.stringify(logdata)}`);
    this.save();
    return this;
  }

  backward() {
    if (!this.isEmpty()) {
      this.stack = this.stack.splice(0, this.stack.length - 1);
      this.router.navigateByUrl(this.current());
    }
    this.log();
    this.save();
    return this;
  }

  toArray() {
    return [...this.stack];
  }

  private log(label?: string) {
    // console.group('History');
    // console.log(label, this.stack);
    // console.groupEnd();
  }

  private isCurrent(url) {
    return this.stack.length ? this.current() === url : false;
  }

  private current(): string {
    return this.stack[this.stack.length - 1];
  }

  private save() {
    sessionStorage.setItem('history_stack', JSON.stringify(this.stack));
  }
}
