import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'es-info-block',
  template: '<ng-content></ng-content>',
  styles: [
      `:host {
          display: block
      }`
  ]
})
export class InfoBlockComponent implements OnInit {

  constructor(
    private renderer: Renderer2,
    private elRef: ElementRef
  ) {
  }

  ngOnInit() {
    this.renderer.addClass(this.elRef.nativeElement, 'info-block');
  }

}
