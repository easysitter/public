import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'es-tiles',
  template: `
    <ng-content></ng-content>`,
  styles: []
})
export class TilesComponent implements OnInit {
  @HostBinding('class.tiles')
  readonly classTiles = true;

  constructor() {
  }

  ngOnInit() {
  }

}
