import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'es-tiles-item',
  template: '<div class="inner"><ng-content></ng-content></div>'
})
export class TilesItemComponent implements OnInit {
  @HostBinding('class.tiles-item')
  readonly classTilesItem = true;

  constructor() {
  }

  ngOnInit() {
  }

}
