import { Directive, forwardRef, HostBinding, HostListener } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  selector: '[esTileSelectable]',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => TileSelectableDirective), multi: true }
  ]
})
export class TileSelectableDirective {
  selected = false;
  private onChange;
  private onTouched;

  @HostBinding('class.selectable')
  get classSelectable() {
    return true;
  }

  @HostBinding('class.selected')
  get classSelected() {
    return this.selected;
  }

  @HostBinding('class.inactive')
  get classInactive() {
    return !this.selected;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: any): void {
    this.selected = obj;
  }

  @HostListener('click')
  onClick() {
    this.selected = !this.selected;
    this.onTouched();
    this.onChange(this.selected);
  }
}
