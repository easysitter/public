import { ElementRef, Inject, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class LayoutService {
  private renderer: Renderer2;

  constructor(
    private _renderer: RendererFactory2,
    @Inject(DOCUMENT) document: Document
  ) {
    this.renderer = _renderer.createRenderer(document.body, null);
  }

  makeScreen(elRef: ElementRef) {
    ['flex', 'flex-column', 'f-grow-1'].forEach(c => this.renderer.addClass(elRef.nativeElement, c));
  }
}
