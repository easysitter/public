import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoBlockComponent } from './info-block/info-block.component';
import { FlexDirective } from './flex.directive';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { LayoutService } from './layout.service';
import { RouterModule } from '@angular/router';
import { TilesComponent } from './tiles/tiles/tiles.component';
import { TilesItemComponent } from './tiles/tiles-item/tiles-item.component';
import { TileSelectableDirective } from './tiles/tile-selectable.directive';

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [
    CommonModule,
    InfoBlockComponent,
    FlexDirective,
    LayoutComponent,
    HeaderComponent,
    TilesComponent,
    TilesItemComponent,
    TileSelectableDirective
  ],
  declarations: [
    InfoBlockComponent,
    FlexDirective,
    LayoutComponent,
    HeaderComponent,
    TilesComponent,
    TilesItemComponent,
    TileSelectableDirective
  ],
  providers: [
    LayoutService
  ]
})
export class LayoutModule {
}
