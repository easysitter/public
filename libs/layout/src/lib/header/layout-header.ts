export abstract class LayoutHeader {
  abstract openNav(): void;

  abstract closeNav(): void;

  abstract showBack(flag: boolean): void;
}
