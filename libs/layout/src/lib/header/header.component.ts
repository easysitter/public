import {
  Component,
  forwardRef,
  Inject,
  OnInit,
  Renderer2,
} from '@angular/core';
import { DOCUMENT, Location } from '@angular/common';
import { BehaviorSubject, combineLatest, merge, Subject } from 'rxjs';
import { filter, map, mapTo } from 'rxjs/operators';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { NavigationService } from '../navigation.service';
import { LayoutHeader } from './layout-header';
import { HEADER } from '../tokens';

@Component({
  selector: 'es-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [
    { provide: HEADER, useExisting: forwardRef(() => HeaderComponent) },
  ],
  exportAs: 'layoutHeader',
})
export class HeaderComponent implements OnInit, LayoutHeader {
  private navOn = new Subject();

  public readonly hasHistory = this.navigation.history.pipe(
    map((history) => !history.isEmpty() && history.toArray().length > 1)
  );

  public readonly showBackButtonSubject = new BehaviorSubject(false);

  public readonly showBackButton = combineLatest([
    this.hasHistory,
    this.showBackButtonSubject,
  ]).pipe(map(([hasHistory, showBack]) => hasHistory && showBack));

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly renderer: Renderer2,
    private readonly location: Location,
    private readonly navigation: NavigationService,
    @Inject(DOCUMENT) private readonly document: Document
  ) {}

  ngOnInit() {
    merge(
      this.navOn,
      this.router.events.pipe(
        filter((event) => event instanceof NavigationStart),
        mapTo(false)
      )
    ).subscribe((navOn) => {
      if (navOn) {
        this.renderer.addClass(this.document.documentElement, 'nav-on');
      } else {
        this.renderer.removeClass(this.document.documentElement, 'nav-on');
      }
    });
  }

  openNav() {
    this.navOn.next(true);
  }

  closeNav() {
    this.navOn.next(false);
  }

  showBack(flag: boolean) {
    this.showBackButtonSubject.next(flag);
  }

  back() {
    this.navigation.back();
    // this.goBack.next();
    // // this.location.back();
  }
}
