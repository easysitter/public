import {
  Component,
  ContentChild,
  ElementRef,
  forwardRef,
  OnInit,
} from '@angular/core';
import { HEADER, LAYOUT } from '../tokens';
import { LayoutHeader, LayoutService } from '@es/layout';
import { Layout } from './layout';
import { HeaderComponent } from '../header/header.component';

@Component({
  selector: 'es-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
  providers: [
    { provide: LAYOUT, useExisting: forwardRef(() => LayoutComponent) },
    { provide: HEADER, useExisting: forwardRef(() => LayoutComponent) },
  ],
})
export class LayoutComponent implements OnInit, Layout, LayoutHeader {
  @ContentChild(HeaderComponent) header: LayoutHeader;
  isDarkTheme = false;

  constructor(private service: LayoutService) {}

  ngOnInit() {}

  darkThemeOn() {
    this.isDarkTheme = true;
  }

  darkThemeOff() {
    this.isDarkTheme = false;
  }

  addPageClasses(elementRef: ElementRef): void {
    this.service.makeScreen(elementRef);
  }

  closeNav(): void {
    this.header.closeNav();
  }

  openNav(): void {
    this.header.openNav();
  }

  showBack(flag: boolean): void {
    this.header.showBack(flag);
  }
}
