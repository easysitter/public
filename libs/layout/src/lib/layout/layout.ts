import { ElementRef } from '@angular/core';

export abstract class Layout {
  abstract darkThemeOn(): void

  abstract darkThemeOff(): void

  abstract addPageClasses(elementRef: ElementRef): void
}
