import { InjectionToken } from '@angular/core';
import { Layout } from './layout/layout';
import { LayoutHeader } from './header/layout-header';

export const LAYOUT = new InjectionToken<Layout>('LAYOUT');

export const HEADER = new InjectionToken<LayoutHeader>('LAYOUT_HEADER');
