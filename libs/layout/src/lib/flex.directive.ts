import { Directive, ElementRef, Input, OnChanges, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[esFlex],[flex]'
})
export class FlexDirective implements OnChanges {
  @Input()
  flexColumn: boolean | string = false;

  @Input()
  flexGrow = 0;

  constructor(
    private renderer: Renderer2,
    private elRef: ElementRef
  ) {
    this.addClass('flex');
  }


  ngOnChanges(changes: SimpleChanges): void {
    if ('flexColumn' in changes) {
      if (this.flexColumn || this.flexColumn === '') {
        this.addClass('flex-column');
      } else {
        this.removeClass('flex-column');
      }
    }
    if ('flexGrow' in changes) {
      if (!changes.flexGrow.firstChange) {
        this.removeClass(`f-grow-${changes.flexGrow.previousValue}`);
      }
      this.addClass(`f-grow-${this.flexGrow}`);
    }
  }

  private addClass(classname) {
    this.renderer.addClass(this.elRef.nativeElement, classname);
  }

  private removeClass(classname) {
    this.renderer.removeClass(this.elRef.nativeElement, classname);
  }
}
