import { CalculatorParams } from '../calculator/calculator-params';

export abstract class Rates {
  abstract getRate(deal: CalculatorParams): Promise<number>;
}
