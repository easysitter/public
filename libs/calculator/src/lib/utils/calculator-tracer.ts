import { Injectable } from "@angular/core";
export interface TraceValue {
  from: Date;
  to: Date;
  price: number;
}

export abstract class CalculatorTracer {
  traced: TraceValue[];
  abstract fork(context): CalculatorTracer;
  abstract trace(value: TraceValue, context?: string);
  abstract flush();
}

@Injectable()
export class NoopTracer implements CalculatorTracer {
  traced: TraceValue[] = [];

  flush() {
  }

  fork(context): CalculatorTracer {
    return this;
  }

  trace(value: TraceValue, context?: string) {
  }

}
