export interface CalculatorParams {
  from: Date;
  to: Date;
  nanny?: number;
  children?: number[];
  startedAt?: Date;
  finishedAt?: Date;
}
