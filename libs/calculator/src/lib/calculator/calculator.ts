import { CalculatorTracer } from '../utils/calculator-tracer';
import { CalculatorParams } from './calculator-params';

export abstract class Calculator {
  abstract getPrice(deal: CalculatorParams, tracer: CalculatorTracer): Promise<number>;
}
