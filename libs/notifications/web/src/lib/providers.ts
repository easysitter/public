import { InjectionToken } from '@angular/core';

export type FirebaseConfig = Readonly<Record<string, any>>;
export const FIREBASE_CONFIG = new InjectionToken<FirebaseConfig>('');
