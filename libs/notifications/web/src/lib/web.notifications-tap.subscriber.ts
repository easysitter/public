import { WebNotifications } from './web-notifications';
import { NotificationsTapSubscriber } from '@es/notifications/core';
import { Injectable } from '@angular/core';

@Injectable()
export class WebNotificationsTapSubscriber extends NotificationsTapSubscriber {
  subscribe(provider: WebNotifications): void {}
}
