import { Inject, Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { defer, EMPTY, Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import 'firebase/messaging';
import { filter, switchMap, tap } from 'rxjs/operators';
import { NotificationsProvider } from '@es/notifications/core';
import { FIREBASE_CONFIG, FirebaseConfig } from './providers';

@Injectable()
export class WebNotifications implements NotificationsProvider {
  constructor(
    private push: SwPush,
    @Inject(FIREBASE_CONFIG) private readonly firebaseConfig: FirebaseConfig
  ) {}

  initialize(): Observable<string> {
    if (!firebase.apps.length) {
      firebase.initializeApp(this.firebaseConfig);
    }
    if (!firebase.messaging.isSupported()) return EMPTY;

    const messaging = firebase.messaging();

    return defer(() => navigator.serviceWorker.getRegistration()).pipe(
      filter((swr) => !!swr),
      tap((swr) => messaging.useServiceWorker(swr)),
      switchMap(() => messaging.getToken())
    );
  }

  subscribeActions(): Observable<any> {
    return this.push.notificationClicks;
  }

  subscribeNotifications(): Observable<any> {
    return this.push.messages;
  }
}
