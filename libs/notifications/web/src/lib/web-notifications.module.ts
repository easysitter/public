import { ModuleWithProviders, NgModule } from '@angular/core';
import { WebNotificationsTapSubscriber } from './web.notifications-tap.subscriber';
import {
  NotificationsProvider,
  NotificationsTapSubscriber,
} from '@es/notifications/core';
import { WebNotifications } from './web-notifications';
import { FIREBASE_CONFIG, FirebaseConfig } from './providers';

interface NotificationsWebConfig {
  firebase: FirebaseConfig;
}

@NgModule({
  providers: [
    {
      provide: NotificationsTapSubscriber,
      useClass: WebNotificationsTapSubscriber,
    },
    {
      provide: NotificationsProvider,
      useClass: WebNotifications,
    },
  ],
})
export class WebNotificationsModule {
  static forRoot(config: NotificationsWebConfig): ModuleWithProviders<WebNotificationsModule> {
    return {
      ngModule: WebNotificationsModule,
      providers: [
        {
          provide: FIREBASE_CONFIG,
          useValue: config.firebase,
        },
      ],
    };
  }
}
