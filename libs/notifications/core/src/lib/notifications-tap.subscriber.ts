import { NotificationsProvider } from './notifications-provider';

export abstract class NotificationsTapSubscriber {
  abstract subscribe(provider: NotificationsProvider): void;
}
