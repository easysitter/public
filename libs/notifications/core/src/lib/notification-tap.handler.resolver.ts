import { Inject, Injectable, Injector, Optional } from '@angular/core';
import {
  NOTIFICATION_TAP_HANDLER,
  NotificationTapHandlerProvider,
} from './providers';
import { NotificationTapHandler } from './notification-tap.handler';

@Injectable({
  providedIn: 'root',
})
export class NotificationTapHandlerResolver {
  constructor(
    @Inject(NOTIFICATION_TAP_HANDLER)
    @Optional()
    private handlerProviders: NotificationTapHandlerProvider[],
    private injector: Injector
  ) {
    console.log(handlerProviders);
  }

  resolve(type): NotificationTapHandler | null {
    if (!this.handlerProviders) return null;
    const provider = this.handlerProviders.find(
      (handlerProvider) => handlerProvider.type === type
    );
    if (!provider) {
      return null;
    }
    return this.injector.get(provider.handler, null);
  }
}
