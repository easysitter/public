import { InjectionToken, Provider, Type } from '@angular/core';
import { NotificationTapHandler } from '@es/notifications/core';

export interface NotificationTapHandlerProvider {
  type: string;
  handler: Type<NotificationTapHandler>;
}

export const NOTIFICATION_TAP_HANDLER = new InjectionToken<
  NotificationTapHandlerProvider
>('NOTIFICATION_TAP_HANDLER');

export function provideNotificationHandler(
  handler: NotificationTapHandlerProvider
): Provider {
  return {
    provide: NOTIFICATION_TAP_HANDLER,
    useValue: handler,
    multi: true,
  };
}
