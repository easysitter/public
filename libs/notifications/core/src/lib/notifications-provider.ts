import { Observable } from 'rxjs';

export abstract class NotificationsProvider {
  abstract initialize(): Observable<string>;

  abstract subscribeNotifications(): Observable<any>;

  abstract subscribeActions(): Observable<any>;
}
