export interface NotificationTapHandler {
  handle(data: any): void;
}
