# notifications-core

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test notifications-core` to execute the unit tests via [Jest](https://jestjs.io).
