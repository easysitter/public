import { ModuleWithProviders, NgModule } from '@angular/core';
import {
  NotificationsProvider,
  NotificationsTapSubscriber,
} from '@es/notifications/core';
import { NativeNotifications } from './native-notifications';
import { NativeNotificationTapSubscriber } from './native.notifications-tap.subscriber';

@NgModule({
  providers: [
    { provide: NotificationsProvider, useClass: NativeNotifications },
    {
      provide: NotificationsTapSubscriber,
      useClass: NativeNotificationTapSubscriber,
    },
  ],
})
export class NativeNotificationsModule {
  static forRoot(): ModuleWithProviders<NativeNotificationsModule> {
    return {
      ngModule: NativeNotificationsModule,
    };
  }
}
