import { bindCallback, Observable, race } from 'rxjs';
import {
  Plugins,
  PushNotification,
  PushNotificationActionPerformed,
} from '@capacitor/core';
import { PushNotificationToken } from '@capacitor/core/dist/esm/core-plugin-definitions';
import { map, pluck, tap } from 'rxjs/operators';
import { NotificationsProvider } from '@es/notifications/core';
import { Injectable } from '@angular/core';

const { PushNotifications } = Plugins;
const pushNotificationsBind = bindCallback(PushNotifications.addListener);

interface PushRegistration {
  event: 'registration';
  result: PushNotificationToken;
}

interface PushRegistrationError {
  event: 'registrationError';
  result: any;
}

interface PushNotificationReceived {
  event: 'pushNotificationReceived';
  result: PushNotification;
}

interface PushNotificationAction {
  event: 'pushNotificationActionPerformed';
  result: PushNotificationActionPerformed;
}

type PushEvent =
  | PushRegistration
  | PushRegistrationError
  | PushNotificationReceived
  | PushNotificationAction;
const fromPush = <T extends PushEvent>(
  event: T['event']
): Observable<T['result']> => {
  return pushNotificationsBind.call(PushNotifications, event);
};

@Injectable()
export class NativeNotifications extends NotificationsProvider {
  initialize(): Observable<string> {
    PushNotifications.requestPermission().then((result) => {
      if (result.granted) {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    const registrationSuccess$ = fromPush<PushRegistration>(
      'registration'
    ).pipe(pluck('value'));
    const registrationError$ = fromPush<PushRegistrationError>(
      'registrationError'
    ).pipe(
      tap((err) => console.error(err)),
      map((err) => {
        throw err;
      })
    );
    return race(registrationSuccess$, registrationError$);
  }

  // Show us the notification payload if the app is open on our device
  subscribeNotifications(): Observable<PushNotification> {
    return fromPush<PushNotificationReceived>('pushNotificationReceived');
  }

  // Method called when tapping on a notification
  subscribeActions(): Observable<PushNotificationActionPerformed> {
    return fromPush<PushNotificationAction>('pushNotificationActionPerformed');
  }
}
