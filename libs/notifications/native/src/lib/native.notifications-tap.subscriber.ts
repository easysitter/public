import { Injectable } from '@angular/core';
import { pluck } from 'rxjs/operators';
import { NotificationTapHandlerResolver } from '@es/notifications/core';
import { NotificationsTapSubscriber } from '@es/notifications/core';
import { NativeNotifications } from './native-notifications';

@Injectable()
export class NativeNotificationTapSubscriber
  implements NotificationsTapSubscriber {
  constructor(
    private notificationTapHandlerResolver: NotificationTapHandlerResolver
  ) {}

  subscribe(provider: NativeNotifications) {
    console.group(NativeNotificationTapSubscriber.name);
    console.log('subscribe', provider);
    console.groupEnd();
    provider
      .subscribeActions()
      .pipe(pluck('notification'))
      .subscribe((notification) => {
        console.log(notification);
        const handler = this.notificationTapHandlerResolver.resolve(
          notification.data.type
        );
        console.log({ notification, handler });
        if (handler) {
          handler.handle(notification.data);
        }
      });
  }
}
