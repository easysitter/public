// tslint:disable
/**
 * Addresses api
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as globalImportUrl from 'url';
import { Configuration } from './configuration';
import globalAxios, { AxiosInstance, AxiosPromise } from 'axios';
// Some imports not used depending on template conditions
// @ts-ignore
import { BASE_PATH, BaseAPI, RequestArgs, RequiredError } from './base';

/**
 *
 * @export
 * @interface District
 */
export interface District {
  /**
   *
   * @type {string}
   * @memberof District
   */
  id: string;
  /**
   *
   * @type {string}
   * @memberof District
   */
  title: string;
}

/**
 *
 * @export
 * @interface ZipCheckInput
 */
export interface ZipCheckInput {
  /**
   *
   * @type {string}
   * @memberof ZipCheckInput
   */
  zip: string;
  /**
   *
   * @type {Array<string>}
   * @memberof ZipCheckInput
   */
  districtIds: Array<string>;
}

/**
 *
 * @export
 * @interface ZipCheckResult
 */
export interface ZipCheckResult {
  /**
   *
   * @type {string}
   * @memberof ZipCheckResult
   */
  zip: string;
  /**
   *
   * @type {string}
   * @memberof ZipCheckResult
   */
  districtId: string;
  /**
   *
   * @type {boolean}
   * @memberof ZipCheckResult
   */
  zipBelongsToDistrict: boolean;
}

/**
 * DistrictApi - axios parameter creator
 * @export
 */
export const DistrictApiAxiosParamCreator = function(configuration?: Configuration) {
  return {
    /**
     *
     * @param {ZipCheckInput} zipCheckInput
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    checkZipInDistricts(zipCheckInput: ZipCheckInput, options: any = {}): RequestArgs {
      // verify required parameter 'zipCheckInput' is not null or undefined
      if (zipCheckInput === null || zipCheckInput === undefined) {
        throw new RequiredError('zipCheckInput', 'Required parameter zipCheckInput was null or undefined when calling checkZipInDistricts.');
      }
      const localVarPath = `/api/districts/check`;
      const localVarUrlObj = globalImportUrl.parse(localVarPath, true);
      let baseOptions;
      if (configuration) {
        baseOptions = configuration.baseOptions;
      }
      const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options };
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;


      localVarHeaderParameter['Content-Type'] = 'application/json';

      localVarUrlObj.query = { ...localVarUrlObj.query, ...localVarQueryParameter, ...options.query };
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = { ...localVarHeaderParameter, ...options.headers };
      const needsSerialization = (typeof zipCheckInput !== 'string') || localVarRequestOptions.headers['Content-Type'] === 'application/json';
      localVarRequestOptions.data = needsSerialization ? JSON.stringify(zipCheckInput !== undefined ? zipCheckInput : {}) : (zipCheckInput || '');

      return {
        url: globalImportUrl.format(localVarUrlObj),
        options: localVarRequestOptions
      };
    },
    /**
     *
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    getDistricts(options: any = {}): RequestArgs {
      const localVarPath = `/api/districts`;
      const localVarUrlObj = globalImportUrl.parse(localVarPath, true);
      let baseOptions;
      if (configuration) {
        baseOptions = configuration.baseOptions;
      }
      const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options };
      const localVarHeaderParameter = {} as any;
      const localVarQueryParameter = {} as any;


      localVarUrlObj.query = { ...localVarUrlObj.query, ...localVarQueryParameter, ...options.query };
      // fix override query string Detail: https://stackoverflow.com/a/7517673/1077943
      delete localVarUrlObj.search;
      localVarRequestOptions.headers = { ...localVarHeaderParameter, ...options.headers };

      return {
        url: globalImportUrl.format(localVarUrlObj),
        options: localVarRequestOptions
      };
    }
  };
};

/**
 * DistrictApi - functional programming interface
 * @export
 */
export const DistrictApiFp = function(configuration?: Configuration) {
  return {
    /**
     *
     * @param {ZipCheckInput} zipCheckInput
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    checkZipInDistricts(zipCheckInput: ZipCheckInput, options?: any): (axios?: AxiosInstance, basePath?: string) => AxiosPromise<Array<ZipCheckResult>> {
      const localVarAxiosArgs = DistrictApiAxiosParamCreator(configuration).checkZipInDistricts(zipCheckInput, options);
      return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
        const axiosRequestArgs = { ...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url };
        return axios.request(axiosRequestArgs);
      };
    },
    /**
     *
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    getDistricts(options?: any): (axios?: AxiosInstance, basePath?: string) => AxiosPromise<Array<District>> {
      const localVarAxiosArgs = DistrictApiAxiosParamCreator(configuration).getDistricts(options);
      return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
        const axiosRequestArgs = { ...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url };
        return axios.request(axiosRequestArgs);
      };
    }
  };
};

/**
 * DistrictApi - factory interface
 * @export
 */
export const DistrictApiFactory = function(configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
  return {
    /**
     *
     * @param {ZipCheckInput} zipCheckInput
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    checkZipInDistricts(zipCheckInput: ZipCheckInput, options?: any): AxiosPromise<Array<ZipCheckResult>> {
      return DistrictApiFp(configuration).checkZipInDistricts(zipCheckInput, options)(axios, basePath);
    },
    /**
     *
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     */
    getDistricts(options?: any): AxiosPromise<Array<District>> {
      return DistrictApiFp(configuration).getDistricts(options)(axios, basePath);
    }
  };
};

/**
 * DistrictApi - object-oriented interface
 * @export
 * @class DistrictApi
 * @extends {BaseAPI}
 */
export class DistrictApi extends BaseAPI {
  /**
   *
   * @param {ZipCheckInput} zipCheckInput
   * @param {*} [options] Override http request option.
   * @throws {RequiredError}
   * @memberof DistrictApi
   */
  public checkZipInDistricts(zipCheckInput: ZipCheckInput, options?: any) {
    return DistrictApiFp(this.configuration).checkZipInDistricts(zipCheckInput, options)(this.axios, this.basePath);
  }

  /**
   *
   * @param {*} [options] Override http request option.
   * @throws {RequiredError}
   * @memberof DistrictApi
   */
  public getDistricts(options?: any) {
    return DistrictApiFp(this.configuration).getDistricts(options)(this.axios, this.basePath);
  }

}


