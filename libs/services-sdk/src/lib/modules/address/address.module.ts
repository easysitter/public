import { DynamicModule, HttpModule, HttpService, Module } from '@nestjs/common';
import { Configuration } from './configuration';
import { DistrictApi } from './api';

@Module({
  imports: [HttpModule]
})
export class AddressModule {
  static forRoot(baseUrl): DynamicModule {
    return {
      module: AddressModule,
      providers: [{
        provide: DistrictApi,
        useFactory: (http: HttpService) => {
          return new DistrictApi(
            new Configuration({}),
            baseUrl,
            http.axiosRef
          );
        },
        inject: [HttpService]
      }],
      exports: [
        DistrictApi
      ]
    };
  }
}
