/// <reference path="../parent.d.ts" />
import { HttpService, Inject, Injectable } from '@nestjs/common';
import { PARENT_PROFILE_SERVICE_BASE_URL } from '../providers';
import { processResponse } from '../utils/operators';

@Injectable()
export class ParentService {

  constructor(
    @Inject(PARENT_PROFILE_SERVICE_BASE_URL) private readonly BASE_URL: string,
    private http: HttpService
  ) {
  }


  async getById(id: Parameters.ProfileId): Promise<ParentProfile> {
    return this.http.get<ParentProfile>(`${this.BASE_URL}/${id}`).pipe(
      processResponse()
    ).toPromise();
  }

  async create(data: CreateProfileDto) {
    return await this.http.post(`${this.BASE_URL}`, data).pipe(
      processResponse()
    ).toPromise();
  }

  async update(id: Parameters.ProfileId, data: ParentProfile): Promise<ParentProfile> {
    return await this.http.put(`${this.BASE_URL}/${id}`, data)
      .pipe(
        processResponse()
      )
      .toPromise();
  }
}
