/// <reference path="../review.d.ts" />
import { HttpService, Inject } from '@nestjs/common';
import { processResponse } from '../utils/operators';
import { REVIEWS_SERVICE_BASE_URL } from '../providers';

export class ReviewsService {
  constructor(
    @Inject(REVIEWS_SERVICE_BASE_URL)
    private BASE_URL,
    private http: HttpService
  ) {
  }

  create(data: ReviewsApi.CreateReviewDto) {
    return this.http.post<ReviewsApi.Responses.$201>(`${this.BASE_URL}/reviews`, data).pipe(
      processResponse()
    ).toPromise();
  }

  find(params: ReviewsApi.QueryParameters) {
    return this.http.get<ReviewsApi.Responses.$200>(
      `${this.BASE_URL}/reviews`,
      { params }
    ).pipe(
      processResponse()
    ).toPromise();
  }
}
