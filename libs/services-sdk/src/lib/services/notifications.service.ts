/// <reference path="../notifications.d.ts" />
import { HttpService, Inject, Injectable } from '@nestjs/common';
import { processResponse } from '../utils/operators';
import { NOTIFICATIONS_SERVICE_BASE_URL } from '../providers';

@Injectable()
export class NotificationsService {

  constructor(
    @Inject(NOTIFICATIONS_SERVICE_BASE_URL) private readonly BASE_URL: string,
    private http: HttpService
  ) {
  }

  subscribePush(data: NotificationsApi.SavePushTokenDto): Promise<void> {
    return this.http.post<void>(`${this.BASE_URL}/push/subscribe`, data).pipe(
      processResponse()
    ).toPromise();
  }

  sendPush(data: NotificationsApi.PushNotificationDto): Promise<void> {
    return this.http.post<void>(`${this.BASE_URL}/push/send`, data).pipe(
      processResponse()
    ).toPromise();
  }
}
