import { RESTDataSource } from "apollo-datasource-rest";

export interface SearchDto {
  date: Date;
  fromTime: string;
  toTime: string;
  trial: boolean;
  children: number[];
}

export interface SearchResult {
  id: number;
  parent: number;
  date: Date;
  fromTime: string;
  toTime: string;
  trial: boolean;
  children: number[];
  nannies: SearchResultNanny[]
}

export interface SearchResultNanny {
  id: number;
  rate: number;
  isApproved: boolean;
}

export class SearchApi extends RESTDataSource {

  constructor(
    baseUrl: string
  ) {
    super();
    this.baseURL = baseUrl;
  }

  approve(result: SearchResult, approved: number[]): Promise<SearchResult> {
    return this.put(`search/results/${result.id}`, {
      nannies: approved
    });
  }

  getSearchResult(id: number): Promise<SearchResult> {
    return this.get(`search/results/${id}`);
  }

  async search(parentId: number, params: SearchDto): Promise<SearchResult> {
    return await this.post(`search`, {
      ...params,
      parent: parentId
    });
  }

}
