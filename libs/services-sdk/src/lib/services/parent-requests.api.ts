import { RESTDataSource } from "apollo-datasource-rest";

export interface CreateRequestDto {
  parent: number;
  date: Date;
  fromTime: string;
  toTime: string;
  children: number[];
  trial: boolean;

  nannies: {
    id: number,
    rate: number
  }[],

  address: string;
  zipCode: string;
  phone: string;
  wishes: string;

  trackId: string;
}

export interface ParentRequest {
  id: number;
  trackId: string | number;
  date: Date
  fromTime: string
  toTime: string
  children: number[]
  trial: boolean
  address: string
  parent: number
  wishes: string
  nannyRequests: any[]
}

export class ParentRequestsApi extends RESTDataSource {

  constructor(
    baseUrl: string
  ) {
    super();
    this.baseURL = baseUrl;
  }

  async getRequests(parent, options?: {}): Promise<ParentRequest[]> {
    const requests = await this.get<ParentRequest[]>(`parents/${parent}/requests`, options);
    return requests.map(deserialize);
  }

  async getRequest(parent, request): Promise<ParentRequest> {
    const result = await this.get(`parents/${parent}/requests/${request}`);
    return deserialize(result);
  }

  async create({ wishes, ...data }: CreateRequestDto): Promise<ParentRequest> {
    const { comment, ...result } = await this.post(`requests`, {
      ...data,
      comment: wishes
    });
    return {
      ...deserialize(result),
      wishes: comment,
      nannyRequests: []
    } as ParentRequest;
  }

  async cancel(parent: number, requestId: number): Promise<boolean> {
    try {
      await this.delete(`parents/${parent}/requests/${requestId}`);
    } catch (e) {
      console.log(e);
      return false;
    }
    return true;
  }


}

function deserialize(item: any): ParentRequest {
  return {
    ...item,
    date: new Date(item.date)
  };
}
