import { RESTDataSource } from 'apollo-datasource-rest';

interface Chat {
  id: string;
  users: string[];
  meta: any
}

interface ChatMessage {
  id: string;
  user: string;
  text: string;
  createdAt: string;
  viewedBy: string[];
}

export class ChatApi extends RESTDataSource {
  constructor(
    baseUrl: string
  ) {
    super();
    this.baseURL = `${baseUrl}/chat`;
  }

  async getById(id) {
    return await this.get<Chat>(`${id}`);
  }

  async findDirect({ nanny, parent }: { nanny: string, parent: string }) {
    const chat = await this.get<Chat>(`${parent}/direct/${nanny}`);
    if (chat) return chat;

    return this.createDirect({ nanny, parent });
  }

  createDirect({ nanny, parent }: { nanny: string, parent: string }) {
    return this.post<Chat>(`${parent}/direct`, {
      directWith: nanny
    });
  }

  findForRequest({ nanny, request }: { nanny: string, request: number }) {
    return this.post<Chat>(`search-for-request`, {
      nanny, request
    });
  }

  findAllDirect(user) {
    return this.get<Chat[]>(`${user}/direct`);
  }

  createRoom(users, meta) {
    return this.post<Chat>('', {
      users, meta
    });
  }

  getMessages(roomId) {
    return this.get<ChatMessage[]>(`${roomId}/messages`);
  }

  sendMessage(roomId, text, user) {
    return this.post<ChatMessage>(`${roomId}/messages`, {
      text, user
    });
  }

  async getUnseenCount(roomId, user) {
    const { count } = await this.get<{ count: number }>(`${roomId}/unseen`, {
      user
    });
    return count;
  }

  async updateUnseen(roomId, user, lastSeenMessageDate) {
    const { success } = await this.put<{ success: boolean }>(`${roomId}/unseen`, {
      user,
      lastSeenMessageDate
    });
    return success;
  }
}
