import { FirebaseConfig } from './firebase-config';
import { Readable, Writable } from 'stream';
import { Injectable } from '@nestjs/common';

interface Upload {
  filename: string;
  mimetype: string;
  stream: Readable
}

@Injectable()
export class FilesStorageService {
  private bucket = this.firebaseConfig.admin.storage().bucket(this.firebaseConfig.bucket);

  constructor(
    private firebaseConfig: FirebaseConfig
  ) {
  }

  async uploadFromStream(data: Upload) {
    const { filename, mimetype, stream } = data;
    const file = this.bucket.file(filename);

    await this.uploadFile(file, mimetype, stream);
    await file.makePublic();
    return this.getPublicUrl(file);
  }

  private uploadFile(bucketFile, mimetype: string, uploadFileStream: Readable) {
    return new Promise((resolve, reject) => {
      const bucketFileStream = this.createWriteStream(bucketFile, mimetype);
      bucketFileStream.on('error', reject);
      bucketFileStream.on('finish', resolve);
      this.connectReadWriteStreams(uploadFileStream, bucketFileStream);
    });
  }

  private createWriteStream(
    bucketFile,
    mimetype: string
  ): Writable {
    return bucketFile.createWriteStream({
      metadata: {
        contentType: mimetype
      }
    });
  }

  private connectReadWriteStreams(readStream: Readable, writeStream: Writable) {
    readStream.on('error', (err) => writeStream.destroy(err));
    readStream.pipe(writeStream);
  }

  private getPublicUrl(file) {
    return `https://storage.googleapis.com/${this.bucket.name}/${file.name}`;
  }
}
