/// <reference path="../deals.d.ts" />

import { HttpService, Inject, Injectable } from '@nestjs/common';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { processResponse } from '../utils/operators';
import { DEALS_SERVICE_BASE_URL } from '../providers';

interface SearchParams {
  request: Parameters.Request;
  parent: Parameters.Parent;
  nanny: Parameters.Nanny;
  state: Parameters.State;
  isActive: Parameters.IsActive;
}

@Injectable()
export class DealsService {
  constructor(
    @Inject(DEALS_SERVICE_BASE_URL)
    private readonly BASE_URL: string,
    private http: HttpService
  ) {
  }

  async find(params: Partial<SearchParams>): Promise<Deal[]> {
    return this.createFind(params).toPromise();
  }

  async findById(id: Parameters.DealId): Promise<Deal> {
    return this.http
      .get(`${this.BASE_URL}/deals/${id}`)
      .pipe(
        processResponse()
      )
      .toPromise();
  }

  async createDeal(offer: OfferDto): Promise<Deal> {
    return this.http
      .post(`${this.BASE_URL}/deals`, offer)
      .pipe(
        processResponse(),
        catchError(() => of(null))
      )
      .toPromise();
  }

  async acceptOffer(offerId: Parameters.DealId, data: OfferResponderDto): Promise<Deal> {
    return this.http
      .put(`${this.BASE_URL}/deals/${offerId}/offer/accept`, data)
      .pipe(
        processResponse()
      )
      .toPromise();
  }

  async refuseOffer(offerId: Parameters.DealId, data: OfferResponderDto): Promise<Deal> {
    return this.http
      .put(`${this.BASE_URL}/deals/${offerId}/offer/refuse`, data)
      .pipe(
        processResponse()
      )
      .toPromise();
  }

  async startVisit(visitId: Parameters.DealId): Promise<Deal> {
    return this.http
      .put(`${this.BASE_URL}/deals/${visitId}/visit/start`, {})
      .pipe(
        processResponse()
      )
      .toPromise();
  }

  async finishVisit(visitId: Parameters.DealId): Promise<Deal> {
    return this.http
      .put(`${this.BASE_URL}/deals/${visitId}/visit/finish`, {})
      .pipe(
        processResponse()
      )
      .toPromise();
  }

  async cancelVisit(visitId: Parameters.DealId): Promise<Deal> {
    return this.http.delete(`${this.BASE_URL}/deals/${visitId}`)
      .pipe(
        processResponse()
      )
      .toPromise();
  }

  private createFind(params: Partial<SearchParams>) {
    return this.http
      .get(`${this.BASE_URL}/deals`, {
        params
      })
      .pipe(processResponse());
  }
}


