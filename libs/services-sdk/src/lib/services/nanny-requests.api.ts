import { RESTDataSource } from "apollo-datasource-rest";

export type NannyRequestStatus = "new" | "accepted" | "closed"

export interface ParentRequest {
  id: number;
  trial: boolean;
  date: Date,
  fromTime: string;
  toTime: string;
  parent: number;
  address: string;
  children: number[];
  wishes: string;
}

export interface NannyRequest {
  id: string,
  rate: number;
  nanny: number;
  status: NannyRequestStatus
  parentRequest: ParentRequest
}

export interface NannyRequestFindFilter {
  status?: NannyRequestStatus
  nanny: number
}

export class NannyRequestsApi extends RESTDataSource {

  constructor(
    baseUrl: string
  ) {
    super();
    this.baseURL = baseUrl;
  }

  async find(filter: NannyRequestFindFilter): Promise<NannyRequest[]> {
    const result = await this.get(`nanny/${filter.nanny}/requests`, {
      isActive: 1
    });
    return result.map((nannyReq) => this.serializeNannyRequest(nannyReq));
  }

  async findById(nanny: number, id: string): Promise<NannyRequest> {
    const result = await this.get(`nanny/${nanny}/requests/${id}`);
    return this.serializeNannyRequest(result);
  }

  async accept(nanny: number, id: string): Promise<NannyRequest> {
    const result = await this.put(`nanny/${nanny}/requests/${id}/accept`);
    return this.serializeNannyRequest(result);
  }

  async reject(nanny: number, id: string): Promise<NannyRequest> {
    const result = await this.put(`nanny/${nanny}/requests/${id}/refuse`);
    return this.serializeNannyRequest(result);
  }

  private serializeNannyRequest(nannyReq) {
    const { user, parentRequest, ...others } = nannyReq;
    return {
      ...others,
      nanny: user,
      parentRequest: parentRequest ? {
        ...parentRequest,
        parent: parentRequest.user
      } : null
    };
  }
}
