import { RESTDataSource } from "apollo-datasource-rest";

export class NannyProfileApi extends RESTDataSource {
  constructor(
    baseUrl: string
  ) {
    super();
    this.baseURL = baseUrl;
  }

  getByUserId(userId: string) {
    return this.get(`${userId}`);
  }

  getByProfileId(profileId: number): Promise<any> {
    return this.get(`profile/${profileId}`);
  }

  async save(userId: string, profileData: any) {
    return this.put(`${userId}`, profileData);
  }
}
