import * as admin from 'firebase-admin';
import { resolve } from 'path';

export class FirebaseConfig {
  public readonly admin!: admin.app.App;
  public readonly bucket = process.env.FIREBASE_UPLOADS_BUCKET;

  constructor() {
    this.admin = admin.initializeApp({
      credential: admin.credential.cert(resolve(process.env.FIREBASE_SECRET_PATH))
    });
  }
}
