declare interface Deal {
  id: number;
  requestId: string;
  trackingId: string;
  parent: number;
  nanny: number;
  initiator: string;
  date: string; // date
  fromTime: string;
  toTime: string;
  rate: number;
  children: string[];
  address: string;
  trial: boolean;
  isActive: boolean;
  state: string;
  tag: string;
  startedAt: Date;
  finishedAt: Date;
}

declare interface OfferDetails {
  rate: number;
  date: string; // date
  fromTime: string;
  toTime: string;
  address: string;
  children: number[];
  trial: boolean;
  safePayment: boolean;
  comment: string;
}

declare interface OfferDto {
  requestId: string;
  trackingId: string;
  parent: number;
  nanny: number;
  initiator: 'parent' | 'nanny';
  details: OfferDetails;
}

declare interface OfferResponderDto {
  type: 'parent' | 'nanny';
  user: number;
}

declare namespace Parameters {
  export type DealId = number;
  export type IsActive = boolean;
  export type Nanny = number;
  export type OfferAction = 'accept' | 'refuse';
  export type Parent = number;
  export type Request = string;
  export type State = 'offer' | 'deal';
  export type VisitAction = 'start' | 'finish';
}

declare interface PathParameters {
  dealId: Parameters.DealId;
  visitAction: Parameters.VisitAction;
}

declare interface QueryParameters {
  request?: Parameters.Request;
  parent?: Parameters.Parent;
  nanny?: Parameters.Nanny;
  state?: Parameters.State;
  isActive?: Parameters.IsActive;
}
