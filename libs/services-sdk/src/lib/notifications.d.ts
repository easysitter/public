declare namespace NotificationsApi {
  export interface PushNotificationDto {
    user: string;
    title: string;
    body: string;
  }

  export type RequestBody = NotificationsApi.PushNotificationDto;

  export interface SavePushTokenDto {
    user: string;
    token: string;
  }
}

