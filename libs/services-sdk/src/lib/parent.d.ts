declare interface CreateProfileDto {
  userId: string;
  profile: ProfileDto;
}

declare namespace Parameters {
  export type ProfileId = number;
  export type UserId = string;
}

declare interface ParentProfile {
  id: number;
  name: string;
  birthDate: string;
  photo: string;
  phone: string;
  description: string;
  address: string;
  zipCode: string;
  children: number[];
}

declare interface PathParameters {
  profileId: Parameters.ProfileId;
}

declare interface ProfileDto {
  name: string;
  birthDate: string;
  photo: string;
  phone: string;
  description: string;
  address: string;
  zipCode: string;
  children: (0 | 1 | 2 | 3 | 4 | 5)[];
}
