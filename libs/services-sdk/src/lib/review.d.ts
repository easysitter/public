declare namespace ReviewsApi {
  export interface CreateReviewDto {
    refType: string;
    refId: string;
    userType: string;
    userId: string;
    rating: number;
    pros: string;
    cons: string;
  }

  namespace Parameters {
    export type RefId = string;
    export type RefType = string;
    export type UserId = string;
    export type UserType = string;
  }

  export interface QueryParameters {
    refType?: ReviewsApi.Parameters.RefType;
    refId?: ReviewsApi.Parameters.RefId;
    userType?: ReviewsApi.Parameters.UserType;
    userId?: ReviewsApi.Parameters.UserId;
  }

  export type RequestBody = ReviewsApi.CreateReviewDto;
  namespace Responses {
    export type $200 = ReviewsApi.ReviewDto[];
    export type $201 = ReviewsApi.ReviewDto;
  }

  export interface ReviewDto {
    id: string;
    rating: number;
    pros: string;
    cons: string;
    createdAt: string;
  }
}

