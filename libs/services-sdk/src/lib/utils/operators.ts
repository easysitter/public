import { catchError, pluck } from 'rxjs/operators';
import { MonoTypeOperatorFunction, ObservableInput, OperatorFunction, pipe, throwError } from 'rxjs';
import { AxiosResponse } from 'axios';

export function logAxiosError<T, R extends ObservableInput<any>>(): MonoTypeOperatorFunction<T> {
  return catchError((e) => {

    if (e.isAxiosError) {
      console.log(e.response.data.message);
    }
    return throwError(e);
  });
}

export function processResponse<T>(): OperatorFunction<AxiosResponse<T>, T> {
  return pipe(
    logAxiosError(),
    pluck('data')
  );
}
