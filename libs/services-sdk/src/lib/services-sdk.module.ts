import { DynamicModule, Global, HttpModule, Module } from "@nestjs/common";
import {
  CHAT_SERVICE_BASE_URL,
  DEALS_SERVICE_BASE_URL,
  NANNY_PROFILE_SERVICE_BASE_URL,
  NOTIFICATIONS_SERVICE_BASE_URL,
  PARENT_PROFILE_SERVICE_BASE_URL,
  REQUESTS_SERVICE_BASE_URL,
  REVIEWS_SERVICE_BASE_URL,
  SEARCH_SERVICE_BASE_URL
} from './providers';
import { DealsService } from './services/deals.service';
import { ParentService } from './services/parent.service';
import { ReviewsService } from './services/reviews.service';
import { NotificationsService } from './services/notifications.service';
import { AddressModule } from './modules/address/address.module';

@Module({})
@Global()
export class ServicesSdkModule {
  static forRoot(serviceMap: ServicesConfig): DynamicModule {
    const urlProviders = this.getUrlProviders(serviceMap);
    return {
      module: ServicesSdkModule,
      imports: [
        HttpModule,
        AddressModule.forRoot(serviceMap.baseUrlsConfig.address)
      ],
      providers: [
        ...urlProviders,
        DealsService,
        ParentService,
        ReviewsService,
        NotificationsService
      ],
      exports: [
        DealsService,
        ParentService,
        ReviewsService,
        NotificationsService,
        AddressModule
      ]
    };
  }

  private static getUrlProviders(serviceMap: ServicesConfig) {
    return [
      { provide: NANNY_PROFILE_SERVICE_BASE_URL, useValue: serviceMap.baseUrlsConfig.nannyProfile },
      { provide: PARENT_PROFILE_SERVICE_BASE_URL, useValue: serviceMap.baseUrlsConfig.parentProfile },
      { provide: CHAT_SERVICE_BASE_URL, useValue: serviceMap.baseUrlsConfig.chat },
      { provide: DEALS_SERVICE_BASE_URL, useValue: serviceMap.baseUrlsConfig.deals },
      { provide: REQUESTS_SERVICE_BASE_URL, useValue: serviceMap.baseUrlsConfig.requests },
      { provide: SEARCH_SERVICE_BASE_URL, useValue: serviceMap.baseUrlsConfig.search },
      { provide: REVIEWS_SERVICE_BASE_URL, useValue: serviceMap.baseUrlsConfig.reviews },
      { provide: NOTIFICATIONS_SERVICE_BASE_URL, useValue: serviceMap.baseUrlsConfig.notifications }
    ];
  }
}

interface ServicesConfig {
  baseUrlsConfig: BaseUrlConfig
}

interface BaseUrlConfig {
  nannyProfile: string;
  parentProfile: string;
  requests: string;
  deals: string;
  search: string;
  chat: string;
  reviews: string;
  notifications: string;
  address: string;
}
