import { Directive, forwardRef, HostBinding, HostListener } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  selector: '[esChildSelectable]',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ChildSelectableDirective), multi: true }
  ],
  host: {
    '[class.selectable]': 'true',
    '[class.inactive]': '!selected'
  }
})
export class ChildSelectableDirective implements ControlValueAccessor {

  @HostBinding('class.selected')
  selected = false;

  private onChange;
  private onTouched;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: any): void {
    this.selected = obj;
  }

  @HostListener('click')
  onClick() {
    this.selected = !this.selected;
    this.onTouched();
    this.onChange(this.selected);
  }
}
