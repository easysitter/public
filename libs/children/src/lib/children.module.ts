import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxdModule } from '@ngxd/core';
import { ChildWithIconComponent } from './child-with-icon.component';
import { ChildInfantComponent } from './child/child-infant.component';
import { ChildToddlerComponent } from './child/child-toddler.component';
import { ChildPreschoolerComponent } from './child/child-preschooler.component';
import { ChildMiddleComponent } from './child/child-middle.component';
import { ChildYoungTeenComponent } from './child/child-young-teen.component';
import { ChildTeenagerComponent } from './child/child-teenager.component';
import { ChildComponent } from './child.component';
import { ChildInfantIconComponent } from './with-icon/child-infant-icon.component';
import { ChildMiddleIconComponent } from './with-icon/child-middle-icon.component';
import { ChildPreschoolerIconComponent } from './with-icon/child-preschooler-icon.component';
import { ChildTeenagerIconComponent } from './with-icon/child-teenager-icon.component';
import { ChildToddlerIconComponent } from './with-icon/child-toddler-icon.component';
import { ChildYoungTeenIconComponent } from './child/child-young-teen-icon.component';
import { ChildSelectableDirective } from './child-selectable.directive';

@NgModule({
    imports: [CommonModule, NgxdModule],
    declarations: [
        ChildComponent,
        ChildWithIconComponent,
        ChildInfantComponent,
        ChildToddlerComponent,
        ChildPreschoolerComponent,
        ChildMiddleComponent,
        ChildYoungTeenComponent,
        ChildTeenagerComponent,
        ChildInfantIconComponent,
        ChildMiddleIconComponent,
        ChildPreschoolerIconComponent,
        ChildTeenagerIconComponent,
        ChildToddlerIconComponent,
        ChildYoungTeenIconComponent,
        ChildSelectableDirective
    ],
    exports: [
        ChildComponent,
        ChildWithIconComponent,
        ChildSelectableDirective
    ]
})
export class ChildrenModule {
}
