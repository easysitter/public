import { Component, Input, OnInit } from '@angular/core';
import { ChildInfantComponent } from './child/child-infant.component';
import { ChildToddlerComponent } from './child/child-toddler.component';
import { ChildPreschoolerComponent } from './child/child-preschooler.component';
import { ChildMiddleComponent } from './child/child-middle.component';
import { ChildYoungTeenComponent } from './child/child-young-teen.component';
import { ChildTeenagerComponent } from './child/child-teenager.component';

@Component({
  selector: 'es-child',
  template: `
      <ng-container *ngxComponentOutlet="resolve[child]"></ng-container>
  `,
  styles: []
})
export class ChildComponent implements OnInit {

  @Input() child: number;

  resolve = {
    0: ChildInfantComponent,
    1: ChildToddlerComponent,
    2: ChildPreschoolerComponent,
    3: ChildMiddleComponent,
    4: ChildYoungTeenComponent,
    5: ChildTeenagerComponent
  };

  constructor() {
  }

  ngOnInit() {
  }

}
