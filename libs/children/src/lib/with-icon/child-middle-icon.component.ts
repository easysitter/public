import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'es-child-middle-icon',
  template: `
      <p>
          child-middle-icon works!
      </p>
  `,
  styles: []
})
export class ChildMiddleIconComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
