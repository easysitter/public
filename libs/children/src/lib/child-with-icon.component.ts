import { Component, Input } from '@angular/core';

@Component({
  selector: 'es-child-with-icon',
  template: `
      <div class="inner">
          <div class="icon age-{{child+1}}"><i></i></div>
          <div class="age">
              <es-child [child]="child"></es-child>
          </div>
          <ng-content></ng-content>
      </div>
  `,
  styles: [],
  host: {
    '[class.children-list-item]': 'true'
  }
})
export class ChildWithIconComponent {
  @Input() child: number;
}
