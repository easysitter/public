import { Observable } from 'rxjs';

export interface PasswordLoginParams {
  login: string;
  password: string;
  skipUserInfo?: boolean;
}

export class InvalidGrantError extends Error {}

export function isInvalidGrantError(err: Error): err is InvalidGrantError {
  return err instanceof InvalidGrantError;
}

export abstract class AuthenticationService {
  abstract login(params?: PasswordLoginParams): Promise<void>;

  abstract logout(): Promise<void>;

  abstract refresh(): Promise<void>;

  abstract isLoggedIn(): Observable<boolean>;

  abstract getIdToken(): Observable<string>;

  abstract getAccessToken(): Observable<string>;

  abstract getIdentity(): Observable<any>;
}
