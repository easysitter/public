import { Injectable } from '@angular/core';
import {
  AuthenticationService,
  InvalidGrantError,
  PasswordLoginParams,
} from './authentication.service';
import { BehaviorSubject, merge, Observable, of, timer } from 'rxjs';
import { AuthConfig, OAuthService } from 'angular-oauth2-oidc';
import { filter, map, mapTo, shareReplay, switchMap } from 'rxjs/operators';

@Injectable()
export class OIDCService implements AuthenticationService {
  public readonly initialized = new BehaviorSubject(false);
  private changes = merge(
    timer(10).pipe(mapTo(null)),
    this.oAuthService.events
  ).pipe(shareReplay({ bufferSize: 1, refCount: false }));
  private idToken = this.changes.pipe(
    filter((e) => e && e.type === 'token_received'),
    map(() => this.oAuthService.getIdToken()),
    shareReplay({ bufferSize: 1, refCount: false })
  );
  private accessToken = merge(
    this.initialized.pipe(
      filter(() => this.oAuthService.hasValidAccessToken())
    ),
    this.changes.pipe(filter((e) => e && e.type === 'token_received'))
  ).pipe(
    map(() => this.oAuthService.getAccessToken()),
    shareReplay({ bufferSize: 1, refCount: false })
  );
  private loggedIn = this.initialized.pipe(
    filter((initialized) => initialized),
    switchMap(() => this.changes),
    map(() => this.oAuthService.hasValidAccessToken()),
    shareReplay({ bufferSize: 1, refCount: false })
  );

  constructor(private oAuthService: OAuthService) {}

  async initialize(config: AuthConfig): Promise<boolean> {
    try {
      this.oAuthService.configure(config);
      await this.oAuthService.loadDiscoveryDocumentAndTryLogin();
      if (
        this.oAuthService.hasValidAccessToken() &&
        !this.oAuthService.hasValidIdToken()
      ) {
        await this.oAuthService.refreshToken();
      }
      if (this.oAuthService.hasValidIdToken()) {
        // await this.oAuthService.loadUserProfile();
      }
      this.initialized.next(true);
      return true;
    } catch (e) {
      return false;
    }
  }

  getIdToken(): Observable<string> {
    return this.idToken;
  }

  getAccessToken(): Observable<string> {
    return this.accessToken;
  }

  isLoggedIn(): Observable<boolean> {
    return this.loggedIn;
  }

  async login(params?: PasswordLoginParams): Promise<void> {
    if (params) {
      try {
        await this.oAuthService.fetchTokenUsingPasswordFlow(
          params.login,
          params.password
        );
      } catch (e) {
        if (e.status === 401 && e.error?.error === 'invalid_grant') {
          throw new InvalidGrantError();
        }
        throw e;
      }
      if (!params.skipUserInfo) {
        await this.oAuthService.loadUserProfile();
      }
      return;
    }
    await this.oAuthService.initLoginFlow();
  }

  async logout(): Promise<void> {
    this.oAuthService.logOut(true);
  }

  async refresh(): Promise<void> {
    await this.oAuthService.refreshToken();
  }

  getIdentity(): Observable<any> {
    return of(this.oAuthService.getIdentityClaims());
  }
}
