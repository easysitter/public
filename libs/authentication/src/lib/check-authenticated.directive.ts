import {
  Directive,
  EmbeddedViewRef,
  Injectable,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';

// TODO: Add Angular decorator.
// TODO: Add Angular decorator.
@Injectable()
export abstract class CheckAuthenticatedStrategy implements OnDestroy {
  private destroy = new Subject<void>();
  private viewRef: EmbeddedViewRef<any>;
  private templateRef!: TemplateRef<any>;
  private vcr!: ViewContainerRef;

  constructor(private authService: AuthenticationService) {}

  configure(templateRef: TemplateRef<any>, vcr: ViewContainerRef): this {
    this.templateRef = templateRef;
    this.vcr = vcr;
    return this;
  }

  run(): void {
    this.authService
      .isLoggedIn()
      .pipe(distinctUntilChanged(), takeUntil(this.destroy))
      .subscribe((isLoggedIn) => {
        this.dispose();
        if (this.check(isLoggedIn)) {
          this.create();
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
    this.destroy = null;
    this.dispose();
  }

  dispose(): void {
    if (this.viewRef) {
      this.viewRef.destroy();
      this.viewRef = null;
    }
  }

  abstract check(isLoggedIn: boolean): boolean;

  protected create(): void {
    this.viewRef = this.vcr.createEmbeddedView(this.templateRef, null);
  }
}

@Injectable()
export class AuthenticatedStrategy extends CheckAuthenticatedStrategy {
  constructor(authenticationService: AuthenticationService) {
    super(authenticationService);
  }

  check(isAuthenticated: boolean): boolean {
    return isAuthenticated;
  }
}

@Injectable()
export class NotAuthenticatedStrategy extends CheckAuthenticatedStrategy {
  constructor(authenticationService: AuthenticationService) {
    super(authenticationService);
  }

  check(isAuthenticated: boolean): boolean {
    return !isAuthenticated;
  }
}

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[isAuthenticated]',
  providers: [
    { provide: CheckAuthenticatedStrategy, useClass: AuthenticatedStrategy },
  ],
})
export class CheckAuthenticatedDirective implements OnInit {
  constructor(
    private templateRef: TemplateRef<any>,
    private vcr: ViewContainerRef,
    private strategy: CheckAuthenticatedStrategy
  ) {}

  ngOnInit(): void {
    this.strategy.configure(this.templateRef, this.vcr).run();
  }
}

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[notAuthenticated]',
  providers: [
    { provide: CheckAuthenticatedStrategy, useClass: NotAuthenticatedStrategy },
  ],
})
export class CheckNotAuthenticatedDirective implements OnInit {
  constructor(
    private templateRef: TemplateRef<any>,
    private vcr: ViewContainerRef,
    private strategy: CheckAuthenticatedStrategy
  ) {}

  ngOnInit(): void {
    this.strategy.configure(this.templateRef, this.vcr).run();
  }
}
