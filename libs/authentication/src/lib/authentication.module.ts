import {
  APP_INITIALIZER,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationService } from './authentication.service';
import { AuthConfig } from 'angular-oauth2-oidc';
import { OIDCService } from './oidc.service';
import {
  AuthenticatedStrategy,
  CheckAuthenticatedDirective,
  CheckAuthenticatedStrategy,
  CheckNotAuthenticatedDirective,
} from './check-authenticated.directive';

export function oidcInitializerFactory(
  service: OIDCService,
  config: AuthConfig
) {
  return async () => await service.initialize(config);
}

export const OIDC_CONFIG = new InjectionToken<AuthConfig>('OIDC_CONFIG');

// @dynamic
@NgModule({
  declarations: [CheckAuthenticatedDirective, CheckNotAuthenticatedDirective],
  exports: [CheckAuthenticatedDirective, CheckNotAuthenticatedDirective],
  providers: [],
  imports: [CommonModule],
})
export class AuthenticationModule {
  static oidcForRoot(): ModuleWithProviders<AuthenticationModule> {
    return {
      ngModule: AuthenticationModule,
      providers: [
        OIDCService,
        {
          provide: AuthenticationService,
          useExisting: OIDCService,
        },
        {
          provide: APP_INITIALIZER,
          useFactory: oidcInitializerFactory,
          deps: [OIDCService, OIDC_CONFIG],
          multi: true,
        },
        {
          provide: CheckAuthenticatedStrategy,
          useClass: AuthenticatedStrategy,
        },
      ],
    };
  }
}
