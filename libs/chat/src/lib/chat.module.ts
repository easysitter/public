import { ModuleWithProviders, NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatMessageBoxComponent } from './chat-message-box/chat-message-box.component';
import { ChatMessageComponent } from './chat-message/chat-message.component';
import { ChatDirective } from './chat.directive';
import { ChatSendBoxComponent } from './chat-send-box/chat-send-box.component';
import { FormsModule } from '@angular/forms';
import { ChatDateMessageComponent } from './chat-date-message/chat-date-message.component';
import { ChatUserMessageComponent } from './chat-user-message/chat-user-message.component';
import { ChatRepo } from './classes/chat.repo';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    ChatDirective,
    ChatMessageBoxComponent,
    ChatMessageComponent,
    ChatSendBoxComponent
  ],
  declarations: [
    ChatMessageBoxComponent,
    ChatMessageComponent,
    ChatDirective,
    ChatSendBoxComponent,
    ChatDateMessageComponent,
    ChatUserMessageComponent
  ]
})
export class ChatModule {
  static forRoot(repo: Type<ChatRepo>): ModuleWithProviders<ChatModule> {
    return {
      ngModule: ChatModule,
      providers: [
        { provide: ChatRepo, useClass: repo }
      ]
    };
  }
}
