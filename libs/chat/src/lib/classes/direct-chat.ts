import {gql, Apollo} from 'apollo-angular';
import { Injectable } from "@angular/core";
import { map, pluck, repeatWhen, retry, shareReplay } from "rxjs/operators";


import { interval } from "rxjs";

const CHAT_USER_FRAGMENT = gql`
  fragment ChatUser on ChatUser {
    ...on ChatUserNanny {
      id
      type
      profile {
        id
        name
        photo
      }
    }
    ... on ChatUserParent {
      id
      type
      profile {
        id
        name
        photo
      }
    }
  }
`;
const QUERY = gql`
  query {
    directChatList {
      id
      unseen
      users {
        ...on ChatUserNanny {
          id
          type
          profile {
            id
            name
            photo
          }
        }
        ... on ChatUserParent {
          id
          type
          profile {
            id
            name
            photo
          }
        }
      }
      lastMessage {
        id
        text
        createdAt
        user {...on ChatUserNanny {
          id
          type
          profile {
            id
            name
            photo
          }
        }
          ... on ChatUserParent {
            id
            type
            profile {
              id
              name
              photo
            }
          }
        }
      }
    }
  }
`;

@Injectable({
  providedIn: "root"
})
export class DirectChat {
  list = this.apollo.query<{ directChatList: any[] }>({
    query: QUERY,
    fetchPolicy: "network-only"
  }).pipe(
    pluck("data", "directChatList"),
    repeatWhen(() => interval(10 * 1000)),
    retry(),
    shareReplay({ bufferSize: 1, refCount: true })
  );

  totalUnseen = this.list.pipe(
    map(list => list.filter((chat) => chat.unseen > 0).length)
  );

  constructor(
    private apollo: Apollo
  ) {
  }
}
