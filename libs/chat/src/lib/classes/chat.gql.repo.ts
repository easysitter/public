import {Apollo, gql} from 'apollo-angular';
import { ChatRepo } from "./chat.repo";

import { ChatEntity } from "./chat.entity";
import { EMPTY, interval, Observable } from "rxjs";
import { Message } from "../message.interface";

import { finalize, map, pluck, switchMapTo, tap } from "rxjs/operators";
import { Injectable, OnDestroy } from "@angular/core";

const MESSAGE = gql`
  fragment Message on Message {
    id
    text
    createdAt
    isOwn
  }
`;

const PARENT_REQUEST_CHAT_QUERY = gql`
  query GetChatForRequest ($request: ID!, $nanny: ID){
    chatForRequest(request: $request, nanny: $nanny) {
      id
    }
  }
`;
const DIRECT_CHAT_QUERY = gql`
  query GetDirectChat ($user: ID!){
    directChat(user: $user) {
      id
    }
  }
`;
const CHAT_MESSAGES_QUERY = gql`
  query GetHistory ($id: String!){
    chat(id: $id) {
      messages {
        ...Message
      }
    }
  }
  ${MESSAGE}
`;
const SEND_MESSAGE_MUTATION = gql`
  mutation sendMessage($chat: String, $user: Int, $text: String ){
    sendMessage(chat: $chat, user: $user, text: $text) {
      ...Message
    }
  }
  ${MESSAGE}
`;
const MARK_ALL_SEEN_MUTATION = gql`
  mutation markAllMessagesSeen($chat: ID!, $lastSeenMessageDate: Date ){
    markAllMessagesSeen(chat: $chat, lastSeenMessageDate: $lastSeenMessageDate)
  }
`;

@Injectable()
export class ChatGqlRepo extends ChatRepo {
  constructor(
    private apollo: Apollo
  ) {
    super();
  }

  addMessage(chat: string, text: string, user: number): void {
    const tmpId = Date.now();
    this.apollo.mutate({
      mutation: SEND_MESSAGE_MUTATION,
      variables: {
        chat: chat.toString(), text, user
      },
      optimisticResponse: {
        __typename: "Mutation",
        sendMessage: {
          __typename: "Message",
          id: tmpId,
          text: text,
          user: user,
          createdAt: new Date(),
          isOwn: true
        }
      },
      update: (proxy, { data: { sendMessage } }) => {
        const data = proxy.readQuery<any>({
          query: CHAT_MESSAGES_QUERY,
          variables: {
            id: chat.toString()
          }
        });
        let messages: any[];
        if (sendMessage.id === tmpId) {
          messages = [...data.chat.messages, sendMessage];
        } else {
          messages = [...data.chat.messages.filter(message => message.id !== tmpId), sendMessage];
        }
        proxy.writeQuery({
          query: CHAT_MESSAGES_QUERY,
          variables: { id: chat.toString() },
          data: {
            ...data,
            chat: {
              ...data.chat,
              messages
            }
          }
        });
      }
    }).subscribe();
  }

  getChatForRequest(request: string, nanny?: string): Observable<ChatEntity> {
    return this.apollo.watchQuery<{ chatForRequest: { id: string } }>({
      query: PARENT_REQUEST_CHAT_QUERY,
      variables: {
        request, nanny
      }
    }).valueChanges.pipe(
      tap((v) => console.log(v)),
      pluck("data", "chatForRequest"),
      map(({ id }) => new ChatEntity(id, this))
    );
  }

  getDirectChat(user: string): Observable<ChatEntity> {
    return this.apollo.watchQuery<{ directChat: { id: string } }>({
      query: DIRECT_CHAT_QUERY,
      variables: {
        user
      }
    }).valueChanges.pipe(
      pluck("data", "directChat"),
      map(({ id }) => new ChatEntity(id, this))
    );
  }

  getById(id: string): ChatEntity {
    return new ChatEntity(id, this);
  }

  getMessages(chatId: string): Observable<Message[]> {
    const queryRef = this.apollo.watchQuery<{ chat: { messages: Message[] } }>({
      query: CHAT_MESSAGES_QUERY,
      variables: {
        id: chatId.toString()
      }
    });
    const refetch = interval(5000).subscribe(() => queryRef.refetch());
    return queryRef.valueChanges.pipe(
      pluck("data", "chat", "messages"),
      finalize(() => {
        refetch.unsubscribe();
      })
    );
  }

  markAllMessagesSeen(chatId: string, lastMessageDate: Date): Observable<void> {
    return this.apollo.mutate({
      mutation: MARK_ALL_SEEN_MUTATION,
      variables: {
        chat: chatId.toString(),
        lastSeenMessageDate: lastMessageDate
      }
    }).pipe(
      switchMapTo(EMPTY)
    );
  }
}
