import { Observable } from "rxjs";
import { DateMessage, Message, MessageTypes, UserMessage } from "../message.interface";
import { map, shareReplay } from "rxjs/operators";
import { ChatRepo } from "./chat.repo";

export class ChatEntity {
  readonly messages$: Observable<Message[]> = this.repo.getMessages(this.id)
    .pipe(
      map(messages => messages.map(setMessageTypeUser)),
      map(messages => {
        return this.addDateMessages(messages || []);
      }),
      shareReplay({ refCount: true, bufferSize: 1 })
    );

  constructor(
    private id: string,
    private repo: ChatRepo
  ) {
  }

  getId() {
    return this.id;
  }

  send(text: string, user: number): void {
    this.repo.addMessage(this.id, text, user);
  }

  private addDateMessages(messages: Message[]): Message[] {
    return messages.reduce((withDates, message): Message[] => {
      return addDateMessage(withDates, message);
    }, []);
  }
}


function addDateMessage(messages, message) {
  if (message.type !== MessageTypes.User) return messages;
  const dateMessage: DateMessage = {
    type: MessageTypes.Date,
    date: message.createdAt
  };
  if (messages.length === 0) {
    return [dateMessage, message];
  }
  const lastMessage = messages[messages.length - 1];

  const lastMessageDate = new Date(lastMessage.createdAt);
  const currentMessageDate = new Date(message.createdAt);
  if (lastMessageDate.getDate() !== currentMessageDate.getDate()) {
    messages.push(dateMessage);
  }
  messages.push(message);
  return [...messages];
}

function setMessageTypeUser(message: Message): UserMessage {
  return {
    ...message,
    type: MessageTypes.User
  } as UserMessage;
}
