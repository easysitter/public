import { Observable } from 'rxjs';
import { Message } from '../message.interface';
import { ChatEntity } from './chat.entity';

export abstract class ChatRepo {

  abstract getChatForRequest(request: string, nanny?: string): Observable<ChatEntity>

  abstract getDirectChat(nanny: string): Observable<ChatEntity>

  abstract getById(id: string): ChatEntity

  abstract getMessages(chatId: string): Observable<Message[]>

  abstract addMessage(chatId: string, text: string, user: number): void

  abstract markAllMessagesSeen(chatId: string, lastMessageDate: Date): Observable<void>
}

