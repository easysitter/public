import {
  Directive,
  EmbeddedViewRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  TemplateRef,
  ViewContainerRef
} from "@angular/core";
import { EMPTY, Observable, Subscription } from "rxjs";
import { Message } from "./message.interface";
import { ChatEntity } from "./classes/chat.entity";
import { ChatRepo } from "./classes/chat.repo";
import { switchMap, tap } from "rxjs/operators";

interface ChatContext {
  $implicit: { messages: Message[], send: (text: string, user: number) => void }
}

@Directive({
  selector: "[chat]"
})
export class ChatDirective implements OnChanges, OnDestroy {
  @Input() chatWith: string | number;
  @Input() chatWithNanny: string | number;
  @Input() chatForRequest: string | number;
  private context: ChatContext = {
    $implicit: null
  };

  private viewRef: EmbeddedViewRef<ChatContext>;
  private subscription: Subscription;
  private chat: ChatEntity;

  constructor(
    private vcr: ViewContainerRef,
    private templateRef: TemplateRef<ChatContext>,
    private repo: ChatRepo
  ) {

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.viewRef) {
      this.vcr.clear();
      this.viewRef.destroy();
      this.viewRef = null;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ("chatForRequest" in changes || "chatWithNanny" in changes || "chatWith" in changes) {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      this.initChat();
    }
  }

  private initChat() {
    let source: Observable<ChatEntity>;

    if (this.chatForRequest) {
      source = this.repo.getChatForRequest(
        this.chatForRequest.toString(),
        this.chatWithNanny && this.chatWithNanny.toString()
      );
    } else if (this.chatWith) {
      source = this.repo.getDirectChat(this.chatWith.toString());
    } else {
      source = EMPTY;
    }
    this.subscription = source.pipe(
      tap((chat) => {
        this.chat = chat;
        this.context.$implicit = {
          messages: [],
          send: (text, user) => {
            this.chat.send(text, user);
          }
        };
      }),
      switchMap(chat => chat.messages$)
    ).subscribe(
      messages => {
        this.context.$implicit.messages = messages;
        if (this.viewRef) {
          this.viewRef.detectChanges();
        } else {
          this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
        }
        this.repo.markAllMessagesSeen(this.chat.getId(), new Date()).subscribe();
      }
    );
  }
}

