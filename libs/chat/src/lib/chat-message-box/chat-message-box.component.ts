import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Subject } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'chat-message-box',
  template: `
      <div class="chat-conversation-content">
          <ng-content></ng-content>
          <div #messageBoxBottom></div>
      </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatMessageBoxComponent implements OnChanges, OnInit, AfterViewInit {
  // @HostBinding('style.background') background = '#4a4a4a';
  private updates = new Subject();
  @Input() messages;
  @ViewChild('messageBoxBottom', { static: true }) bottom: ElementRef<HTMLDivElement>;

  ngOnInit(): void {
    this.updates.pipe(delay(20)).subscribe(
      () => {
        this.bottom.nativeElement.scrollIntoView({ behavior: 'smooth' });
      }
    );
  }

  ngAfterViewInit(): void {
    this.bottom.nativeElement.scrollIntoView({});
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('messages' in changes) {
      this.updates.next(true);
    }
  }
}
