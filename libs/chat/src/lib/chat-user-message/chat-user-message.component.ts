import { Component, Input, OnInit } from '@angular/core';
import { UserMessage } from '../message.interface';

@Component({
  selector: 'chat-user-message',
  templateUrl: './chat-user-message.component.html',
  styleUrls: ['./chat-user-message.component.css']
})
export class ChatUserMessageComponent implements OnInit {
  @Input() message: UserMessage;

  constructor() {
  }

  ngOnInit() {
  }

}
