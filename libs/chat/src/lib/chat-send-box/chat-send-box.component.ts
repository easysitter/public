import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'chat-send-box',
  templateUrl: './chat-send-box.component.html',
  styleUrls: ['./chat-send-box.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatSendBoxComponent {
  @ViewChild('text', { static: true }) text: ElementRef<HTMLInputElement>;
  @Output() send = new EventEmitter();

  sendForm(text) {
    if (!text.trim()) return;
    this.send.emit(text.trim());
    this.text.nativeElement.form.reset();
    this.text.nativeElement.focus();
  }
}
