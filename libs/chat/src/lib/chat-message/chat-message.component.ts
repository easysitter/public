import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Message, MessageTypes } from '../message.interface';

@Component({
  selector: 'chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatMessageComponent {
  @Input() message: Message;
  readonly messageTypes = MessageTypes;
}
