export enum MessageTypes {
  User,
  Date
}
export interface UserMessage {
  id: string;
  text: string;
  createdAt: number;
  type: MessageTypes.User;
  isOwn: boolean;
}

export interface DateMessage {
  date: Date | number;
  type: MessageTypes.Date
}


export type Message = UserMessage | DateMessage;
