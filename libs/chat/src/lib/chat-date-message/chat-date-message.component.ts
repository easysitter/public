import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { DateMessage } from '../message.interface';

@Component({
  selector: 'chat-date-message',
  templateUrl: './chat-date-message.component.html',
  styleUrls: ['./chat-date-message.component.css']
})
export class ChatDateMessageComponent implements OnInit {
  @HostBinding('class')
  className = 'date p-3';

  @Input() message: DateMessage;

  constructor() {
  }

  ngOnInit() {
  }

}
