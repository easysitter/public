import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatDateMessageComponent } from './chat-date-message.component';

describe('ChatDateMessageComponent', () => {
  let component: ChatDateMessageComponent;
  let fixture: ComponentFixture<ChatDateMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatDateMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatDateMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
