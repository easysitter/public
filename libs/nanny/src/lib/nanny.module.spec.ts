import { async, TestBed } from '@angular/core/testing';
import { NannyModule } from './nanny.module';

describe('NannyModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NannyModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(NannyModule).toBeDefined();
  });
});
