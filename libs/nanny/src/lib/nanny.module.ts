import { ModuleWithProviders, NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NannyDirective } from './nanny.directive';
import { NannyPricingComponent } from './nanny-pricing/nanny-pricing.component';
import { LayoutModule } from '@es/layout';
import { NannyProfileService } from './nanny-profile.service';

@NgModule({
  imports: [CommonModule, LayoutModule],
  declarations: [NannyDirective, NannyPricingComponent],
  exports: [NannyDirective, NannyPricingComponent]
})
export class NannyModule {
  static forRoot(options: NannyModuleOptions): ModuleWithProviders<NannyModule> {
    return {
      ngModule: NannyModule,
      providers: [
        { provide: NannyProfileService, useClass: options.useForProfile }
      ]
    };
  }
}

export interface NannyModuleOptions {
  useForProfile: Type<NannyProfileService>
}
