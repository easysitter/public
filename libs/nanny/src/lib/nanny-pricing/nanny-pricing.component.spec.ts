import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NannyPricingComponent } from './nanny-pricing.component';

describe('NannyPricingComponent', () => {
  let component: NannyPricingComponent;
  let fixture: ComponentFixture<NannyPricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NannyPricingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NannyPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
