import { Component, Input, OnInit } from '@angular/core';
import { Price, Trial } from '../interfaces/price.interface';

@Component({
  selector: 'es-nanny-pricing',
  templateUrl: './nanny-pricing.component.html',
  styleUrls: ['./nanny-pricing.component.css']
})
export class NannyPricingComponent implements OnInit {

  @Input()
  price: Price;

  @Input()
  trial: Trial;

  constructor() {
  }

  ngOnInit() {
  }

}
