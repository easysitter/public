import { Directive, EmbeddedViewRef, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { NannyProfile } from './interfaces/nanny-profile.interface';
import { NannyProfileService } from './nanny-profile.service';
import { Subscription } from 'rxjs';

interface NannyDirectiveContext {
  $implicit: NannyProfile
}

@Directive({
  selector: '[esNanny]'
})
export class NannyDirective implements OnInit, OnDestroy {
  @Input()
  esNannyBy: number;

  context: NannyDirectiveContext = {
    $implicit: null
  };
  private viewRef: EmbeddedViewRef<NannyDirectiveContext>;
  private subscription: Subscription;

  constructor(
    private vcr: ViewContainerRef,
    private templateRef: TemplateRef<NannyDirectiveContext>,
    private profile: NannyProfileService
  ) {
  }

  ngOnInit(): void {

    this.subscription = this.profile.load().subscribe((profile) => {
      if (profile) {
        this.context.$implicit = profile;
      }
      if (!this.viewRef) {
        this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
      }
    });
  }


  ngOnDestroy(): void {
    if (this.viewRef) {
      this.vcr.clear();
      this.viewRef.destroy();
      this.viewRef = null;
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }
}
