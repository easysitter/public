import { Price } from './price.interface';

interface ScheduleItem {
  day: number;
  from: string;
  to: string;
}

type Schedule = ScheduleItem[]

export interface NannyProfile {
  id?: number;
  name: string
  birthDate: string
  description: string
  photo: string
  price: Price
  trial: Price
  trialEnabled: boolean
  phone: string
  schedule: Schedule
  ageCategories: number[]
  maxChildrenCount: number
  documents?: Record<string, string>
  features?: Record<string, string>
  districts: string[]
}
