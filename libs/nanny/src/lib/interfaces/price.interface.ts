export interface Price {
  value: number
  currency: string
}

export interface Trial {
  price: Price
  allowed: boolean
}
