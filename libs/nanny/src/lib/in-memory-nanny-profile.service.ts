import { BehaviorSubject, defer, EMPTY, Observable } from 'rxjs';
import { NannyProfile } from './interfaces/nanny-profile.interface';
import { NannyProfileService } from './nanny-profile.service';

const mockProfile: NannyProfile = {
  name: 'Екатерина И.',
  birthDate: '2001-11-02',
  description: '<p>Люблю детей, быстро устанавливаю контакт с ними. Уравновешенный характер, широкий кругозор, высокий уровень\n' +
    '      ответственности.</p>\n' +
    '    <p>Есть опыт работы с детьми в возрасте от 2-х до 10-ти лет. Знания и практические навыки в использовании\n' +
    '      развивающих систем, навыки уборки детской комнаты, ухода за детскими вещами, опыт в приготовлении еды для ребенка,\n' +
    '      опыт совместного выполнения домашних заданий</p>',
  photo: './assets/images/image-bs-profile-ava.jpg',
  price: {
    value: 9,
    currency: 'USD'
  },
  trial: {
    value: 0,
    currency: 'USD'
  },
  trialEnabled: true,
  schedule: [{
    day: 1,
    from: '9:00',
    to: '12:00'
  }, {
    day: 1,
    from: '14:00',
    to: '21:00'
  }, {
    day: 2,
    from: '9:00',
    to: '12:00'
  }, {
    day: 3,
    from: '9:00',
    to: '12:00'
  }],
  phone: '+7 123 456 78 90',
  ageCategories: [0, 1, 2],
  maxChildrenCount: 5,
  districts: []
};

export class InMemoryNannyProfileService extends NannyProfileService {
  profile!: BehaviorSubject<NannyProfile>;

  constructor() {
    super();
    const saved = localStorage.getItem('nanny-profile');
    this.profile = new BehaviorSubject<NannyProfile>(saved ? JSON.parse(saved) as NannyProfile : null);
  }

  load(): Observable<NannyProfile> {
    return this.profile;
  }

  save(data: NannyProfile): Observable<void> {
    return defer(() => {
      localStorage.setItem('nanny-profile', JSON.stringify(data));
      this.profile.next(data as NannyProfile);
      return EMPTY;
    });
  }
}
