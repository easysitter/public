import { Observable } from 'rxjs';
import { NannyProfile } from './interfaces/nanny-profile.interface';

export abstract class NannyProfileService {
  abstract load(): Observable<NannyProfile>;

  abstract save(data: Partial<NannyProfile>): Observable<void>;
}
