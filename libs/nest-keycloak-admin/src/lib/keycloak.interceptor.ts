import { AxiosRequestConfig, AxiosResponse } from 'axios';

export interface KeycloakInterceptor<T extends AxiosResponse | AxiosRequestConfig> {
  register(): void

  unregister(): void

  intercept(value: T): T | Promise<T>
}
