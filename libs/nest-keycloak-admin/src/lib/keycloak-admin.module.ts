import { DynamicModule, HttpModule, Module, OnModuleInit } from '@nestjs/common';
import { KeyCloakAdminAuth } from './keycloak-admin-auth';
import { KeycloakAdminService } from './keycloak-admin.service';
import {
  KEYCLOAK_ADMIN_BASE_URL,
  KEYCLOAK_ADMIN_CLIENT_ID,
  KEYCLOAK_ADMIN_CLIENT_SECRET,
  KEYCLOAK_ADMIN_REALM
} from './providers';
import { KeycloakAuthInterceptor } from './keycloak-auth.interceptor';
import { KeycloakLogInterceptor } from './keycloak-log.interceptor';

@Module({
  imports: [
    HttpModule
  ],
  controllers: [],
  providers: [
    KeyCloakAdminAuth,
    KeycloakAdminService,
    KeycloakLogInterceptor,
    KeycloakAuthInterceptor
  ],
  exports: [
    KeycloakAdminService
  ]
})
export class KeycloakAdminModule implements OnModuleInit {
  constructor(
    private readonly authInterceptor: KeycloakAuthInterceptor,
    private readonly logInterceptor: KeycloakLogInterceptor
  ) {
  }

  static register(config: KeycloakAdminConfig): DynamicModule {
    return {
      module: KeycloakAdminModule,
      providers: [
        { provide: KEYCLOAK_ADMIN_BASE_URL, useValue: config.baseUrl },
        { provide: KEYCLOAK_ADMIN_CLIENT_ID, useValue: config.authClientId },
        { provide: KEYCLOAK_ADMIN_CLIENT_SECRET, useValue: config.authClientSecret },
        { provide: KEYCLOAK_ADMIN_REALM, useValue: config.realm }
      ]
    };
  }

  onModuleInit(): any {
    this.authInterceptor.register();
    this.logInterceptor.register();
  }
}

interface KeycloakAdminConfig {
  baseUrl: string;
  authClientId: string;
  authClientSecret: string;
  realm: string;
}
