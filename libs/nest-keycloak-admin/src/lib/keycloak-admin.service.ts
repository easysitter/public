import { HttpService, Inject, Injectable } from '@nestjs/common';
import { KEYCLOAK_ADMIN_BASE_URL, KEYCLOAK_ADMIN_REALM } from './providers';
import { catchError, map, pluck } from 'rxjs/operators';
import { AxiosError } from 'axios';
import { Observable, throwError } from 'rxjs';

@Injectable()
export class KeycloakAdminService {

  constructor(
    private http: HttpService,
    @Inject(KEYCLOAK_ADMIN_BASE_URL) private BASE_URL: string,
    @Inject(KEYCLOAK_ADMIN_REALM) private realm: string
  ) {
  }

  createUser({ password, clientId, ...userData }: CreateUserDto): Observable<string> {
    return this.http
      .post(`${this.BASE_URL}/admin/realms/${this.realm}/users`, {
        ...userData,
        enabled: true,
        credentials: [{
          type: 'password',
          value: password,
          temporary: false
        }],
        clientConsents: [{
          clientId
        }],
        'requiredActions': ['VERIFY_EMAIL']
      }, {
        headers: {
          Accept: 'application/json'
        }
      })
      .pipe(
        catchError((err: AxiosError) => {
          return throwError(new Error(err.response.data.errorMessage));
        }),
        map(response => {
          const userDetailsUrl: string = response.headers.location;
          return userDetailsUrl.split('/').pop();
        })
      );
  }

  sendVerificationEmail({ user, redirect_uri, client_id }: SendVerificationEmailDto) {
    return this.http.put(
      `${this.BASE_URL}/admin/realms/${this.realm}/users/${user}/execute-actions-email`,
      ['VERIFY_EMAIL'],
      {
        params: {
          client_id,
          redirect_uri
        },
        headers: {
          Accept: 'application/json'
        }
      })
      .pipe(
        pluck('data')
      );
  }
}

interface CreateUserDto {
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  clientId: string;
}

interface SendVerificationEmailDto {
  user: string;
  client_id: string;
  redirect_uri: string;
}

