import { HttpService, Inject, Injectable } from '@nestjs/common';
import { KEYCLOAK_ADMIN_BASE_URL, KEYCLOAK_ADMIN_CLIENT_ID, KEYCLOAK_ADMIN_CLIENT_SECRET } from './providers';
import { pluck, shareReplay } from 'rxjs/operators';
import { addSeconds, isAfter } from 'date-fns';

@Injectable()
export class KeyCloakAdminAuth {
  obtainedAt: Date;
  response: AdminAuthResponse;

  constructor(
    private http: HttpService,
    @Inject(KEYCLOAK_ADMIN_BASE_URL) private BASE_URL: string,
    @Inject(KEYCLOAK_ADMIN_CLIENT_ID) private clientId: string,
    @Inject(KEYCLOAK_ADMIN_CLIENT_SECRET) private clientSecret: string
  ) {
  }

  async getToken() {
    const hasResponse = this.response;
    if (!hasResponse) {
      this.obtainedAt = new Date();
      this.response = await this.obtainAdminToken().toPromise();
    }
    const now = new Date();
    const isAccessExpired = isAfter(now, addSeconds(this.obtainedAt, this.response.expires_in * .75));
    if (isAccessExpired) {
      this.obtainedAt = new Date();
      this.response = await this.obtainAdminToken().toPromise();
    }
    return `${this.response.token_type} ${this.response.access_token}`;
  }

  private obtainAdminToken() {
    const params = new URLSearchParams();
    params.append('client_id', this.clientId);
    params.append('client_secret', this.clientSecret);

    const hasToken = !!this.response;
    if (!hasToken) {
      params.append('grant_type', 'client_credentials');
    } else {
      const isRefreshExpired = hasToken && isAfter(new Date(), addSeconds(this.obtainedAt, this.response.refresh_expires_in * .75));
      if (isRefreshExpired) {
        params.append('grant_type', 'client_credentials');
      } else {
        params.append('grant_type', 'refresh_token');
        params.append('refresh_token', this.response.refresh_token);
      }
    }
    return this.http
      .post<AdminAuthResponse>(`${this.BASE_URL}/realms/master/protocol/openid-connect/token`, params, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).pipe(
        pluck('data'),
        shareReplay({ bufferSize: 1, refCount: false })
      );
  }
}

interface AdminAuthResponse {
  access_token: string;
  refresh_token: string;
  token_type: string;
  expires_in: number;
  refresh_expires_in: number;
}
