import { HttpService, Inject, Injectable } from '@nestjs/common';
import { KeycloakInterceptor } from './keycloak.interceptor';
import { AxiosRequestConfig } from 'axios';
import { KeyCloakAdminAuth } from './keycloak-admin-auth';
import { KEYCLOAK_ADMIN_BASE_URL } from './providers';

@Injectable()
export class KeycloakAuthInterceptor implements KeycloakInterceptor<AxiosRequestConfig> {
  private axiosInterceptorId: number;

  constructor(
    private readonly http: HttpService,
    private readonly auth: KeyCloakAdminAuth,
    @Inject(KEYCLOAK_ADMIN_BASE_URL)
    private readonly keycloakBaseUrl: string
  ) {
  }

  register() {
    this.axiosInterceptorId = this.http.axiosRef.interceptors.request.use(async (req) => {
      return await this.intercept(req);
    });
  }

  unregister() {
    if (this.axiosInterceptorId) {
      this.http.axiosRef.interceptors.request.eject(this.axiosInterceptorId);
      this.axiosInterceptorId = null;
    }
  }

  async intercept(req: AxiosRequestConfig) {
    const isKeycloakRequest = this.isKeycloakRequest(req);
    const isObtainToken = /token$/.test(req.url);
    const isPost = req.method.toLowerCase() === 'post';

    const authorizationRequired = !(isObtainToken && isPost);
    if (isKeycloakRequest && authorizationRequired) {
      req.headers.Authorization = await this.auth.getToken();
    }
    return req;
  }

  private isKeycloakRequest(requestConfig: AxiosRequestConfig) {
    return requestConfig.url.startsWith(this.keycloakBaseUrl);
  }
}
