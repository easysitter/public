import { HttpService, Inject, Injectable } from '@nestjs/common';
import { KeycloakInterceptor } from './keycloak.interceptor';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { KeyCloakAdminAuth } from './keycloak-admin-auth';
import { KEYCLOAK_ADMIN_BASE_URL } from './providers';

@Injectable()
export class KeycloakLogInterceptor implements KeycloakInterceptor<AxiosRequestConfig>, KeycloakInterceptor<AxiosResponse> {
  private axiosRequestInterceptorId: number;
  private axiosResponseInterceptorId: number;

  constructor(
    private readonly http: HttpService,
    private readonly auth: KeyCloakAdminAuth,
    @Inject(KEYCLOAK_ADMIN_BASE_URL)
    private readonly keycloakBaseUrl: string
  ) {
  }


  intercept(requestConfig: AxiosRequestConfig): AxiosRequestConfig | Promise<AxiosRequestConfig>
  intercept(response: AxiosResponse): AxiosResponse | Promise<AxiosResponse>
  intercept(requestConfigOrResponse: AxiosRequestConfig | AxiosResponse): AxiosRequestConfig | Promise<AxiosRequestConfig> | AxiosResponse | Promise<AxiosResponse> {
    if ('config' in requestConfigOrResponse) {
      return this.interceptResponse(requestConfigOrResponse);
    } else {
      return this.interceptRequest(requestConfigOrResponse);
    }
  }

  register(): void {
    this.axiosRequestInterceptorId = this.http.axiosRef.interceptors.request.use(
      (requestConfig) => this.intercept(requestConfig)
    );
    this.axiosResponseInterceptorId = this.http.axiosRef.interceptors.response.use(
      (response) => this.intercept(response)
    );

  }

  unregister(): void {
    if (this.axiosRequestInterceptorId) {
      this.http.axiosRef.interceptors.request.eject(this.axiosRequestInterceptorId);
      this.axiosRequestInterceptorId = null;
    }
    if (this.axiosResponseInterceptorId) {
      this.http.axiosRef.interceptors.response.eject(this.axiosResponseInterceptorId);
      this.axiosResponseInterceptorId = null;
    }
  }

  private interceptRequest(requestConfigOrResponse: AxiosRequestConfig) {
    const requestConfig = requestConfigOrResponse;
    const shouldLog = this.isKeycloakRequest(requestConfig);
    if (shouldLog) {
      const log = this.requestConfigToLog(requestConfig);
      log.context = 'keycloak request';
      this.log(log);
    }
    return requestConfig;
  }

  private interceptResponse(response: AxiosResponse<any>) {
    const shouldLog = this.isKeycloakRequest(response.config);
    if (shouldLog) {
      const log = this.requestConfigToLog(response.config);
      log.context = 'keycloak response';
      log.response = {
        status: response.status,
        data: response.data
      };
      this.log(log);
    }
    return response;
  }

  private isKeycloakRequest(requestConfig: AxiosRequestConfig) {
    return requestConfig.url.startsWith(this.keycloakBaseUrl);
  }

  private requestConfigToLog(req: AxiosRequestConfig) {
    const method = req.method.toUpperCase();
    let query = '';
    if (req.params) {
      query = '?' + new URLSearchParams(Object.entries(req.params)).toString();
    }
    const logData: any = { method, url: `${req.url}${query}` };
    if (/^P(OS|U)T$/i.test(method)) {
      logData.body = req.data;
    }
    return logData;
  }


  private log(log: any) {
    console.log(JSON.stringify(log));
  }
}
