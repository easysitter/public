import { combineLatest, Observable } from 'rxjs';
import { bufferTime, filter, map, pluck, startWith, tap } from 'rxjs/operators';

import { QueryResult, QueryUpdatesBuilder, QueryWatchOptions } from './interfaces';

export class ApolloQueryUpdatesBuilder<T> implements QueryUpdatesBuilder<T> {
  constructor(
    private updatesStream: Observable<T>
  ) {
  }

  watchArrayQuery<R extends Record<K, T[]>, K extends keyof R>(options: QueryWatchOptions<T[], K, R>): Observable<T[]> {
    const updatesStream = this.updatesStream.pipe(
      bufferTime(200),
      filter(updateValues => updateValues.length > 0)
    );
    return this.watch(updatesStream, options);
  }

  watchQuery<R extends QueryResult<K, T>, K extends keyof R>(options: QueryWatchOptions<T, K, R>) {
    const { updatesStream } = this;
    return this.watch(updatesStream, options);
  }

  private watch<R extends QueryResult<K, T | T[]>, K extends keyof R>(
    updatesStream: Observable<T | T[]>,
    options: QueryWatchOptions<T | T[], K, R>
  ) {
    const {
      queryRef,
      key,
      updateStrategy
    } = options;
    const updates = updatesStream.pipe(
      tap(values => {
        queryRef.updateQuery(previousQueryResult => {
          return {
            ...previousQueryResult,
            [key]: updateStrategy.update(previousQueryResult[key], values)
          };
        });
      }),
      startWith(null)
    );
    return combineLatest(
      queryRef.valueChanges.pipe(
        pluck('data', key)
      ),
      updates
    ).pipe(map(([values]) => values));
  }
}
