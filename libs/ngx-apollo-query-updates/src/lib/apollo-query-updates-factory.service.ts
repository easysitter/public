import { Observable } from 'rxjs';
import { QueryUpdatesBuilder, QueryUpdatesFactory } from './interfaces';
import { ApolloQueryUpdatesBuilder } from './apollo-query-updates-builder';

export class ApolloQueryUpdatesFactory extends QueryUpdatesFactory {
  createBuilder<T>(stream: Observable<T>): QueryUpdatesBuilder<T> {
    return new ApolloQueryUpdatesBuilder(stream);
  }
}
