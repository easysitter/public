import {QueryRef} from 'apollo-angular';
import { Observable } from 'rxjs';


export type QueryResult<K extends string | number | symbol, T> = Record<K, T>;

export interface QueryUpdateStrategy<T> {
  update(existed: T, newValue: T)
}

export interface QueryWatchOptions<T, K extends keyof R, R extends QueryResult<K, T>> {
  queryRef: QueryRef<R>
  key: K
  updateStrategy: QueryUpdateStrategy<T>
}

export interface QueryUpdatesBuilder<T> {
  watchArrayQuery<K extends keyof R, R extends QueryResult<K, T[]>>(options: QueryWatchOptions<T[], K, R>): Observable<T[]>;

  watchQuery<K extends keyof R, R extends QueryResult<K, T>>(options: QueryWatchOptions<T, K, R>): Observable<T>;
}

export abstract class QueryUpdatesFactory {
  abstract createBuilder<T>(stream: Observable<T>): QueryUpdatesBuilder<T>;
}
