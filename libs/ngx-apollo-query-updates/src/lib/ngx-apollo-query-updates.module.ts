import { ModuleWithProviders, NgModule, Type } from '@angular/core';
import { QueryUpdatesFactory } from './interfaces';
import { ApolloQueryUpdatesFactory } from './apollo-query-updates-factory.service';

@NgModule()
export class NgxApolloQueryUpdatesModule {
  static forRoot(
    options?: Options
  ): ModuleWithProviders<NgxApolloQueryUpdatesModule> {
    return {
      ngModule: NgxApolloQueryUpdatesModule,
      providers: [
        { provide: QueryUpdatesFactory, useClass: (options && options.useClass) || ApolloQueryUpdatesFactory }
      ]
    };
  }
}

interface Options {
  useClass: Type<QueryUpdatesFactory>
}
