import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { ShortProfileComponent } from './short-profile/short-profile.component';
import { ProfilePhotoComponent } from './profile-photo/profile-photo.component';
import { LayoutModule } from '@es/layout';

@NgModule({
  imports: [CommonModule, LayoutModule],
  exports: [
    ProfileComponent,
    ShortProfileComponent,
    ProfilePhotoComponent
  ],
  declarations: [
    ProfileComponent,
    ShortProfileComponent,
    ProfilePhotoComponent
  ]
})
export class ProfileUiModule {
}
