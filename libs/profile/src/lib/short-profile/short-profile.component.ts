import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'es-short-profile',
  templateUrl: './short-profile.component.html',
  styleUrls: ['./short-profile.component.css']
})
export class ShortProfileComponent implements OnInit {
  @Input()
  photo: string;
  @Input()
  name: string;

  constructor() {
  }

  ngOnInit() {
  }

}
