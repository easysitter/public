import { Component, ElementRef, HostBinding, Input, OnChanges, OnInit, Renderer2, SimpleChanges } from '@angular/core';

@Component({
  selector: 'es-profile-photo',
  templateUrl: './profile-photo.component.html',
  styleUrls: ['./profile-photo.component.css']
})
export class ProfilePhotoComponent implements OnChanges, OnInit {
  @HostBinding('style.background-image')
  background;

  @Input()
  size: 'small' | 'normal' | 'large' = 'normal';

  @Input()
  name: string;

  @Input()
  set src(value) {
    this.background = 'url(' + value + ')';
  }

  constructor(
    private readonly elRef: ElementRef<HTMLElement>,
    private readonly rendrer: Renderer2
  ) {
  }

  get element() {
    return this.elRef.nativeElement;
  }

  ngOnInit(): void {
    this.setupSizes();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('size' in changes) {
      this.cleanupSize();
      this.setupSizes();
    }
  }

  private cleanupSize() {
    Array.from({ length: 20 }, (_, i) => `userpic-round-${i}`).forEach(
      cls => this.rendrer.removeClass(this.element, cls)
    );
  }

  private setupSizes() {
    let newClass;
    switch (this.size) {
      case 'large':
        newClass = 'userpic-round-19';
        break;
      case 'normal':
        newClass = 'userpic-round-15';
        break;
      case 'small':
        newClass = 'userpic-round-9';
        break;
      default:
        newClass = 'userpic-round-15';
    }
    this.rendrer.addClass(this.element, newClass);
  }
}
