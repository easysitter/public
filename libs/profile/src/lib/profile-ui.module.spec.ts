import { async, TestBed } from '@angular/core/testing';
import { ProfileUiModule } from './profile-ui.module';

describe('ProfileUiModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ProfileUiModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ProfileUiModule).toBeDefined();
  });
});
