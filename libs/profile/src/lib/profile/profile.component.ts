import { Component, Input, OnInit } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'es-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @Input()
  background: string;

  @Input()
  name: string;

  @Input()
  photo: string | SafeResourceUrl;

  constructor() { }

  ngOnInit() {
  }

}
