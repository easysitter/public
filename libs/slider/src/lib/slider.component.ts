import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  EventEmitter,
  OnDestroy,
  Output,
  QueryList
} from '@angular/core';
import { SlideDirective } from './slide.directive';
import { Subscription } from 'rxjs';

@Component({
  selector: 'es-slider',
  template: `
    <div class="f-grow-1 welcome-slider" (swipeleft)="next()" (swiperight)="prev()">
      <ng-container *ngTemplateOutlet="slides[currentSlideIdx].template"></ng-container>
    </div>
    <div class="p-4 pt-5">
      <div class="flex justify-between align-center">
        <div><span class="text-secondary fw-500" (click)="prev()">Назад</span></div>
        <div>
          <div class="slider-dots">
                      <span *ngFor="let slide of slides; let i = index" class="dot"
                            [class.active]="i===currentSlideIdx"></span>
          </div>
          </div>
          <div><span class="text-secondary fw-500" [style.opacity]="last?0:1" (click)="next()">Далее</span></div>
        </div>
      </div>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SliderComponent implements AfterContentInit, OnDestroy {
  @ContentChildren(SlideDirective) slidesQuery: QueryList<SlideDirective>;
  @Output() navigated = new EventEmitter<{ idx: number, last: boolean }>();

  slides: SlideDirective[];

  currentSlideIdx = 0;
  private subscription: Subscription;

  constructor() {
  }

  get last() {
    return this.currentSlideIdx === this.slides.length - 1;
  }

  get first() {
    return this.currentSlideIdx === 0;
  }

  ngAfterContentInit(): void {
    this.slides = this.slidesQuery.toArray();
    this.subscription = this.slidesQuery.changes.subscribe((slides) => this.slides = slides);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  next() {
    const idx = Math.min(this.currentSlideIdx + 1, this.slides.length - 1);
    this.setCurrentSlide(idx);
  }

  prev() {
    const idx = Math.max(this.currentSlideIdx - 1, 0);
    this.setCurrentSlide(idx);
  }

  private setCurrentSlide(idx) {
    this.currentSlideIdx = idx;
    this.navigated.emit({ idx: this.currentSlideIdx, last: this.last });
  }
}
