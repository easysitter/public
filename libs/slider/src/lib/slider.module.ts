import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './slider.component';
import { SlideDirective } from './slide.directive';


@NgModule({
  declarations: [SliderComponent, SlideDirective],
  imports: [
    CommonModule
  ], exports: [
    SliderComponent,
    SlideDirective
  ]
})
export class SliderModule {
}
