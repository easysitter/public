import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[esSlide]'
})
export class SlideDirective {

  constructor(public readonly template: TemplateRef<any>) {
  }

}
