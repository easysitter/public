import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';

export interface ReviewFormResult {
  rating: number;
  pros: string;
  cons: string;
}

@Component({
  selector: 'es-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.css']
})
export class ReviewFormComponent implements OnInit {

  @Output()
  save = new EventEmitter<ReviewFormResult>();

  form = this.formBuilder.group({
    rating: 0,
    pros: '',
    cons: ''
  });

  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
  }

  handleSubmit() {
    if (!this.form.valid) return;
    this.save.emit(this.form.value);
  }
}
