import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingInputAccessor } from './rating-input-accessor.component';

describe('ReviewInputComponent', () => {
  let component: RatingInputAccessor;
  let fixture: ComponentFixture<RatingInputAccessor>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RatingInputAccessor]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingInputAccessor);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
