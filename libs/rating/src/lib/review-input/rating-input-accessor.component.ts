import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'es-rating-input',
  templateUrl: './rating-input-accessor.component.html',
  styleUrls: ['./rating-input-accessor.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RatingInputAccessor),
      multi: true
    }
  ]
})
export class RatingInputAccessor implements OnInit, ControlValueAccessor {
  readonly stars = [
    { star: 1 },
    { star: 2 },
    { star: 3 },
    { star: 4 },
    { star: 5 }
  ];
  private disabled: boolean;
  private value = 0;

  constructor() {
  }

  ngOnInit() {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(value: number): void {
    this.value = value;
  }

  setRating(value) {
    if (!this.disabled) {
      this.value = value;
      this.onChange(value);
      this.onTouched();
    }
  }

  private onChange: any = () => null;

  private onTouched: any = () => null;
}
