import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatingComponent } from './rating/rating.component';
import { LayoutModule } from '@es/layout';
import { RatingInputAccessor } from './review-input/rating-input-accessor.component';
import { ReviewFormComponent } from './review-form/review-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, LayoutModule, ReactiveFormsModule],
  declarations: [
    RatingComponent,
    RatingInputAccessor,
    ReviewFormComponent
  ],
  exports: [
    RatingComponent,
    RatingInputAccessor,
    ReviewFormComponent
  ]
})
export class RatingModule {
}
