import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'es-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {
  value: number;
  @Input()
  showText = false;
  stars: { starred: boolean }[] = [];

  constructor() {
  }

  @Input()
  set rating(value) {
    this.value = value;
    this.stars = Array.from({ length: 5 }, (_, idx) => ({ starred: Math.round(value - idx) > 0 }));
  }

  ngOnInit() {
  }

}
