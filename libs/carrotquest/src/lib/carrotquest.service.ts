import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class CarrotquestService {
  private initialized = false;
  private initializing = false;

  constructor(
    @Inject(DOCUMENT)
    private readonly document: Document
  ) {}

  private get carrotquest() {
    return (window as any).carrotquest;
  }

  async init(carrotId: string) {
    if (this.initialized || this.initializing) return;
    this.initializing = true;
    const script = this.document.createElement('script');
    const content = `
!function(){function t(t,e){return function(){window.carrotquestasync.push(t,arguments)}}if("undefined"==typeof carrotquest){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="//cdn.carrotquest.app/api.min.js",document.getElementsByTagName("head")[0].appendChild(e),window.carrotquest={},window.carrotquestasync=[],carrotquest.settings={};for(var n=["connect","track","identify","auth","oth","onReady","addCallback","removeCallback","trackMessageInteraction"],a=0;a<n.length;a++)carrotquest[n[a]]=t(n[a])}}(),carrotquest.connect("${carrotId}");
`;
    script.innerHTML = content;
    this.document.head.appendChild(script);
    return new Promise((resolve) => {
      this.carrotquest.onReady(() => {
        this.initialized = true;
        this.initializing = false;
        this.carrotquest.removeChat();
        resolve(undefined);
      });
    });
  }

  openChat() {
    this.carrotquest.open();
  }
}
