import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarrotquestService } from './carrotquest.service';
import { OpenCarrotChatDirective } from './open-carrot-chat.directive';

export function carrotInitializer(carrot: CarrotquestService, CARROT_ID: string) {
  return () => carrot.init(CARROT_ID);
}

@NgModule({
  imports: [CommonModule],
  declarations: [OpenCarrotChatDirective],
  exports: [OpenCarrotChatDirective]
})
export class CarrotquestModule {
  static forRoot(carrotId): ModuleWithProviders<CarrotquestModule> {
    return {
      ngModule: CarrotquestModule,
      providers: [{
        provide: 'CARROT_ID',
        useValue: carrotId
        // }, {
        //   provide: APP_INITIALIZER,
        //   multi: true,
        //   deps: [CarrotquestService, 'CARROT_ID'],
        //   useFactory: carrotInitializer
      }]
    };
  }
}

