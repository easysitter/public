import { Directive, HostListener, Inject } from '@angular/core';
import { CarrotquestService } from './carrotquest.service';

@Directive({
  selector: '[carrotOpenChat]'
})
export class OpenCarrotChatDirective {

  constructor(
    private carrot: CarrotquestService,
    @Inject('CARROT_ID')
    private carrotId: string
  ) {
  }

  @HostListener('click')
  async onClick() {
    console.log(this.carrotId);
    await this.carrot.init(this.carrotId);
    this.carrot.openChat();
  }
}
