FROM node:12-alpine as builder
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm ci
COPY . .
RUN npm run build parent-application -- --prod


### STAGE 2: Setup ###
FROM nginx:stable-alpine

## Copy our default nginx config
COPY apps/parent-application/nginx/default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /usr/src/app/dist/apps/parent-application /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]

