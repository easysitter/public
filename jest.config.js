const { getJestProjects } = require('@nrwl/jest');

module.exports = {
  projects: [
    ...getJestProjects(),
    '<rootDir>/apps/nanny-webapi',
    '<rootDir>/libs/profile',
    '<rootDir>/libs/rating',
    '<rootDir>/libs/layout',
    '<rootDir>/libs/nanny',
    '<rootDir>/libs/requests',
    '<rootDir>/apps/nanny-application',
    '<rootDir>/libs/slider',
    '<rootDir>/libs/children',
    '<rootDir>/libs/ngx-apollo-query-updates',
    '<rootDir>/apps/parent-webapi',
    '<rootDir>/libs/services-sdk',
    '<rootDir>/libs/forms',
    '<rootDir>/libs/custom-date',
    '<rootDir>/libs/authentication',
    '<rootDir>/libs/carrotquest',
    '<rootDir>/libs/videochat',
    '<rootDir>/libs/nest-keycloak-admin',
    '<rootDir>/libs/calculator',
    '<rootDir>/libs/notifications/core',
    '<rootDir>/libs/notifications/web',
    '<rootDir>/libs/notifications/native',
  ],
};
