FROM node:12-alpine AS deps

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm ci
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .
EXPOSE 3000


FROM deps as nanny-webapi-dev
CMD [ "npm", "run", "nx", "serve", "nanny-webapi" ]

FROM deps as nanny-webapi-build
RUN ["npm", "run", "build", "nanny-webapi"]
RUN ["npm", "prune", "--production"]

FROM node:12-alpine AS nanny-webapi
WORKDIR /usr/src/app
COPY package*.json ./
COPY --from=nanny-webapi-build /usr/src/app/node_modules ./dist/apps/node_modules
COPY --from=nanny-webapi-build /usr/src/app/dist/apps/nanny-webapi ./dist/apps/nanny-webapi
WORKDIR /usr/src/app/dist/apps/nanny-webapi
CMD [ "node", "main.js" ]
