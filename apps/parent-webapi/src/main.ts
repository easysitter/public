/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/

import { NestFactory } from '@nestjs/core';
import * as Keycloak from 'keycloak-connect';

import { AppModule } from './app/app.module';
import { KC_INSTANCE } from './app/providers';
import { Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const keycloak = app.get<Keycloak>(KC_INSTANCE);
  app.use(keycloak.middleware());

  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  const port = process.env.port || 3333;
  app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        retry: {
          retries: 3
        },
        connectionTimeout: 5000,
        brokers: process.env.KAFKA_BROKERS.split(','),
        ...(process.env.KAFKA_SASL_USER
            ? {
              sasl: {
                mechanism: 'plain',
                username: process.env.KAFKA_SASL_USER,
                password: process.env.KAFKA_SASL_PASSWORD
              },
              ssl: true
            }
            : {}
        )
      },
      consumer: {
        groupId: 'parent-webapi'
      }
    }
  });
  await app.listen(port, () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    console.log('Listening at http://localhost:' + port + '/graphql');
    app.startAllMicroservicesAsync();
  });
}

bootstrap();
