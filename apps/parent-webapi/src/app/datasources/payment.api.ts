import { RESTDataSource } from 'apollo-datasource-rest';

export class PaymentApi extends RESTDataSource {

  constructor(
    baseURL: string
  ) {
    super();
    this.baseURL = baseURL;
  }

  async getDealsPayments(user) {
    const response = await this.get<any[]>(`users/${user}/deals`);

    return response.map(mapResponse);
  }

  async getDealPayments(user, deal) {
    const response = await this.get<any[]>(`users/${user}/deals/${deal}`);

    return response.map(mapResponse);
  }

  async getDealPendingPayments(user, deal) {
    const response = await this.get<any[]>(`users/${user}/deals/${deal}/pending`);

    return mapResponse(response);
  }
}


function mapResponse(paymentData) {
  if (!paymentData) return null;
  return {
    id: paymentData.id as string,
    amount: paymentData.amount as number,
    currency: paymentData.currency as string,
    isCompleted: paymentData.isCompleted as boolean,
    checkoutUrl: paymentData.meta.redirect_uri as string
  };
}
