import { ChatApi, NannyProfileApi, NannyRequestsApi, ParentRequestsApi, SearchApi } from "@services-sdk";

import { PaymentApi } from './payment.api';

export interface DataSources {
  nanny: NannyProfileApi;
  parentRequests: ParentRequestsApi;
  nannyRequests: NannyRequestsApi;
  search: SearchApi;
  chat: ChatApi;
  paymentApi: PaymentApi;
}
