import { ResolveField, Resolver } from "@nestjs/graphql";

@Resolver("ChatUser")
export class ChatUserResolver {

  @ResolveField()
  __resolveType(value) {
    switch (value.type) {
      case "parent":
        return "ChatUserParent";
      case  "nanny":
        return "ChatUserNanny";
      default:
        return null;
    }
  }
}
