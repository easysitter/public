import { Args, Context, Mutation, Parent, Query, ResolveField, Resolver, Subscription } from "@nestjs/graphql";
import { Inject, ParseIntPipe, UseGuards } from "@nestjs/common";
import { PubSub } from "graphql-subscriptions";
import { PUB_SUB } from "../graphql-subscriptions.module";
import { AuthGuard } from "../../auth.guard";
import { GqlContext } from "../../datasources/context";
import { DataSources } from "../../datasources/datasources";

export function noop(value) {
  return value;
}

@Resolver("Chat")
export class ChatResolver {
  constructor(
    @Inject(PUB_SUB) private pubSub: PubSub
  ) {
  }

  @Query("chat")
  @UseGuards(AuthGuard)
  async getChat(
    @Args("id") id: string,
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return dataSources.chat.getById(id);
  }

  @Query("chatForRequest")
  @UseGuards(AuthGuard)
  async getChatForRequest(
    @Args("request", ParseIntPipe) request: number,
    @Args("nanny") nanny: string,
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return dataSources.chat.findForRequest({
      nanny: `n${nanny}`,
      request
    });
  }

  @Query("directChat")
  @UseGuards(AuthGuard)
  async getDirectChatWith(
    @Args("user") nanny: string,
    @Context() { dataSources, profileId }: GqlContext<DataSources>
  ) {
    return dataSources.chat.findDirect({
      nanny: `n${nanny}`,
      parent: `p${profileId}`
    });
  }


  @Query("directChatList")
  @UseGuards(AuthGuard)
  async getDirectChatList(
    @Args("user") nanny: string,
    @Context() { dataSources, profileId }: GqlContext<DataSources>
  ) {
    return dataSources.chat.findAllDirect(`p${profileId}`);
  }


  @ResolveField("messages")
  getMessages(
    @Parent() chat,
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return chat ? dataSources.chat.getMessages(chat.id) : [];
  }

  @ResolveField("unseen")
  getUnseenCount(
    @Parent() chat,
    @Context() { dataSources, profileId }: GqlContext<DataSources>
  ) {
    return chat ? dataSources.chat.getUnseenCount(chat.id, `p${profileId}`) : 0;
  }

  @ResolveField("users")
  users(
    @Parent() chat
  ) {
    return chat ? chat.users.map(
      (userId: string) => {
        if (userId.startsWith("p")) {
          return {
            type: "parent",
            id: userId.slice(1)
          };
        }
        if (userId.startsWith("n")) {
          return {
            type: "nanny",
            id: userId.slice(1)
          };
        }
        return {
          id: userId,
          type: ""
        };
      }
    ) : [];
  }

  @Mutation("sendMessage")
  @UseGuards(AuthGuard)
  sendMessage(
    @Args("text") text: string,
    @Args("chat") room: string,
    @Context() { profileId, dataSources }: GqlContext<DataSources>
  ) {
    return dataSources.chat.sendMessage(room, text, `p${profileId}`);
  }

  @Mutation("markAllMessagesSeen")
  @UseGuards(AuthGuard)
  markAllMessagesSeen(
    @Args("chat") room: string,
    @Args("lastSeenMessageDate") lastSeenMessageDate: Date,
    @Context() { profileId, dataSources }: GqlContext<DataSources>
  ) {
    return dataSources.chat.updateUnseen(room, `p${profileId}`, lastSeenMessageDate);
  }

  @Subscription("newMessage", {
    resolve: ({ message }) => {
      return message;
    },
    filter: (payload, variables) => {
      return payload.chat.id.toString() === variables.chat.toString();
    }
  })
  newMessage() {
    return this.pubSub.asyncIterator("message_sent");
  }
}
