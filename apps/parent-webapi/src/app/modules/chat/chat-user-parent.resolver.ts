import { Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { ParentService } from "@services-sdk";

@Resolver("ChatUserParent")
export class ChatUserParentResolver {

  constructor(
    private profileService: ParentService,
  ) {
  }
  @ResolveField()
  profile(
    @Parent() user
  ) {
    return this.profileService.getById(user.id);
  }
}
