import { Parent, ResolveField, Resolver } from "@nestjs/graphql";

@Resolver("ChatUserNanny")
export class ChatUserNannyResolver {

  @ResolveField()
  profile(
    @Parent() user
  ) {
    return { id: user.id };
  }
}
