import { Controller, Inject } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { PubSub } from 'graphql-subscriptions';
import { PUB_SUB } from '../graphql-subscriptions.module';

@Controller('/chat')
export class ChatController {
  constructor(
    @Inject(PUB_SUB) private pubSub: PubSub
  ) {
  }

  @EventPattern('chat')
  async chatEventHandler({ key, value }) {
    switch (key) {
      case 'message':
        await this.pubSub.publish('message_sent', value);
        break;
      case 'created':
        await this.pubSub.publish('chat_created', value);
        break;
    }
  }
}
