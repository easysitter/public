import { DynamicModule, Global, Module } from '@nestjs/common';
import { KC_INSTANCE, KC_SUBSCRIPTION_HANDLER } from '../providers';
import * as Keycloak from 'keycloak-connect';
import { KeycloakSubscriptionHandler } from 'keycloak-connect-graphql';

@Global()
@Module({})
export class KeycloakModule {
  static forRoot(config): DynamicModule {
    return {
      module: KeycloakModule,
      providers: [
        {
          provide: KC_INSTANCE,
          useFactory: () => new Keycloak({}, config),
          inject: []
        },
        {
          provide: KC_SUBSCRIPTION_HANDLER,
          useFactory: (keycloak) => new KeycloakSubscriptionHandler({ keycloak, protect: true }),
          inject: [KC_INSTANCE]
        }
      ],
      exports: [
        KC_INSTANCE,
        KC_SUBSCRIPTION_HANDLER
      ]
    };
  }
}
