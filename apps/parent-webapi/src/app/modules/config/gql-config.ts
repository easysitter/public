import { GqlModuleOptions, GqlOptionsFactory } from '@nestjs/graphql';
import { HttpService, Inject } from '@nestjs/common';
import { KC_SUBSCRIPTION_HANDLER } from '../../providers';
import { join, resolve } from 'path';
import {
  KeycloakContext,
  KeycloakSubscriptionContext,
} from 'keycloak-connect-graphql';
import { catchError, pluck, tap } from 'rxjs/operators';
import {
  CHAT_SERVICE_BASE_URL,
  NANNY_PROFILE_SERVICE_BASE_URL,
  PARENT_PROFILE_SERVICE_BASE_URL,
  PAYMENTS_SERVICE_BASE_URL,
  REQUESTS_SERVICE_BASE_URL,
  SEARCH_SERVICE_BASE_URL,
} from './providers';
import { of } from 'rxjs';
import {
  ChatApi,
  NannyProfileApi,
  NannyRequestsApi,
  ParentRequestsApi,
  SearchApi,
} from '@services-sdk';
import { PaymentApi } from '../../datasources/payment.api';

export class GqlConfig implements GqlOptionsFactory {
  private cacheMap: Map<string, number> = new Map<string, number>();

  constructor(
    @Inject(KC_SUBSCRIPTION_HANDLER)
    private keycloakSubscriptionHandler,
    @Inject(PARENT_PROFILE_SERVICE_BASE_URL)
    private PROFILE_SERVICE_BASE_URL: string,
    @Inject(NANNY_PROFILE_SERVICE_BASE_URL)
    private NANNY_SERVICE_BASE_URL: string,
    @Inject(REQUESTS_SERVICE_BASE_URL)
    private REQUESTS_BASE_URL: string,
    @Inject(SEARCH_SERVICE_BASE_URL)
    private readonly SEARCH_BASE_URL: string,
    @Inject(CHAT_SERVICE_BASE_URL)
    private CHAT_BASE_URL: string,
    @Inject(PAYMENTS_SERVICE_BASE_URL)
    private readonly PAYMENTS_BASE_URL: string,
    private http: HttpService
  ) {}

  createGqlOptions(): Promise<GqlModuleOptions> | GqlModuleOptions {
    return {
      typePaths: [
        process.env.SCHEMA_PATH
          ? resolve(process.env.SCHEMA_PATH)
          : join(__dirname, '../../../gql/*.graphql'),
      ],
      installSubscriptionHandlers: true,
      uploads: {
        maxFiles: 5,
      },
      context: async ({ req, connection }) => {
        if (connection) {
          return connection.context;
        }
        const kauth = new KeycloakContext({ req });
        let profileId = null;
        if (
          kauth &&
          kauth.accessToken &&
          (kauth.accessToken.content as any).sub
        ) {
          profileId = await this.getProfileId(
            (kauth.accessToken.content as any).sub
          );
        }
        return {
          kauth,
          profileId: profileId,
        };
      },
      subscriptions: {
        onConnect: async (connectionParams) => {
          const token =
            await this.keycloakSubscriptionHandler.onSubscriptionConnect(
              connectionParams,
              null,
              null
            );
          const kauth = new KeycloakSubscriptionContext(token);
          let profileId = null;
          if (
            kauth &&
            kauth.accessToken &&
            (kauth.accessToken.content as any).sub
          ) {
            profileId = await this.getProfileId(
              (kauth.accessToken.content as any).sub
            );
          }
          return {
            kauth,
            profileId: profileId,
          };
        },
      },
      dataSources: () => {
        return {
          chat: new ChatApi(this.CHAT_BASE_URL),
          paymentApi: new PaymentApi(this.PAYMENTS_BASE_URL),
          nanny: new NannyProfileApi(this.NANNY_SERVICE_BASE_URL),
          parentRequests: new ParentRequestsApi(this.REQUESTS_BASE_URL),
          nannyRequests: new NannyRequestsApi(this.REQUESTS_BASE_URL),
          search: new SearchApi(this.SEARCH_BASE_URL),
        };
      },
    };
  }

  private async getProfileId(uid: string): Promise<number> {
    if (this.cacheMap.has(uid)) {
      return this.cacheMap.get(uid);
    }
    return this.http
      .get(`${this.PROFILE_SERVICE_BASE_URL}/user/${uid}`)
      .pipe(
        pluck('data'),
        tap((id) => this.cacheMap.set(uid, id)),
        catchError(() => of(null))
      )
      .toPromise();
  }
}
