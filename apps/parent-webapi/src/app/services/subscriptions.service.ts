import { HttpService, Inject, Injectable } from "@nestjs/common";
import { PAYMENTS_SERVICE_BASE_URL } from "../modules/config/providers";
import { pluck } from "rxjs/operators";

/**
 * @deprecated
 */
@Injectable()
export class SubscriptionsService {
  constructor(
    @Inject(PAYMENTS_SERVICE_BASE_URL) private readonly BASE_URL: string,
    private http: HttpService
  ) {
  }

  async createSubscription(profileId, returnUrl) {
    return await this.http
      .post<{ payment: { meta: { redirect_uri: string } } }>(`${this.BASE_URL}/subscriptions`, {
        userId: profileId,
        returnUrl
      })
      .pipe(
        pluck('data')
      )
      .toPromise();
  }

  async getByUser(profileId: number) {
    return await this.http
      .get<{ isActive: boolean }>(`${this.BASE_URL}/subscriptions/users/${profileId}`)
      .pipe(
        pluck('data')
      )
      .toPromise();
  }
}
