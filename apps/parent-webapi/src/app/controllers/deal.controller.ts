import { EventPattern } from '@nestjs/microservices';
import { Controller, Inject } from '@nestjs/common';
import { PUB_SUB } from '../modules/graphql-subscriptions.module';
import { PubSub } from 'graphql-subscriptions';

@Controller()
export class DealController {
  constructor(
    @Inject(PUB_SUB) private pubsub: PubSub
  ) {
  }

  @EventPattern('deal_concluded')
  async onDealConcluded({ value }) {
    const { id } = value;
    try {
      await this.publishStatusUpdate({
        id
      });
    } catch (e) {
      console.error(e);
    }
  }

  @EventPattern('offer_created')
  async onOfferCreated({ value }) {
    const { deal } = value;
    try {
      await this.publishStatusUpdate({ id: deal });
    } catch (e) {
      console.error(e);
    }
  }

  @EventPattern('visit_started')
  async onVisitStarted({ value }) {
    try {
      await this.publishStatusUpdate(value);
    } catch (e) {
      console.error(e);
    }
  }

  @EventPattern('visit_finished')
  async onVisitFinished({ value }) {
    try {
      await this.publishStatusUpdate(value);
    } catch (e) {
      console.error(e);
    }
  }

  private async publishStatusUpdate(deal) {
    return await this.pubsub.publish(
      'dealStatusUpdate',
      deal
    );
  }
}
