import { EventPattern } from '@nestjs/microservices';
import { Controller, Inject } from '@nestjs/common';
import { PUB_SUB } from '../modules/graphql-subscriptions.module';
import { PubSub } from 'graphql-subscriptions';

@Controller()
export class RequestController {
  constructor(
    @Inject(PUB_SUB) private pubsub: PubSub
  ) {
  }

  @EventPattern('payments')
  async onPayment({ key, value }) {
    const { user } = value;
    if (key === 'subscription_activated') {
      try {
        await this.pubsub.publish('subscription_activated', { user });
      } catch (e) {
        console.error(e);
      }
    }
  }

  @EventPattern('nrequest_status_updated')
  async onRequestStatusUpdate({ value }) {
    const { request: id, status } = value;
    try {
      await this.publishStatusUpdate({ id, status });
    } catch (e) {
      console.error(e);
    }
  }

  private async publishStatusUpdate(request) {
    return await this.pubsub.publish(
      'requestsStatusUpdate',
      request
    );
  }
}
