import { HttpModule, Logger, Module } from "@nestjs/common";

import { GraphQLModule } from "@nestjs/graphql";
import { DateScalar } from "./date.scalar";
import { JsonScalar } from "./json.scalar";
import { SearchResolver } from "./resolvers/search.resolver";
import { ParentRequestsResolver } from "./resolvers/parent-requests.resolver";
import { NannyRequestResolver } from "./resolvers/nanny-request.resolver";
import { AuthGuard } from "./auth.guard";
import { ParentResolver } from "./resolvers/parent.resolver";
import { NannyResolver } from "./resolvers/nanny.resolver";
import { DealsResolver } from "./resolvers/deals.resolver";
import { ConfigModule } from "./modules/config/config.module";
import { GqlConfig } from "./modules/config/gql-config";
import { GraphqlSubscriptionsModule } from "./modules/graphql-subscriptions.module";
import { KeycloakModule } from "./modules/keycloak.module";
import { RequestController } from "./controllers/request.controller";
import { ChatModule } from "./modules/chat/chat.module";
import { FilesStorageService, ServicesSdkModule } from "@services-sdk";
import { DealController } from "./controllers/deal.controller";
import { SearchResultNannyResolver } from "./resolvers/search-result-nanny.resolver";
import { SubscriptionsService } from "./services/subscriptions.service";
import { UploadsResolver } from "./resolvers/uploads.resolver";
import { VideochatResolver } from './resolvers/videochat.resolver';
import { PaymentResolver } from "./resolvers/payment.resolver";
import { KeycloakAdminModule } from '@es/nest-keycloak-admin';
import { AuthResolver } from './resolvers/auth.resolver';

@Module({
  imports: [
    ConfigModule,
    KeycloakModule.forRoot({
      "realm": process.env.KEYCLOAK_REALM,
      "bearer-only": true,
      "auth-server-url": process.env.KEYCLOAK_AUTH_SERVER_URL,
      "ssl-required": "none",
      "resource": process.env.KEYCLOAK_RESOURCE,
      "verify-token-audience": true,
      "use-resource-role-mappings": true,
      "confidential-port": 0
    }),
    GraphQLModule.forRootAsync({
      useExisting: GqlConfig
    }),
    GraphqlSubscriptionsModule,
    HttpModule,
    KeycloakAdminModule.register({
      baseUrl: process.env.KEYCLOAK_AUTH_SERVER_URL,
      realm: process.env.KEYCLOAK_REALM,
      authClientId: process.env.KEYCLOAK_ADMIN_CLIENT_ID,
      authClientSecret: process.env.KEYCLOAK_ADMIN_CLIENT_SECRET
    }),
    ChatModule,
    ServicesSdkModule.forRoot({
      baseUrlsConfig: {
        chat: process.env.CHAT_SERVICE_BASE_URL || "http://localhost:3334/api",
        requests: process.env.REQUESTS_SERVICE_BASE_URL || "http://localhost:3335/api",
        deals: process.env.DEALS_SERVICE_BASE_URL || "http://localhost:3336/api",
        search: process.env.SEARCH_SERVICE_BASE_URL || "http://localhost:3337/api",
        nannyProfile: process.env.NANNY_PROFILE_SERVICE_BASE_URL || "http://localhost:3338/api",
        parentProfile: process.env.PARENT_PROFILE_SERVICE_BASE_URL || "http://localhost:3339/api",
        reviews: process.env.REVIEWS_SERVICE_BASE_URL || "http://localhost:3340/api",
        notifications: process.env.NOTIFICATIONS_SERVICE_BASE_URL || "http://localhost:3341/api",
        address: process.env.ADDRESS_SERVICE_BASE_URL || "http://localhost:3343"
      }
    })
  ],
  controllers: [
    DealController,
    RequestController
  ],
  providers: [
    Logger,
    AuthGuard,
    DateScalar,
    JsonScalar,
    AuthResolver,
    ParentResolver,
    NannyResolver,
    DealsResolver,
    SearchResolver,
    SearchResultNannyResolver,
    ParentRequestsResolver,
    NannyRequestResolver,
    UploadsResolver,
    FilesStorageService,
    VideochatResolver,
    PaymentResolver,
    SubscriptionsService
  ]
})
export class AppModule {
}
