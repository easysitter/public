import { Context, Query, Resolver } from '@nestjs/graphql';
import { GqlContext } from '../datasources/context';
import { DataSources } from '../datasources/datasources';

@Resolver('Payment')
export class PaymentResolver {
  @Query('payments')
  getUserPayments(
    @Context() { profileId, dataSources }: GqlContext<DataSources>
  ) {
    return dataSources.paymentApi.getDealsPayments(profileId);
  }
}
