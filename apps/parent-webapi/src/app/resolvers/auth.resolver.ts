import { Args, Mutation } from '@nestjs/graphql';
import { KeycloakAdminService } from '@es/nest-keycloak-admin';
import { Injectable } from '@nestjs/common';
import { ParentService } from '@services-sdk';

@Injectable()
export class AuthResolver {

  constructor(
    private keycloakAdmin: KeycloakAdminService,
    private profile: ParentService
  ) {
  }

  @Mutation('registerUser')
  async registerUser(
    @Args('data') data,
    @Args('redirect') redirectUri
  ) {
    let userId;
    const clientId = process.env.KEYCLOAK_RESOURCE;
    try {
      userId = await this.keycloakAdmin.createUser({
        ...data,
        username: data.email,
        clientId
      }).toPromise();
    } catch (e) {
      return {
        success: false,
        error: e.message
      };
    }

    await this.keycloakAdmin.sendVerificationEmail({
      user: userId,
      client_id: clientId,
      redirect_uri: redirectUri
    }).toPromise();
    await this.profile.create({
      profile: {
        address: '',
        birthDate: '',
        children: [],
        name: `${data.firstName} ${data.lastName}`,
        description: '',
        phone: '',
        photo: '',
        zipCode: ''
      },
      userId
    });
    return {
      success: true
    };
  }

}
