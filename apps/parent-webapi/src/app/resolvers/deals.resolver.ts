import { BadRequestException, ForbiddenException, Inject, ParseIntPipe, UseGuards } from "@nestjs/common";

import { Args, Context, Mutation, Parent, Query, ResolveField, Resolver, Subscription } from "@nestjs/graphql";
import { PubSub } from "graphql-subscriptions";

import { DealsService, ParentService, ReviewsService } from "@services-sdk";

import { AuthGuard } from "../auth.guard";
import { PUB_SUB } from "../modules/graphql-subscriptions.module";
import { GqlContext } from "../datasources/context";
import { DataSources } from "../datasources/datasources";

@Resolver("Deal")
export class DealsResolver {

  constructor(
    @Inject(PUB_SUB) private pubsub: PubSub,
    private parentService: ParentService,
    private deals: DealsService,
    private reviews: ReviewsService
  ) {
  }

  @Query("deals")
  @UseGuards(AuthGuard)
  async getDeals(
    @Context("profileId") parent: number
  ) {
    return this.deals.find({
      parent,
      state: "deal"
    });
  }

  @Query()
  @UseGuards(AuthGuard)
  async deal(
    @Args("id") id: string
  ) {
    return this.deals.findById(+id);
  }

  @Mutation("createDeal")
  @UseGuards(AuthGuard)
  async createDeal(
    @Context() context: GqlContext<DataSources>,
    @Args("data") data
  ): Promise<any> {
    const nannyId = +data.nanny;
    const request = await context.dataSources.parentRequests.getRequest(context.profileId, data.request);
    const nannyRequest = request.nannyRequests.find(nRequest => nRequest.user === nannyId) || {};
    return this.deals.createDeal({
      requestId: `${request.id}`,
      trackingId: `${request.trackId}`,
      parent: context.profileId,
      nanny: nannyRequest.user,
      initiator: "parent",
      details: {
        date: request.date.toISOString(),
        fromTime: request.fromTime,
        toTime: request.toTime,
        address: request.address,
        children: request.children,
        rate: nannyRequest.rate,
        trial: request.trial,
        comment: (request as any).comment || request.wishes,
        safePayment: false
      }
    });
  }

  @Mutation()
  async cancelVisit(
    @Args("id") visitId,
    @Context("profileId") user
  ) {
    const deal = await this.deals.findById(visitId);
    if (!deal) throw new BadRequestException();
    if (deal.parent !== user) throw new ForbiddenException();
    return this.deals.cancelVisit(deal.id);
  }

  @Mutation()
  @UseGuards(AuthGuard)
  async submitReview(
    @Context("profileId") profileId,
    @Args("deal", ParseIntPipe) dealId,
    @Args("review") reviewData
  ) {
    const review = await this.reviews.create({
      refId: dealId,
      refType: "deal",
      userId: `${profileId}`,
      userType: "parent",
      ...reviewData
    });
    const deal = await this.deals.findById(dealId);
    this.pubsub.publish("dealStatusUpdate", deal);
    return {
      ...deal,
      review
    };
  }


  @Subscription("dealStatusUpdate", {
    resolve: (value) => value
  })
  dealStatusUpdate() {
    return this.pubsub.asyncIterator("dealStatusUpdate");
  }

  @ResolveField("nanny")
  async getNanny(
    @Parent() deal,
    @Context() context: GqlContext<DataSources>
  ) {
    return await context.dataSources.nanny.getByProfileId(deal.nanny);
  }

  @ResolveField("parent")
  getParent(@Parent() deal) {
    return this.parentService.getById(deal.parent);
  }

  @ResolveField()
  wishes(@Parent() deal) {
    return deal.wishes || deal.comment;
  }

  @ResolveField()
  async review(
    @Context("profileId") profileId,
    @Parent() deal
  ) {
    if (deal.isActive) return null;
    if (deal.review) {
      return deal.review;
    }
    const reviews = await this.reviews.find({
      refId: deal.id,
      refType: "deal",
      userId: `${profileId}`,
      userType: "parent"
    });
    return reviews && reviews.length ? reviews[0] : null;
  }

  @ResolveField("pendingPayment")
  async getPendingPayment(
    @Parent() deal,
    @Context() { dataSources }
  ) {
    return dataSources.paymentApi.getDealPendingPayments(deal.parent, deal.id);
  }
}
