import { Args, Context, Mutation, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { ParseIntPipe, UseGuards } from "@nestjs/common";
import { AuthGuard } from "../auth.guard";
import { ParentService, SearchDto, SearchResult } from "@services-sdk";
import { GqlContext } from "../datasources/context";
import { DataSources } from "../datasources/datasources";

@Resolver("SearchResult")
export class SearchResolver {

  constructor(
    private parentProfile: ParentService
  ) {
  }

  @Query("searchResult")
  async getSearchResult(
    @Args("searchId", ParseIntPipe) id: number,
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return await dataSources.search.getSearchResult(id);
  }

  @Query()
  async searchPrefilled(
    @Context() context: GqlContext<DataSources>
  ) {
    const profile = await this.parentProfile.getById(context.profileId);
    return {
      address: profile.address,
      zipCode: profile.zipCode,
      phone: profile.phone,
      wishes: ""
    };
  }

  @ResolveField("nannies")
  getNannies(
    @Parent() searchResult: SearchResult,
    @Context() context: GqlContext<DataSources>
  ) {
    const promises = searchResult.nannies.map(async searchResultNanny => {
      const profile = await context.dataSources.nanny.getByProfileId(searchResultNanny.id);
      return {
        ...searchResultNanny,
        description: profile.description,
        name: profile.name,
        birthDate: profile.birthDate,
        photo: profile.photo
      };
    });
    return Promise.all(promises);
  }

  @Mutation("search")
  async runSearch(
    @Args("params") params: SearchDto,
    @Context() { dataSources, profileId }: GqlContext<DataSources>
  ) {
    return await dataSources.search.search(profileId, params);
  }

  @Mutation("approveNannies")
  async approveNannies(
    @Args("searchId", ParseIntPipe) id: number,
    @Args("nannies") nannies: number[],
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    const result = await dataSources.search.getSearchResult(id);
    return await dataSources.search.approve(result, nannies.map(Number));
  }

  @Mutation("createRequest")
  @UseGuards(AuthGuard)
  async createRequest(
    @Args("params") params: CreateRequestDto,
    @Context() { dataSources, profileId }: GqlContext<DataSources>
  ) {
    const { search: searchId, address, zipCode, wishes, phone, nannies } = params;

    const filteredNannyIds = nannies.map(Number);
    const searchResult = await dataSources.search.getSearchResult(+searchId);
    await dataSources.search.approve(searchResult, params.nannies.map(Number));
    const { id, ...requestParams } = searchResult;
    return dataSources.parentRequests.create({
      ...requestParams,
      parent: profileId,
      nannies: searchResult.nannies.filter(nanny => filteredNannyIds.includes(nanny.id)),
      trackId: id.toString(),
      zipCode, address, wishes, phone
    });
  }
}


export interface CreateRequestDto {
  search: number;
  nannies: number[],

  address: string;
  zipCode: string;
  phone: string;
  wishes: string;

}
