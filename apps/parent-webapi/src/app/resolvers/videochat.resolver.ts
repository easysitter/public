import { Args, Context, Mutation, Parent, Query, ResolveProperty, Resolver } from '@nestjs/graphql';
import { AuthGuard } from '../auth.guard';
import { HttpService, UseGuards } from '@nestjs/common';
import { pluck } from 'rxjs/operators';

@Resolver('VideochatSession')
export class VideochatResolver {
  private BASE_URL = process.env.VIDEOCHAT_SERVICE_BASE_URL;

  constructor(
    private http: HttpService
  ) {
  }

  @Query('videochat')
  getVideochat(
    @Args('id') id: string
  ) {
    return {
      id
    };
  }

  @Mutation('createVideochat')
  @UseGuards(AuthGuard)
  createSession(
    @Context('profileId') parent: number,
    @Args('nanny') nanny: string
  ) {
    return this.http.post(`${this.BASE_URL}/sessions`, {
      parent, nanny
    }).pipe(
      pluck('data')
    ).toPromise();
  }

  @ResolveProperty('token')
  token(
    @Parent() session,
    @Context('profileId') parent: number
  ) {
    return this.http.post(`${this.BASE_URL}/sessions/${session.id}/token`, {
      role: 'parent', user: parent
    }).pipe(
      pluck('data')
    ).toPromise();
  }
}
