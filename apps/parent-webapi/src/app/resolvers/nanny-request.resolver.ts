import { Args, Context, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { GqlContext } from "../datasources/context";
import { DataSources } from "../datasources/datasources";

@Resolver("NannyRequest")
export class NannyRequestResolver {

  @Query("request")
  getRequest(
    @Args("id") request,
    @Args("nanny") nanny,
    @Context() context: GqlContext<DataSources>
  ) {
    return context.dataSources.nannyRequests.findById(nanny, request);
  }

  @ResolveField("nanny")
  getNanny(
    @Parent() nannyRequest,
    @Context() context: GqlContext<DataSources>
  ) {
    const { user } = nannyRequest;
    return context.dataSources.nanny.getByProfileId(user);
  }

  @ResolveField()
  status(
    @Parent() nannyRequest
  ) {
    if (!nannyRequest.isActive) return "closed";
    return nannyRequest.status;
  }
}
