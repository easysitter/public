import { Args, Context, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { ParseIntPipe, UseGuards } from "@nestjs/common";
import { AuthGuard } from "../auth.guard";
import { differenceInYears } from "date-fns";
import { GqlContext } from "../datasources/context";
import { DataSources } from "../datasources/datasources";

@Resolver("Nanny")
export class NannyResolver {
  @Query("nanny")
  @UseGuards(AuthGuard)
  async getNanny(
    @Args("id", ParseIntPipe) id: number,
    @Context() context: GqlContext<DataSources>
  ) {
    return await context.dataSources.nanny.getByProfileId(id);
  }

  @ResolveField()
  async name(
    @Parent() nanny,
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    if (!nanny) return null;
    const { name } = await dataSources.nanny.getByProfileId(nanny.id);
    return name;
  }

  @ResolveField()
  async rating(
    @Parent() nanny,
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    if (!nanny) return null;
    const { rating } = await dataSources.nanny.getByProfileId(nanny.id);
    return rating;
  }

  @ResolveField()
  async description(
    @Parent() nanny,
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    if (!nanny) return null;
    const { description } = await dataSources.nanny.getByProfileId(nanny.id);
    return description;
  }

  @ResolveField("age")
  async getAge(
    @Parent() nanny,
    @Context() context: GqlContext<DataSources>
  ) {
    if (!nanny) return null;
    const { birthDate } = await context.dataSources.nanny.getByProfileId(nanny.id);
    return differenceInYears(new Date(), new Date(birthDate));
  }

  @ResolveField()
  async photo(
    @Parent() nanny,
    @Context() context: GqlContext<DataSources>
  ) {
    if (!nanny) return null;
    const { photo } = await context.dataSources.nanny.getByProfileId(nanny.id);
    return photo || "assets/images/funny-bday-cat.jpg";
  }

  @ResolveField()
  async features(
    @Parent() nanny,
    @Context() context: GqlContext<DataSources>
  ) {
    const defaultFeatures = {
      nosmoking: false,
      first_aid: false,
      driving_licence: false,
      car: false,
      education: false
    };
    if (!nanny) return defaultFeatures;
    const { features } = await context.dataSources.nanny.getByProfileId(nanny.id);
    return features || defaultFeatures;
  }
}
