import {
  Args,
  Context,
  Mutation,
  Query,
  Resolver,
  Subscription,
} from '@nestjs/graphql';
import { Inject, Injectable, UseGuards } from '@nestjs/common';
import { AuthGuard } from '../auth.guard';
import { NotificationsService, ParentService } from '@services-sdk';
import { KeycloakContext } from 'keycloak-connect-graphql';
import { SubscriptionsService } from '../services/subscriptions.service';
import { PUB_SUB } from '../modules/graphql-subscriptions.module';
import { PubSub } from 'graphql-subscriptions';

@Injectable()
@Resolver('Parent')
export class ParentResolver {
  constructor(
    @Inject(PUB_SUB) private pubsub: PubSub,
    private profileService: ParentService,
    private notifications: NotificationsService,
    private subscriptionsService: SubscriptionsService
  ) {}

  @Query('profile')
  @UseGuards(AuthGuard)
  async me(@Context('profileId') profileId: number) {
    try {
      return await this.profileService.getById(profileId);
    } catch (e) {
      return null;
    }
  }

  @Query()
  @UseGuards(AuthGuard)
  async hasActiveSubscription(@Context('profileId') profileId: number) {
    try {
      const { isActive } = await this.subscriptionsService.getByUser(profileId);
      return isActive || false;
    } catch (e) {
      return false;
    }
  }

  @Mutation()
  @UseGuards(AuthGuard)
  async requestSubscriptionPaymentUrl(
    @Args('returnUrl') returnUrl: string,
    @Context('profileId') profileId: number
  ) {
    try {
      const { payment } = await this.subscriptionsService.createSubscription(
        profileId,
        returnUrl
      );
      return payment.meta.redirect_uri;
    } catch (e) {
      return '';
    }
  }

  @Mutation('updateProfile')
  @UseGuards(AuthGuard)
  updateProfile(
    @Context('profileId') profileId: number,
    @Context('kauth') kuath: KeycloakContext,
    @Args('data') data
  ) {
    if (!profileId) {
      return this.profileService.create({
        profile: data,
        userId: (kuath.accessToken.content as any).sub,
      });
    } else {
      return this.profileService.update(profileId, data);
    }
  }

  @Mutation()
  @UseGuards(AuthGuard)
  saveNotificationToken(
    @Args('token') token: string,
    @Context('profileId') profileId
  ) {
    this.notifications.subscribePush({
      user: `p${profileId}`,
      token,
    });
    return true;
  }

  @Subscription('subscriptionActivated', {
    resolve: (value) => !!value,
    filter: (payload, variables, context) => {
      return context.profileId.toString() === payload.user.toString();
    },
  })
  requestsStatusUpdate() {
    return this.pubsub.asyncIterator('subscription_activated');
  }
}
