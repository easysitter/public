import { Inject, UseGuards } from "@nestjs/common";

import { Args, Context, Mutation, Parent, Query, ResolveField, Resolver, Subscription } from "@nestjs/graphql";
import { PubSub } from "graphql-subscriptions";

import { PUB_SUB } from "../modules/graphql-subscriptions.module";
import { AuthGuard } from "../auth.guard";
import { GqlContext } from "../datasources/context";
import { DataSources } from "../datasources/datasources";

@Resolver('ParentRequest')
export class ParentRequestsResolver {

  constructor(
    @Inject(PUB_SUB) private pubsub: PubSub,
  ) {
  }

  @Query('requests')
  @UseGuards(AuthGuard)
  async getRequests(
    @Context() context: GqlContext<DataSources>
  ) {
    if (!context.profileId) return [];
    return context.dataSources.parentRequests.getRequests(context.profileId, {
      isActive: true
    });
  }

  @Mutation()
  cancelRequest(
    @Args('id') id: string,
    @Context() context: GqlContext<DataSources>
  ) {
    return context.dataSources.parentRequests.cancel(context.profileId, +id);
  }

  @ResolveField('nannyRequests')
  async getNannyRequests(
    @Parent() request,
    @Context() context: GqlContext<DataSources>
  ) {
    let { nannyRequests } = request;
    if (!nannyRequests) {
      request = await context.dataSources.parentRequests.getRequest(context.profileId, request.id);
      if (request) {
        nannyRequests = request.nannyRequests;
      } else {
        nannyRequests = [];
      }
    }
    return nannyRequests.map(r => ({ ...r, request: request }));
  }

  @Subscription('requestsStatusUpdate', {
    resolve: (value) => value
  })
  requestsStatusUpdate() {
    return this.pubsub.asyncIterator('requestsStatusUpdate');
  }
}
