import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { differenceInYears } from 'date-fns';

@Resolver('SearchResultNanny')
export class SearchResultNannyResolver {
  @ResolveField('age')
  getAge(@Parent() nanny) {
    if (!nanny) return null;
    return differenceInYears(new Date(), new Date(nanny.birthDate));
  }

  @ResolveField()
  photo(@Parent() nanny) {
    if (!nanny) return null;
    return nanny.photo || 'assets/images/funny-bday-cat.jpg';
  }
}
