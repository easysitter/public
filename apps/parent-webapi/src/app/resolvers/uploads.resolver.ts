import { Args, Context, Mutation } from '@nestjs/graphql';
import { Injectable, UseGuards } from '@nestjs/common';
import { AuthGuard } from '../auth.guard';
import { FilesStorageService } from '@services-sdk';
import { KeycloakContext } from 'keycloak-connect-graphql';

@Injectable()
export class UploadsResolver {
  constructor(private filesStorage: FilesStorageService) {}

  @Mutation()
  @UseGuards(AuthGuard)
  async uploadFile(
    @Context('profileId') profileId,
    @Context('kauth') kauth: KeycloakContext,
    @Args() args
  ) {
    const file = await args.file;
    const stream = file.createReadStream();

    const filename = await this.filesStorage.uploadFromStream({
      filename: `parent/${
        profileId || (kauth.accessToken.content as any).sub || 'unknown'
      }/${args.resourceType || 'common'}/${file.filename}`,
      mimetype: file.mimetype,
      stream,
    });
    return {
      public_url: filename,
    };
  }
}
