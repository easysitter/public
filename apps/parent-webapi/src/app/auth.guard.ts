import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { KeycloakContext } from 'keycloak-connect-graphql';

@Injectable()
export class AuthGuard implements CanActivate {

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const kauth: KeycloakContext = GqlExecutionContext.create(context).getContext().kauth;
    return kauth.isAuthenticated();
  }

}
