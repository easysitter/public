module.exports = {
  preset: '../../jest.preset.js',
  coverageDirectory: '../../coverage/apps/parent-webapi',
  globals: { 'ts-jest': { tsconfig: '<rootDir>/tsconfig.spec.json' } },
  displayName: 'parent-webapi',
  testEnvironment: 'node',
};
