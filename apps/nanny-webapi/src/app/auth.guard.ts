import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { GqlExecutionContext } from '@nestjs/graphql';
import { KeycloakContext } from 'keycloak-connect-graphql';

@Injectable()
export class AuthGuard implements CanActivate {

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const kauth: KeycloakContext = GqlExecutionContext.create(context).getContext().kauth;
    return kauth.isAuthenticated();
  }

}
