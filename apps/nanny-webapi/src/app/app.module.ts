import { HttpModule, Logger, Module } from "@nestjs/common";
import { GraphQLModule } from "@nestjs/graphql";
import { FilesStorageService, NannyRequestsApi, ServicesSdkModule } from "@services-sdk";

import { ChatModule } from "./modules/chat/chat.module";
import { DateScalar } from "./date.scalar";
import { AuthGuard } from "./auth.guard";
import { JsonScalar } from "./json.scalar";
import { UserResolver } from "./resolvers/user.resolver";
import { RequestResolver } from "./resolvers/request.resolver";
import { GraphqlSubscriptionsModule } from "./modules/graphql-subscriptions.module";
import { DealsResolver } from "./resolvers/deals.resolver";
import { ParentResolver } from "./resolvers/parent.resolver";
import { KeycloakModule } from "./modules/keycloak/keycloak.module";
import { GqlConfig } from "./modules/config/gql-config";
import { RequestController } from "./controllers/request.controller";
import { ConfigModule } from "./modules/config/config.module";
import { DealController } from "./controllers/deal.controller";
import { UploadsResolver } from "./resolvers/uploads.resolver";
import { NannyProfileResolver } from "./resolvers/nanny-profile.resolver";
import { ParentRequestResolver } from "./resolvers/parent-request.resolver";
import { REQUESTS_SERVICE_BASE_URL } from "./modules/config/providers";
import { VideochatResolver } from './resolvers/videochat.resolver';
import { VideochatController } from './controllers/videochat.controller';

@Module({
  imports: [
    ConfigModule,
    KeycloakModule.forRoot({
      "realm": process.env.KEYCLOAK_REALM,
      "bearer-only": true,
      "auth-server-url": process.env.KEYCLOAK_AUTH_SERVER_URL,
      "ssl-required": "none",
      "resource": process.env.KEYCLOAK_RESOURCE,
      "verify-token-audience": true,
      "use-resource-role-mappings": true,
      "confidential-port": 0,
      "admin-user-name": process.env.KEYCLOAK_ADMIN_USERNAME,
      "admin-user-password": process.env.KEYCLOAK_ADMIN_PASSWORD
    }),
    GraphQLModule.forRootAsync({
      useExisting: GqlConfig
    }),
    GraphqlSubscriptionsModule,
    ServicesSdkModule.forRoot({
      baseUrlsConfig: {
        chat: process.env.CHAT_SERVICE_BASE_URL || "http://localhost:3334/api",
        requests: process.env.REQUESTS_SERVICE_BASE_URL || "http://localhost:3335/api",
        deals: process.env.DEALS_SERVICE_BASE_URL || "http://localhost:3336/api",
        search: process.env.SEARCH_SERVICE_BASE_URL || "http://localhost:3337/api",
        nannyProfile: process.env.NANNY_PROFILE_SERVICE_BASE_URL || "http://localhost:3338/api",
        parentProfile: process.env.PARENT_PROFILE_SERVICE_BASE_URL || "http://localhost:3339/api",
        reviews: process.env.REVIEWS_SERVICE_BASE_URL || "http://localhost:3340/api",
        notifications: process.env.NOTIFICATIONS_SERVICE_BASE_URL || "http://localhost:3341/api",
        address: process.env.ADDRESS_SERVICE_BASE_URL || "http://localhost:3343"
      }
    }),
    ChatModule,
    HttpModule
  ],
  controllers: [
    RequestController,
    DealController,
    VideochatController
  ],
  providers: [
    Logger,
    DateScalar,
    JsonScalar,
    AuthGuard,
    FilesStorageService,
    {
      provide: NannyRequestsApi,
      useFactory: (baseUrl: string) => {
        return new NannyRequestsApi(baseUrl);
      },
      inject: [REQUESTS_SERVICE_BASE_URL]
    },
    UserResolver,
    NannyProfileResolver,
    RequestResolver,
    ParentRequestResolver,
    DealsResolver,
    ParentResolver,
    UploadsResolver,
    VideochatResolver
  ]
})
export class AppModule {
}
