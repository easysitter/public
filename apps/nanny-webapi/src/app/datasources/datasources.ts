import { ChatApi, NannyProfileApi, NannyRequestsApi } from "@services-sdk";

export interface DataSources {
  chat: ChatApi;
  nanny: NannyProfileApi;
  nannyRequests: NannyRequestsApi
}
