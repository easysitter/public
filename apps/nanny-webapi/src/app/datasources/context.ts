import { KeycloakContext } from 'keycloak-connect-graphql';


export interface GqlContext<D> {
  kauth: KeycloakContext;
  profileId?: number;
  dataSources: D
}
