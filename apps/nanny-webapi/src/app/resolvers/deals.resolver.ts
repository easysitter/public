import { Args, Context, Mutation, Parent, Query, ResolveField, Resolver, Subscription } from "@nestjs/graphql";
import { Inject, ParseIntPipe, UseGuards } from "@nestjs/common";
import { AuthGuard } from "../auth.guard";
import { PUB_SUB } from "../modules/graphql-subscriptions.module";
import { PubSub } from "graphql-subscriptions";
import { DealsService, ReviewsService } from "@services-sdk";


@Resolver('Deal')
export class DealsResolver {

  constructor(
    @Inject(PUB_SUB) private pubsub: PubSub,
    private readonly dealsService: DealsService,
    private readonly reviewsService: ReviewsService
  ) {
  }

  @Query('deals')
  @UseGuards(AuthGuard)
  async deals(
    @Args('filter') filter: any,
    @Context('profileId') profileId: number
  ) {
    if (!profileId) return [];
    const { isActive } = filter;
    return this.dealsService.find({
      state: 'deal',
      isActive,
      nanny: profileId
    });
  }

  @Query()
  @UseGuards(AuthGuard)
  async deal(
    @Args('id') id: string
  ) {
    return this.dealsService.findById(+id);
  }

  @Mutation()
  @UseGuards(AuthGuard)
  async submitReview(
    @Context('profileId') profileId,
    @Args('deal', ParseIntPipe) dealId,
    @Args('review') reviewData
  ) {
    const review = await this.reviewsService.create({
      refId: dealId,
      refType: 'deal',
      userId: `${profileId}`,
      userType: 'nanny',
      ...reviewData
    });
    const deal = await this.dealsService.findById(dealId);
    this.pubsub.publish('dealStatusUpdate', deal);
    return {
      ...deal,
      review
    };
  }

  @Mutation()
  startVisit(
    @Args('id') id: string
  ) {
    return this.dealsService.startVisit(+id);
  }

  @Mutation()
  finishVisit(
    @Args('id') id: string
  ) {
    return this.dealsService.finishVisit(+id);
  }

  @Subscription('dealStatusUpdate', {
    resolve: (value) => value,
    filter: (payload, variables, { profileId }) => {
      return payload.nanny.toString() === profileId.toString();
    }
  })
  dealStatusUpdate() {
    return this.pubsub.asyncIterator('dealStatusUpdate');
  }

  @ResolveField('state')
  getState(@Parent() deal) {
    if (deal.isActive) return 'new';
    return deal.tag;
  }

  @ResolveField()
  wishes(@Parent() deal) {
    return deal.wishes || deal.comment;
  }

  @ResolveField('review')
  async getReview(
    @Parent() deal,
    @Context('profileId') profileId
  ) {
    if (deal.isActive) return null;
    if (deal.review) {
      return deal.review;
    }
    const reviews = await this.reviewsService.find({
      refId: deal.id,
      refType: 'deal',
      userId: `${profileId}`,
      userType: 'nanny'
    });
    return reviews && reviews.length ? reviews[0] : null;
  }
}
