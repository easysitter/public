import { Args, Context, Mutation, Parent, Query, ResolveField, Resolver } from "@nestjs/graphql";
import { UseGuards } from "@nestjs/common";
import { AuthGuard } from "../auth.guard";
import { KeycloakAdmin } from "../modules/keycloak/keycloak-admin";
import { District, DistrictApi, NotificationsService } from "@services-sdk";
import { GqlContext } from "../datasources/context";
import { DataSources } from "../datasources/datasources";

@Resolver("User")
export class UserResolver {
  constructor(
    private keycloakAdmin: KeycloakAdmin,
    private notifications: NotificationsService,
    private addressApi: DistrictApi
  ) {

  }

  @Query("me")
  @UseGuards(AuthGuard)
  async me(@Context() { kauth }: GqlContext<DataSources>) {
    const { sub } = kauth.accessToken.content;
    return { id: sub };
  }

  @Query()
  async districts(): Promise<District[]> {
    const { data } = await this.addressApi.getDistricts();
    return data;
  }

  @Mutation()
  saveNotificationToken(
    @Args("token")  token: string,
    @Context("profileId") profileId
  ) {
    this.notifications.subscribePush({
      user: `n${profileId}`,
      token
    });
  }

  @ResolveField("profile")
  async resolveProfile(
    @Parent() user,
    @Context() context: GqlContext<DataSources>
  ) {
    return await context.dataSources.nanny.getByUserId(user.id);
  }

  @Mutation("saveProfile")
  @UseGuards(AuthGuard)
  async saveProfile(
    @Args("profile") profileData,
    @Context() {kauth, dataSources}: GqlContext<DataSources>
  ) {
    const { sub } = kauth.accessToken.content;
    const profile = await dataSources.nanny.save(sub, profileData);
    this.keycloakAdmin.setAttributes(
      kauth.accessToken.content.sub,
      Object.entries(profile.documents).reduce((attrs, [name, doc]) => ({ ...attrs, [name]: doc }), {})
    ).subscribe();
    return profile;
  }
}
