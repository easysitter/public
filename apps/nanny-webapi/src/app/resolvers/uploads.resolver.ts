import { Args, Context, Mutation } from "@nestjs/graphql";
import { Injectable, UseGuards } from "@nestjs/common";
import { AuthGuard } from "../auth.guard";
import { FilesStorageService } from "@services-sdk";

@Injectable()
export class UploadsResolver {
  constructor(
    private filesStorage: FilesStorageService
  ) {
  }

  @Mutation()
  @UseGuards(AuthGuard)
  async uploadFile(
    @Context("profileId") profileId,
    @Args() args
  ) {
    const file = await args.file;
    const stream = file.createReadStream();

    const filename = await this.filesStorage.uploadFromStream({
      filename: `nanny/${profileId || "unknown"}/${args.resourceType || "common"}/${file.filename}`,
      mimetype: file.mimetype,
      stream
    });
    return {
      public_url: filename
    };
  }
}
