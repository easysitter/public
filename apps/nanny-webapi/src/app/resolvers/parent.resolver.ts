import { Args, Context, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { ParentService } from '@services-sdk';

@Resolver('Parent')
export class ParentResolver {
  constructor(
    private parentsService: ParentService
  ) {

  }

  @Query('parent')
  getParent(
    @Args('id') id
  ) {
    return this.parentsService.getById(id);
  }

  @ResolveField('id')
  async getId(@Parent() parent, @Context() context) {
    if (Number.isInteger(parent)) {
      parent = await this.fetchInContext(context, parent);
    }
    return parent.id;
  }

  @ResolveField('name')
  async getName(@Parent() parent, @Context() context) {
    if (Number.isInteger(parent)) {
      parent = await this.fetchInContext(context, parent);
    }
    return parent.name;
  }

  @ResolveField('photo')
  async getPhoto(@Parent() parent, @Context() context) {
    if (Number.isInteger(parent)) {
      parent = await this.fetchInContext(context, parent);
    }
    return parent.photo || 'assets/images/funny-bday-cat.jpg';
  }

  fetchInContext(context, id) {
    if (!context.parents) {
      context.parents = new Map();
    }
    if (!context.parents.has(id)) {
      context.parents.set(id, this.parentsService.getById(id));
    }
    return context.parents.get(id);
  }
}
