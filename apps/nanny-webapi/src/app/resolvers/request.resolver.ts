import { Args, Context, Parent, Query, ResolveField, Resolver, Subscription } from "@nestjs/graphql";
import { PubSub } from "graphql-subscriptions";
import { Inject, UseGuards } from "@nestjs/common";
import { PUB_SUB } from "../modules/graphql-subscriptions.module";
import { AuthGuard } from "../auth.guard";
import { NannyRequest, NannyRequestStatus } from "@services-sdk";
import { GqlContext } from "../datasources/context";
import { DataSources } from "../datasources/datasources";

export function noop(value) {
  return value;
}

@Resolver("NannyRequest")
export class RequestResolver {

  constructor(
    @Inject(PUB_SUB) private pubsub: PubSub
  ) {

  }

  @Query("requests")
  @UseGuards(AuthGuard)
  getRequests(
    @Context() { profileId, dataSources }: GqlContext<DataSources>
  ) {
    return dataSources.nannyRequests.find({ nanny: profileId });
  }

  @Query("conversations")
  @UseGuards(AuthGuard)
  async getConversations(
    @Context() { profileId, dataSources }: GqlContext<DataSources>
  ) {
    return dataSources.nannyRequests.find({ status: "accepted", nanny: profileId });
  }

  @Query("request")
  @UseGuards(AuthGuard)
  async getRequest(
    @Context() { profileId, dataSources }: GqlContext<DataSources>,
    @Args("id") id: number
  ) {
    return dataSources.nannyRequests.findById(profileId, `${id}`);
  }

  @ResolveField("request")
  getParentRequest(@Parent() nannyRequest: NannyRequest) {
    return nannyRequest.parentRequest;
  }


  @Subscription("requestsStatusUpdate", {
    resolve: (value) => value,
    filter: (payload, variables, { profileId }) => {
      return payload.nanny.toString() === profileId.toString();
    }
  })
  requestsStatusUpdate() {
    return this.pubsub.asyncIterator("requestsStatusUpdate");
  }

  @ResolveField()
  status(
    @Parent() nannyRequest
  ): NannyRequestStatus {
    if (!nannyRequest.isActive) return "closed";
    return nannyRequest.status;
  }
}

