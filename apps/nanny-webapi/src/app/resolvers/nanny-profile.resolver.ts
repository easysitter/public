import { Context, Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { GqlContext } from "../datasources/context";
import { DataSources } from "../datasources/datasources";
import { NannyProfileApi } from "@services-sdk";

@Resolver("NannyProfile")
export class NannyProfileResolver {

  @ResolveField()
  name(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "name");
  }

  @ResolveField()
  birthDate(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "birthDate");
  }

  @ResolveField()
  description(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "description");
  }

  @ResolveField()
  photo(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "photo");
  }

  @ResolveField()
  price(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "price");
  }

  @ResolveField()
  trial(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "trial");
  }

  @ResolveField()
  trialEnabled(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "trialEnabled");
  }

  @ResolveField()
  phone(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "phone");
  }

  @ResolveField()
  schedule(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "schedule");
  }

  @ResolveField()
  ageCategories(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "ageCategories");
  }

  @ResolveField()
  maxChildrenCount(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "maxChildrenCount");
  }

  @ResolveField()
  documents(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "documents");
  }

  @ResolveField()
  districts(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    return this.resolveField(id, dataSources.nanny, "districts");
  }

  @ResolveField()
  async features(
    @Parent() { id },
    @Context() { dataSources }: GqlContext<DataSources>
  ) {
    if (!id) {
      return {
        nosmoking: true,
        first_aid: false,
        driving_licence: false,
        car: false,
        education: false
      };
    }
    const profile = await dataSources.nanny.getByProfileId(id);
    if (profile && profile.features) {
      return profile.features;
    }
  }

  private async resolveField(profileId, api: NannyProfileApi, fieldName) {
    const profile = await api.getByProfileId(profileId);
    return profile ? profile[fieldName] : null;
  }
}
