import {
  Args,
  Context,
  Mutation,
  Parent,
  Query,
  ResolveProperty,
  Resolver,
  Subscription,
} from '@nestjs/graphql';
import { HttpService, Inject } from '@nestjs/common';
import { PUB_SUB } from '../modules/graphql-subscriptions.module';
import { PubSub } from 'graphql-subscriptions';
import { pluck } from 'rxjs/operators';

@Resolver('VideochatSession')
export class VideochatResolver {
  private BASE_URL = process.env.VIDEOCHAT_SERVICE_BASE_URL;

  constructor(
    @Inject(PUB_SUB) private pubSub: PubSub,
    private http: HttpService
  ) {}

  @Query('videochat')
  getVideochat(@Args('id') id: string) {
    return {
      id,
    };
  }

  @Mutation('refuseVideochat')
  async refuseVideochat(
    @Args('sessionId') id: string,
    @Context('profileId') userId: number
  ) {
    try {
      await this.http
        .post(`${this.BASE_URL}/sessions/${id}/terminate`, {
          role: 'nanny',
          user: userId.toString(),
        })
        .toPromise();
      return true;
    } catch (e) {
      return false;
    }
  }

  @Subscription('incomingVideochat', {
    resolve: (value) => {
      console.log({ value });
      return value;
    },
    filter: (payload, variables, context) => {
      console.log({ payload, variables, context });
      return context.profileId.toString() === payload.user;
    },
  })
  incomingVideochat() {
    return this.pubSub.asyncIterator('incoming_videochat');
  }

  @ResolveProperty('token')
  token(@Parent() session, @Context('profileId') parent: number) {
    return this.http
      .post(`${this.BASE_URL}/sessions/${session.id}/token`, {
        role: 'nanny',
        user: parent,
      })
      .pipe(pluck('data'))
      .toPromise();
  }
}
