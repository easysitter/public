import { Context, Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { DistrictApi } from "@services-sdk";
import { GqlContext } from "../datasources/context";
import { DataSources } from "../datasources/datasources";

@Resolver('ParentRequest')
export class ParentRequestResolver {
  constructor(
    private districtApi: DistrictApi
  ) {
  }

  @ResolveField()
  async district(
    @Context() {dataSources, kauth}: GqlContext<DataSources>,
    @Parent() parentRequest
  ) {
    const { data: districts } = await this.districtApi.getDistricts();
    const nanny = await dataSources.nanny.getByUserId(kauth.accessToken.content.sub);
    const { zipCode } = parentRequest;
    if (!nanny.districts || !zipCode) return null;
    const { data: checkResult } = await this.districtApi.checkZipInDistricts({
      zip: zipCode, districtIds: districts.map(({ id }) => id)
    });
    const zipCheckResult = checkResult.find(r => r.zipBelongsToDistrict);
    if (!zipCheckResult) {
      return null;
    }
    const { districtId } = zipCheckResult;
    const { title } = districts.find(({ id }) => id === districtId);
    return {
      id: districtId,
      title,
      isOwn: nanny.districts.includes(districtId)
    };
  }
}
