import { GqlModuleOptions, GqlOptionsFactory } from "@nestjs/graphql";
import { HttpService, Inject } from "@nestjs/common";
import { KC_SUBSCRIPTION_HANDLER } from "../keycloak/providers";
import { join, resolve } from "path";
import { KeycloakContext, KeycloakSubscriptionContext } from "keycloak-connect-graphql";
import { pluck } from "rxjs/operators";
import { CHAT_SERVICE_BASE_URL, NANNY_PROFILE_SERVICE_BASE_URL, REQUESTS_SERVICE_BASE_URL } from "./providers";
import { ChatApi, NannyProfileApi, NannyRequestsApi } from "@services-sdk";

export class GqlConfig implements GqlOptionsFactory {
  constructor(
    @Inject(KC_SUBSCRIPTION_HANDLER)
    private keycloakSubscriptionHandler,
    @Inject(NANNY_PROFILE_SERVICE_BASE_URL)
    private profileServiceBaseUrl: string,
    @Inject(REQUESTS_SERVICE_BASE_URL)
    private REQUESTS_BASE_URL: string,
    @Inject(CHAT_SERVICE_BASE_URL)
    private CHAT_BASE_URL: string,
    private http: HttpService
  ) {
  }

  createGqlOptions(): Promise<GqlModuleOptions> | GqlModuleOptions {
    return {
      typePaths: [
        process.env.SCHEMA_PATH
          ? resolve(process.env.SCHEMA_PATH)
          : join(__dirname, "../../../gql/*.graphql")
      ],
      installSubscriptionHandlers: true,
      uploads: {
        maxFiles: 5
      },
      context: async ({ req, connection }) => {
        if (connection) {
          return connection.context;
        }
        const kauth = new KeycloakContext({ req });
        let profile = null;
        if (kauth && kauth.accessToken && kauth.accessToken.content.sub) {
          profile = await this.getProfileId(kauth.accessToken.content.sub);
        }
        return {
          kauth, // 3. add the KeycloakContext to `kauth`,
          profileId: profile
        };
      },
      subscriptions: {
        onConnect: async (connectionParams) => {
          const token = await this.keycloakSubscriptionHandler.onSubscriptionConnect(connectionParams, null, null);
          const kauth = new KeycloakSubscriptionContext(token);
          let profile = null;
          if (kauth && kauth.accessToken && kauth.accessToken.content.sub) {
            profile = await this.getProfileId(kauth.accessToken.content.sub);
          }
          return {
            kauth,
            profileId: profile
          };
        }
      },
      dataSources: () => {
        return {
          chat: new ChatApi(this.CHAT_BASE_URL),
          nanny: new NannyProfileApi(this.profileServiceBaseUrl),
          nannyRequests: new NannyRequestsApi(this.REQUESTS_BASE_URL)
        };
      }
    };
  }

  private getProfileId(user: string) {
    return this.http.get(`${this.profileServiceBaseUrl}/${user}`).pipe(
      pluck("data", "id")
    ).toPromise();
  }

}
