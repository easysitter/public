import { Global, HttpModule, Module } from '@nestjs/common';
import { GqlConfig } from './gql-config';
import {
  CHAT_SERVICE_BASE_URL,
  DEALS_SERVICE_BASE_URL,
  NANNY_PROFILE_SERVICE_BASE_URL,
  PARENT_PROFILE_SERVICE_BASE_URL,
  REQUESTS_SERVICE_BASE_URL,
  SEARCH_SERVICE_BASE_URL
} from './providers';
import { FirebaseConfig } from "@services-sdk";

@Global()
@Module({
  imports: [HttpModule],
  providers: [
    GqlConfig,
    FirebaseConfig,
    {
      provide: CHAT_SERVICE_BASE_URL,
      useValue: process.env.CHAT_SERVICE_BASE_URL || 'http://localhost:3334/api'
    },
    {
      provide: REQUESTS_SERVICE_BASE_URL,
      useValue: process.env.REQUESTS_SERVICE_BASE_URL || 'http://localhost:3335/api'
    },
    {
      provide: DEALS_SERVICE_BASE_URL,
      useValue: process.env.DEALS_SERVICE_BASE_URL || 'http://localhost:3336/api'
    },
    {
      provide: SEARCH_SERVICE_BASE_URL,
      useValue: process.env.SEARCH_SERVICE_BASE_URL || 'http://localhost:3337/api'
    },
    {
      provide: NANNY_PROFILE_SERVICE_BASE_URL,
      useValue: process.env.NANNY_PROFILE_SERVICE_BASE_URL || 'http://localhost:3338/api'
    },
    {
      provide: PARENT_PROFILE_SERVICE_BASE_URL,
      useValue: process.env.PARENT_PROFILE_SERVICE_BASE_URL || 'http://localhost:3339/api'
    }
  ],
  exports: [
    GqlConfig,
    FirebaseConfig,
    CHAT_SERVICE_BASE_URL,
    REQUESTS_SERVICE_BASE_URL,
    DEALS_SERVICE_BASE_URL,
    SEARCH_SERVICE_BASE_URL,
    NANNY_PROFILE_SERVICE_BASE_URL,
    PARENT_PROFILE_SERVICE_BASE_URL
  ]
})
export class ConfigModule {

}
