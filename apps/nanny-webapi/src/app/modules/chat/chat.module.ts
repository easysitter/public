import { HttpModule, Module } from '@nestjs/common';
import { ChatResolver } from './chat.resolver';
import { ChatEventsHandler } from './chat-events.handler';
import { ChatController } from './chat.controller';
import { MessageResolver } from './message.resolver';
import { ChatUserResolver } from "./chat-user.resolver";
import { ChatUserParentResolver } from "./chat-user-parent.resolver";
import { ChatUserNannyResolver } from "./chat-user-nanny.resolver";

@Module({
  imports: [
    HttpModule
  ],
  controllers: [
    ChatController
  ],
  providers: [
    ChatResolver,
    MessageResolver,
    ChatUserResolver,
    ChatUserParentResolver,
    ChatUserNannyResolver,
    ChatEventsHandler
  ]
})
export class ChatModule {
}
