import { EventPattern } from '@nestjs/microservices';
import { Inject } from '@nestjs/common';
import { PUB_SUB } from '../graphql-subscriptions.module';
import { PubSub } from 'graphql-subscriptions';

export class ChatEventsHandler {

  constructor(
    @Inject(PUB_SUB) private pubSub: PubSub
  ) {
  }

  @EventPattern('chat_created')
  async chatCreated(data: Record<string, unknown>) {
    await this.pubSub.publish('chat_created', data);
  }

  @EventPattern('message_sent')
  async messageSent(data: Record<string, unknown>) {
    await this.pubSub.publish('message_sent', data.message);
  }
}
