import { Context, Parent, ResolveField, Resolver } from "@nestjs/graphql";
import { GqlContext } from "../../datasources/context";
import { DataSources } from "../../datasources/datasources";

@Resolver("Message")
export class MessageResolver {
  @ResolveField()
  isOwn(
    @Parent() message,
    @Context() { profileId }: GqlContext<DataSources>
  ) {
    return message.user === `n${profileId}`;
  }

  @ResolveField()
  user(
    @Parent() message
  ) {
    const userId = message.user;
    if (userId.startsWith("p")) {
      return {
        type: "parent",
        id: userId.slice(1)
      };
    }
    if (userId.startsWith("n")) {
      return {
        type: "nanny",
        id: userId.slice(1)
      };
    }
    return {
      id: userId,
      type: ""
    };
  }
}
