import { HttpService, Inject, Injectable } from '@nestjs/common';
import { KC_CONFIG } from './providers';
import { catchError, pluck, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { KeycloakModuleConfig } from './keycloak-module-config';

@Injectable()
export class KeycloakAdmin {
  constructor(
    @Inject(KC_CONFIG)
    private config: KeycloakModuleConfig,
    private http: HttpService
  ) {
  }

  get serverUrl() {
    return `${this.config['auth-server-url']}`;
  }

  get realm() {
    return this.config['realm'];
  }

  setAttributes(user, attributes: Record<string, string>) {
    return this.getToken().pipe(
      switchMap(
        token => this.http
          .put(
            `${this.serverUrl}/admin/realms/${this.realm}/users/${user}`,
            { attributes },
            {
              headers: {
                Authorization: `bearer ${token}`,
                Accept: 'application/json'
              }
            }
          )
          .pipe(
            pluck('data'),
            catchError(err => {
              console.error(err.toJSON());
              return of(null);
            })
          )
      )
    );
  }


  private getToken() {
    const params = new URLSearchParams();
    params.append('username', this.config['admin-user-name']);
    params.append('password', this.config['admin-user-password']);
    params.append('grant_type', 'password');
    params.append('client_id', 'admin-cli');
    return this.http.post(`${this.serverUrl}/realms/master/protocol/openid-connect/token`, params, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).pipe(
      pluck('data', 'access_token'),
      catchError(err => {
        console.error(err.toJSON());
        return of(null);
      })
    );
  }
}
