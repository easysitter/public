export interface KeycloakModuleConfig {
  'realm': string;
  'bearer-only': boolean;
  'auth-server-url': string;
  'ssl-required': string;
  'resource': string
  'verify-token-audience': boolean;
  'use-resource-role-mappings': boolean;
  'confidential-port': number;
  'admin-user-name'?: string
  'admin-user-password'?: string;
}
