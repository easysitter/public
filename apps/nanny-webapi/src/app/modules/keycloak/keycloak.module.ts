import { DynamicModule, Global, HttpModule, Module } from '@nestjs/common';
import { KC_CONFIG, KC_INSTANCE, KC_SUBSCRIPTION_HANDLER } from './providers';
import * as Keycloak from 'keycloak-connect';
import { KeycloakSubscriptionHandler } from 'keycloak-connect-graphql';
import { KeycloakAdmin } from './keycloak-admin';
import { KeycloakModuleConfig } from './keycloak-module-config';

@Global()
@Module({})
export class KeycloakModule {
  static forRoot(config: KeycloakModuleConfig): DynamicModule {
    return {
      module: KeycloakModule,
      imports: [
        HttpModule
      ],
      providers: [
        KeycloakAdmin,
        {
          provide: KC_CONFIG,
          useValue: config
        },
        {
          provide: KC_INSTANCE,
          useFactory: () => new Keycloak({}, config),
          inject: []
        },
        {
          provide: KC_SUBSCRIPTION_HANDLER,
          useFactory: (keycloak) => new KeycloakSubscriptionHandler({ keycloak, protect: true }),
          inject: [KC_INSTANCE]
        }
      ],
      exports: [
        KC_INSTANCE,
        KC_SUBSCRIPTION_HANDLER,
        KC_CONFIG,
        KeycloakAdmin
      ]
    };
  }
}
