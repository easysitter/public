import { CustomScalar, Scalar } from '@nestjs/graphql';
import { Kind, ValueNode } from 'graphql';

@Scalar('JSON')
export class JsonScalar implements CustomScalar<string, any> {
  description = 'JSON custom scalar type';

  parseValue(value: string): any {
    return JSON.parse(value); // value from the client
  }

  serialize(value: any): string {
    return JSON.stringify(value, null, 4);
  }

  parseLiteral(ast: ValueNode): Date {
    if (ast.kind === Kind.STRING) {
      return this.parseValue(ast.value);
    }
    return null;
  }
}
