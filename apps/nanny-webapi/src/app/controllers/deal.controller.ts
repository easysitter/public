import { EventPattern } from '@nestjs/microservices';
import { Controller, Inject } from '@nestjs/common';
import { PUB_SUB } from '../modules/graphql-subscriptions.module';
import { PubSub } from 'graphql-subscriptions';
import { DealsService } from '@services-sdk';

@Controller()
export class DealController {
  constructor(
    @Inject(PUB_SUB) private pubsub: PubSub,
    private readonly dealsService: DealsService
  ) {
  }

  @EventPattern('deal_concluded')
  async onDealConcluded({ value }) {
    const { id } = value;
    try {
      await this.publishStatusUpdate({
        id
      });
    } catch (e) {
      console.error(e);
    }
  }

  @EventPattern('visit_started')
  async onVisitStarted({ value }) {
    try {
      await this.publishStatusUpdate(value);
    } catch (e) {
      console.error(e);
    }
  }

  @EventPattern('visit_finished')
  async onVisitFinished({ value }) {
    try {
      await this.publishStatusUpdate(value);
    } catch (e) {
      console.error(e);
    }
  }

  @EventPattern('offer_created')
  async onOfferCreated({ value }) {
    const { deal } = value;
    try {
      await this.publishStatusUpdate({ id: deal });
    } catch (e) {
      console.error(e);
    }
  }

  private async publishStatusUpdate(data) {
    const { id } = data;
    const deal = await this.dealsService.findById(id);
    return await this.pubsub.publish(
      'dealStatusUpdate',
      deal
    );
  }
}
