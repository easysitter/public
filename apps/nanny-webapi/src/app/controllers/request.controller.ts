import { EventPattern } from "@nestjs/microservices";
import { Controller, Inject, Logger } from "@nestjs/common";
import { PUB_SUB } from "../modules/graphql-subscriptions.module";
import { PubSub } from "graphql-subscriptions";
import { NannyRequestsApi } from "@services-sdk";

@Controller()
export class RequestController {
  constructor(
    @Inject(PUB_SUB) private pubsub: PubSub,
    private nannyRequestsService: NannyRequestsApi,
    private logger: Logger
  ) {
  }

  @EventPattern('nrequest_status_updated')
  async onRequestStatusUpdate({ value }) {
    const { request: id, nanny } = value;
    try {
      this.logger.log({
        event: 'nrequest_status_updated',
        action: 'handeled',
        data: { id, nanny }
      });
      const request = await this.nannyRequestsService.findById(nanny, id);

      this.logger.log({
        event: 'nrequest_status_updated',
        action: 'request load done',
        data: { request }
      });
      await this.publishStatusUpdate(request);
    } catch (e) {

      this.logger.error({
        event: 'nrequest_status_updated',
        action: 'request load failed',
        data: { error: e }
      });
    }
  }

  private async publishStatusUpdate(request) {
    return await this.pubsub.publish(
      'requestsStatusUpdate',
      request
    );
  }
}
