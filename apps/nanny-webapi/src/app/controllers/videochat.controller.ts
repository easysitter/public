import { Controller, Inject } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { PUB_SUB } from '../modules/graphql-subscriptions.module';
import { PubSub } from 'graphql-subscriptions';

@Controller()
export class VideochatController {
  constructor(
    @Inject(PUB_SUB) private pubSub: PubSub
  ) {
  }

  @EventPattern('videochat')
  handleVideochat(
    { value }
  ) {
    if (value.role !== 'nanny') return;
    this.pubSub.publish('incoming_videochat', {
      user: value.user,
      id: value.session
    });
  }
}
