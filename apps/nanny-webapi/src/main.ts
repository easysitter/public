/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/

import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { Transport } from '@nestjs/microservices';
import * as Keycloak from 'keycloak-connect';
import { KC_INSTANCE } from './app/modules/keycloak/providers';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const keycloak = app.get<Keycloak>(KC_INSTANCE);
  app.use(keycloak.middleware());
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  const port = process.env.port || 3333;

  app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      client: {
        retry: {
          retries: 3
        },
        connectionTimeout: 5000,
        brokers: process.env.KAFKA_BROKERS.split(','),
        ...(process.env.KAFKA_SASL_USER
            ? {
              sasl: {
                mechanism: 'plain',
                username: process.env.KAFKA_SASL_USER,
                password: process.env.KAFKA_SASL_PASSWORD
              },
              ssl: true
            }
            : {}
        )
      },
      consumer: {
        groupId: 'nanny-webapi'
      }
    }
  });
  await app.startAllMicroservicesAsync();
  console.log('Microservices connected');

  await app.listen(port, async () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });
}

bootstrap();
