module.exports = {
  preset: '../../jest.preset.js',
  coverageDirectory: '../../coverage/apps/webapi',
  globals: { 'ts-jest': { tsconfig: '<rootDir>/tsconfig.spec.json' } },
  displayName: 'nanny-webapi',
  testEnvironment: 'node',
};
