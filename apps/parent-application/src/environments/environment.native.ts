import { AuthConfig } from 'angular-oauth2-oidc';
import { RuntimeConfig } from '../app/services/runtime-config';

export const environment = {
  production: true,
  native: true,
  configUrl: 'assets/config.json',
  hmr: false,
  authenticationConfigFactory: authCodeFlowConfigFactory,
};

export function authCodeFlowConfigFactory(config: RuntimeConfig): AuthConfig {
  const auth = config.get('auth');
  return {
    ...auth,
    redirectUri: window.location.origin,
  };
}
