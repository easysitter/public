// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { AuthConfig } from 'angular-oauth2-oidc';
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error';
import { RuntimeConfig } from '../app/services/runtime-config'; // Included with Angular CLI.

export const environment = {
  production: false,
  native: false,
  hmr: false,
  authenticationConfigFactory: authCodeFlowConfigFactory,
  configUrl: 'assets/config.json',
};

export function authCodeFlowConfigFactory(config: RuntimeConfig): AuthConfig {
  const auth = config.get('auth');
  return {
    ...auth,
    redirectUri: window.location.origin,
  };
}
