import { InjectionToken } from '@angular/core';
import { NgxMaterialTimepickerTheme } from 'ngx-material-timepicker';

export const TIME_PICKER_THEME = new InjectionToken<NgxMaterialTimepickerTheme>('TIME_PICKER_THEME');
