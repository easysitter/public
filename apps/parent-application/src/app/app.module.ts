import { APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { WebSocketLink } from '@apollo/client/link/ws';
import { InMemoryCache, split } from '@apollo/client/core';
import { getMainDefinition } from '@apollo/client/utilities';
import {
  ErrorHandler,
  Injectable,
  LOCALE_ID,
  NgModule,
  NgZone,
} from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { Router } from '@angular/router';
import {
  BrowserModule,
  HAMMER_GESTURE_CONFIG,
  HammerGestureConfig,
} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import localeRu from '@angular/common/locales/ru';
import localePl from '@angular/common/locales/pl';

import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { TextFieldModule } from '@angular/cdk/text-field';

import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import {
  AuthenticationGuard,
  AuthenticationModule,
  AuthenticationService,
  OIDC_CONFIG,
} from '@es/authentication';
import { LayoutModule } from '@es/layout';
import { ChildrenModule } from '@es/children';
import { ProfileUiModule } from '@es/profile';
import { RatingModule } from '@es/rating';
import { RequestsModule } from '@es/requests';
import { OPENTOK_API_KEY, VideochatModule } from '@es/videochat';
import { NativeNotificationsModule } from '@es/notifications/native';
import { FIREBASE_CONFIG, WebNotificationsModule } from '@es/notifications/web';

import { AppComponent } from './app.component';
import { AppRouting } from './app.routing';
import { RequestsPageComponent } from './pages/requests-page/requests-page.component';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TIME_PICKER_THEME } from './providers';
import { SearchResultPageComponent } from './pages/search-result-page/search-result-page.component';
import { CreateRequestPageComponent } from './pages/create-request-page/create-request-page.component';
import { SearchPriceSummaryComponent } from './components/search-price-summary/search-price-summary.component';
import { NanniesSelectionListComponent } from './components/nannies-selection-list/nannies-selection-list.component';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { RequestsService } from './services/requests/requests.service';
import { SearchService } from './services/search/search.service';
import { GqlSearchService } from './services/search/gql-search.service';
import { OperationDefinitionNode } from 'graphql';
import { HttpClientModule } from '@angular/common/http';
import { GqlRequestsService } from './services/requests/gql-requests.service';
import { environment } from '../environments/environment';
import { ChatPageComponent } from './pages/chat-page/chat-page.component';
import { ChatGqlRepo, ChatModule } from '@easysitter-chat/chat';
import { DealsPageComponent } from './pages/deals-page/deals-page.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { NannyProfilePageComponent } from './pages/nanny-profile-page/nanny-profile-page.component';
import { DealDetailsPageComponent } from './pages/deal-details-page/deal-details-page.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgxPwaInstallModule } from 'ngx-pwa-install';
import { PwaWrapperComponent } from './components/pwa-wrapper/pwa-wrapper.component';
import { SentryErrorHandler } from './services/sentry-error-handler';
import { SearchDatetimeSelectorComponent } from './components/search-datetime-selector/search-datetime-selector.component';
import { NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { LineDatePikerComponent } from './components/line-date-piker/line-date-piker.component';
import { CustomDateModule } from '@es/custom-date';
import Hammer from 'hammerjs';
import { DealTimeRangeComponent } from './components/deal-time-range/deal-time-range.component';
import { DealStatusBadgeComponent } from './components/deal-status-badge/deal-status-badge.component';
import { DealRealCostComponent } from './components/deal-real-cost/deal-real-cost.component';
import { AUTH_GUARD } from './injection-tokens';
import { NativeUrlOpenService } from './services/native-url-open.service';
import { CarrotquestModule } from '@es/carrotquest';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { CreateDealSummaryDialog } from './components/create-deal-summary/create-deal-summary.dialog';
import {
  Calculator,
  calculatorFactory,
  CalculatorTracer,
  NoopTracer,
} from '@es/calculator';
import { DefaultRuntimeConfig, RuntimeConfig } from './services/runtime-config';

registerLocaleData(localeRu);
registerLocaleData(localePl);

@Injectable()
export class AppHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    swipe: { direction: Hammer.DIRECTION_HORIZONTAL },
    pinch: { enable: false },
    rotate: { enable: false },
    pan: { enable: false },
  };
}

@NgModule({
    declarations: [
        AppComponent,
        RequestsPageComponent,
        SearchPageComponent,
        SearchResultPageComponent,
        CreateRequestPageComponent,
        ChatPageComponent,
        SearchPriceSummaryComponent,
        NanniesSelectionListComponent,
        DealsPageComponent,
        ProfilePageComponent,
        NannyProfilePageComponent,
        DealDetailsPageComponent,
        PwaWrapperComponent,
        SearchDatetimeSelectorComponent,
        LineDatePikerComponent,
        DealTimeRangeComponent,
        DealStatusBadgeComponent,
        DealRealCostComponent,
        ConfirmDialogComponent,
        CreateDealSummaryDialog,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        LayoutModule,
        AppRouting,
        HttpClientModule,
        OAuthModule.forRoot({
            resourceServer: {
                sendAccessToken: true,
            },
        }),
        AuthenticationModule.oidcForRoot(),
        AuthenticationModule,
        CarrotquestModule.forRoot('37399-bf6dd71c4c70de19a916a65de3'),
        environment.native
            ? NativeNotificationsModule.forRoot()
            : WebNotificationsModule,
        ChildrenModule,
        ProfileUiModule,
        RatingModule,
        RequestsModule,
        MatDialogModule,
        MatBottomSheetModule,
        ReactiveFormsModule,
        NgxMaterialTimepickerModule,
        MatDatepickerModule,
        MatNativeDateModule,
        TextFieldModule,
        ChatModule.forRoot(ChatGqlRepo),
        ServiceWorkerModule.register('sw.js', {
            enabled: environment.production && !environment.native,
        }),
        NgxPwaInstallModule.forRoot(),
        NgbTimepickerModule,
        CustomDateModule,
        VideochatModule,
    ],
    providers: [
        { provide: LOCALE_ID, useValue: 'ru' },
        { provide: ErrorHandler, useClass: SentryErrorHandler },
        { provide: HAMMER_GESTURE_CONFIG, useClass: AppHammerConfig },
        {
            provide: FIREBASE_CONFIG,
            useFactory: (config: RuntimeConfig) => config.get('firebase'),
            deps: [RuntimeConfig],
        },
        {
            provide: OPENTOK_API_KEY,
            useFactory: (config: RuntimeConfig) => config.get('opentokApiKey'),
            deps: [RuntimeConfig],
        },
        {
            provide: TIME_PICKER_THEME,
            useValue: {
                clockFace: {
                    clockFaceBackgroundColor: 'var(--white)',
                    clockHandColor: 'var(--secondary)',
                    clockFaceTimeActiveColor: 'var(--white)',
                    // clockFaceBackgroundColor: '#5fa1cb'
                },
                dial: {
                    dialBackgroundColor: 'var(--secondary)',
                    dialEditableActiveColor: 'var(--secondary)',
                },
                container: {
                    buttonColor: 'var(--secondary)',
                },
            },
        },
        {
            provide: APOLLO_OPTIONS,
            useFactory: (httpLink: HttpLink, auth: AuthenticationService, config: RuntimeConfig) => {
                let wsApiProtocol;
                let httpApiProtocol;
                const dataSource = config.get('dataSource');
                if (dataSource.secured) {
                    wsApiProtocol = 'wss://';
                    httpApiProtocol = 'https://';
                }
                else {
                    wsApiProtocol = 'ws://';
                    httpApiProtocol = 'http://';
                }
                const wsUri = wsApiProtocol + dataSource.host;
                const httpUri = httpApiProtocol + dataSource.host;
                const wsClient = new WebSocketLink({
                    uri: `${wsUri}/graphql`,
                    options: {
                        reconnect: true,
                        connectionParams: async () => {
                            const isLoggedIn = await auth.isLoggedIn().toPromise();
                            if (!isLoggedIn) {
                                return {};
                            }
                            const token = await auth.getAccessToken().toPromise();
                            return {
                                Authorization: token ? `Bearer ${token}` : '',
                            };
                        },
                    },
                });
                const httpClient = httpLink.create({
                    uri: `${httpUri}/graphql`,
                });
                const link = split(
                // split based on operation type
                ({ query }) => {
                    const { kind, operation } = getMainDefinition(query) as OperationDefinitionNode;
                    return (kind === 'OperationDefinition' && operation === 'subscription');
                }, wsClient, httpClient);
                return {
                    cache: new InMemoryCache({
                        possibleTypes: {
                            ChatUser: ['ChatParentUser', 'ChatNannyUser'],
                        },
                    }),
                    link,
                };
            },
            deps: [HttpLink, AuthenticationService, RuntimeConfig],
        },
        { provide: RequestsService, useClass: GqlRequestsService },
        { provide: SearchService, useClass: GqlSearchService },
        { provide: AUTH_GUARD, useClass: AuthenticationGuard },
        { provide: OAuthStorage, useValue: localStorage },
        {
            provide: NativeUrlOpenService,
            useFactory: (zone: NgZone, router: Router) => environment.native ? new NativeUrlOpenService(router, zone) : null,
            deps: [NgZone, Router],
        },
        {
            provide: Calculator,
            useValue: calculatorFactory({
                dayRate: 34,
                nightRate: 46,
                overtimeStep: 30,
            }),
        },
        {
            provide: CalculatorTracer,
            useClass: NoopTracer,
        },
        { provide: RuntimeConfig, useExisting: DefaultRuntimeConfig },
        {
            provide: OIDC_CONFIG,
            useFactory: environment.authenticationConfigFactory,
            deps: [RuntimeConfig],
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
