import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { VideochatModule } from '@es/videochat';
import { VideocallByTokenComponent } from './videocall-by-token/videocall-by-token.component';

@NgModule({
  declarations: [VideocallByTokenComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: VideocallByTokenComponent }]),
    VideochatModule,
  ],
})
export class VideochatNativePageModule {}
