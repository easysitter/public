import { Component, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { LayoutService } from '@es/layout';

@Component({
  selector: 'pa-videocall-by-token',
  templateUrl: './videocall-by-token.component.html',
  styleUrls: ['./videocall-by-token.component.scss'],
})
export class VideocallByTokenComponent {
  call = this.route.queryParamMap.pipe(
    map((queryParams) => {
      return {
        session: queryParams.get('session'),
        token: queryParams.get('token'),
      };
    })
  );

  constructor(
    private route: ActivatedRoute,
    private layout: LayoutService,
    private elRef: ElementRef
  ) {}

  ngOnInit(): void {
    this.layout.makeScreen(this.elRef);
  }

  onCallEnded() {
    if (!this.route.snapshot.queryParamMap.has('next')) {
      window.close();
    }
    const next = this.route.snapshot.queryParamMap.get('next');
    window.location.href = next;
  }
}
