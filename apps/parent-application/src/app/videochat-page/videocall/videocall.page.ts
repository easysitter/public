import { Apollo, gql } from 'apollo-angular';
import { Component, ElementRef, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { map, pluck, switchMap } from 'rxjs/operators';

import { LayoutService } from '@es/layout';
import { environment } from '../../../environments/environment';
import { RuntimeConfig } from '../../services/runtime-config';

@Component({
  selector: 'pa-videocall',
  templateUrl: './videocall.page.html',
  styleUrls: ['./videocall.page.css'],
})
export class VideocallPage implements OnInit {
  call = this.getNannyId().pipe(
    switchMap((nannyId) => {
      return this.apollo
        .mutate<{
          createVideochat: { token: { session: string; token: string } };
        }>({
          mutation: gql`
            mutation createVideochat($nanny: ID) {
              createVideochat(nanny: $nanny) {
                token {
                  token
                  session
                }
              }
            }
          `,
          variables: {
            nanny: nannyId,
          },
        })
        .pipe(pluck('data', 'createVideochat', 'token'));
    })
  );

  constructor(
    private apollo: Apollo,
    private layout: LayoutService,
    private elRef: ElementRef,
    private route: ActivatedRoute,
    private router: Router,
    private config: RuntimeConfig
  ) {}

  ngOnInit(): void {
    this.layout.makeScreen(this.elRef);
    if (environment.native) {
      this.call.subscribe(({ token, session }) => {
        const query = new URLSearchParams();
        query.set('session', session);
        query.set('token', token);
        query.set('next', window.location.href);
        const { baseUrl } = this.config.get('videoChat');
        window.location.href = `${baseUrl}/only-video-chat/?${query.toString()}`;
      });
    }
  }

  getNannyId() {
    return this.route.paramMap.pipe(map((params) => params.get('nannyId')));
  }

  onCallEnded() {
    let next = '/requests';
    if (this.route.snapshot.queryParamMap.has('next')) {
      next = this.route.snapshot.queryParamMap.get('next');
    }
    this.router.navigateByUrl(next);
  }
}
