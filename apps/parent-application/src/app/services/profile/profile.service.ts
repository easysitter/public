import {gql, Apollo} from 'apollo-angular';
import { Injectable } from '@angular/core';

import { pluck, shareReplay } from 'rxjs/operators';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private readonly profileQueryRef = this.apollo.watchQuery<{ me: { profile: any } }>({
    query: PARENT_PROFILE_QUERY
  });
  private profile: Observable<any> = this.profileQueryRef.valueChanges.pipe(
    pluck('data', 'profile'),
    shareReplay({ bufferSize: 1, refCount: true })
  );

  constructor(
    private apollo: Apollo
  ) {
  }

  load() {
    return this.profile;
  }

  save({ name, photo, children, email, phone, description, birthDate, address, zipCode }) {
    return this.apollo.mutate({
      mutation: PARENT_PROFILE_MUTATION,
      refetchQueries: [
        { query: PARENT_PROFILE_QUERY }
      ],
      awaitRefetchQueries: true,
      variables: {
        data: { name, photo, children, email, phone, description, birthDate, address, zipCode }
      }
    });
  }
}

const PARENT_PROFILE_QUERY = gql`query profile {
  profile {
    ...on Parent {
      id
      name
      description
      photo
      children
      birthDate
      address
      zipCode
      phone
    }
  }
}`;
const PARENT_PROFILE_MUTATION = gql`mutation updateProfile($data: ParentProfileDTO) {
  updateProfile(data: $data) {
    id
  }
}`;
