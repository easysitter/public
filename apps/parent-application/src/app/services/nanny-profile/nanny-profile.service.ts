import {Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';


import { map, pluck } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NannyProfileService {

  constructor(
    private apollo: Apollo
  ) {
  }

  load(id): Observable<NannyProfile> {
    return this.apollo.query<{ nanny: NannyProfile }>({
      query: NANNY_PROFILE_QUERY,
      variables: { id }
    }).pipe(
      pluck('data', 'nanny'),
      map(({ features, ...profile }) => ({
        ...profile,
        features: JSON.parse(features)
      }))
    );
  }
}

export interface NannyProfile {
  id: number;
  name: string;
  photo: string;
  description: string;
  age: number;
  features: any;
}


const NANNY_PROFILE_QUERY = gql`
  query nanny($id: ID){
    nanny (id: $id) {
      ... on Nanny {
        id
        name
        photo
        description
        age
        price {
          value
        }
        trial {
          value
        }
        trialEnabled
        features
      }
    }
  }
`;
