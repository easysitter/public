import { Inject, Injectable, InjectionToken } from '@angular/core';

export abstract class RuntimeConfig {
  abstract get<K extends keyof Config>(key: K): Config[K];
}

interface Config {
  auth: Readonly<Record<string, any>>;
  dataSource: { host: string; secured: boolean };
  firebase: Readonly<Record<string, any>>;
  opentokApiKey: string;
  videoChat: { baseUrl: string };
}

export const DEFAULT_CONFIG = new InjectionToken('DEFAULT_CONFIG');

@Injectable({
  providedIn: 'root',
})
export class DefaultRuntimeConfig implements RuntimeConfig {
  constructor(@Inject(DEFAULT_CONFIG) private config: Config) {}

  get<K extends keyof Config>(key: K): Config[K] {
    return this.config[key];
  }
}
