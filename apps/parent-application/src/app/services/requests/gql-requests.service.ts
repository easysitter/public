import {Apollo, gql} from 'apollo-angular';
import {
  CreateDealDto,
  CreateRequestDto,
  ExtendedNannyRequest,
  ExtendedParentRequest,
  RequestsService
} from "./requests.service";
import { EMPTY, interval, merge, Observable } from "rxjs";
import { Injectable } from "@angular/core";


import { map, pluck, shareReplay, switchMapTo, tap } from "rxjs/operators";
import { formatTime } from "@es/custom-date";

const NANNY = gql`
  fragment Nanny on Nanny {
    id
    name
    photo
    age
    description
  }
`;

const PARENT_REQUEST = gql`
  fragment ParentRequest on ParentRequest {
    id
    date
    fromTime
    toTime
    children
  }
`;
const NANNY_REQUEST = gql`
  fragment NannyRequest on NannyRequest {
    id
    status
    rate
    nanny {
      ...Nanny
    }
    request {
      ...ParentRequest
    }
  }
  ${NANNY}
  ${PARENT_REQUEST}
`;

const REQUESTS_QUERY = gql`
  query requests {
    requests {
      ...ParentRequest
      nannyRequests {
        ...NannyRequest
      }
    }
  }
  ${PARENT_REQUEST}
  ${NANNY_REQUEST}
`;
const REQUEST_DETAILS_QUERY = gql`
  query request($id: ID, $nanny: ID) {
    request(id: $id, nanny: $nanny) {
      ...NannyRequest
    }
  }
  ${NANNY_REQUEST}
`;
const CREATE_REQUEST = gql`
  mutation createRequest($params: CreateRequestDto) {
    createRequest(params: $params) {
      ...ParentRequest
      nannyRequests {
        ...NannyRequest
      }
    }
  }
  ${PARENT_REQUEST}
  ${NANNY_REQUEST}
`;

const CREATE_DEAL = gql`
  mutation createOffer($data: CreateDealInput) {
    createDeal(data: $data) {
      ...on Deal{
        id
      }
    }
  }
`;

const CANCEL_REQUEST = gql`
  mutation cancelRequest($id: ID) {
    cancelRequest(id: $id)
  }
`;

const REQUEST_STATUS_UPDATE = gql`
  subscription requestStatusUpdate {
    requestsStatusUpdate {
      id
      nannyRequests {
        ...NannyRequest
      }
    }
  }
  ${PARENT_REQUEST}
  ${NANNY_REQUEST}
`;

@Injectable()
export class GqlRequestsService extends RequestsService {
  private requestsQueryRef = this.apollo
    .watchQuery<{ requests: ExtendedParentRequest[] }>({
      query: REQUESTS_QUERY
    });
  private statusUpdates = this.apollo.subscribe({
    query: REQUEST_STATUS_UPDATE
  }).pipe(
    shareReplay({ refCount: true })
  );

  private requests = merge(
    this.requestsQueryRef.valueChanges.pipe(
      pluck("data", "requests"),
      map((requests) => requests.map(({ fromTime, toTime, nannyRequests, ...rest }) => {
        fromTime = formatTime(fromTime, rest.date);
        toTime = formatTime(toTime, rest.date);
        return {
          fromTime,
          toTime,
          nannyRequests: nannyRequests.map(({ request, ...nRequest }) => {
            return {
              ...nRequest,
              request: {
                ...request,
                fromTime,
                toTime
              }
            };
          }),
          ...rest
        };
      }))
    ),
    this.statusUpdates.pipe(
      tap(() => this.requestsQueryRef.refetch()),
      switchMapTo(EMPTY)
    )
  ).pipe(
    shareReplay({ bufferSize: 1, refCount: true })
  );

  constructor(
    private apollo: Apollo
  ) {
    super();
    interval(10000).subscribe(() => this.requestsQueryRef.refetch());
  }

  fetch(): Observable<ExtendedParentRequest[]> {
    return this.requests;
  }

  getDetails(id, nanny): Observable<ExtendedNannyRequest> {
    const queryRef = this.apollo.watchQuery<{ request: ExtendedNannyRequest }>({
      query: REQUEST_DETAILS_QUERY,
      variables: {
        id,
        nanny
      }
    });
    return merge(
      queryRef.valueChanges.pipe(pluck("data", "request")),
      this.statusUpdates.pipe(
        tap(() => queryRef.refetch()),
        switchMapTo(EMPTY)
      )
    ).pipe(
      map(({ request, ...rest }) => {
        return {
          ...rest,
          request: {
            ...request,
            fromTime: formatTime(request.fromTime, request.date),
            toTime: formatTime(request.toTime, request.date)
          }
        };
      }),
      shareReplay({ bufferSize: 1, refCount: true })
    );
  }

  create(data: CreateRequestDto): Observable<any> {
    return this.apollo.mutate({
      mutation: CREATE_REQUEST,
      variables: {
        params: data
      },
      refetchQueries: () => [
        { query: REQUESTS_QUERY }
      ],
      awaitRefetchQueries: true
    }).pipe(
      pluck("data", "createRequest")
    );
  }

  createDeal(data: CreateDealDto): Observable<any> {
    return this.apollo.mutate({
      mutation: CREATE_DEAL,
      variables: {
        data
      },
      refetchQueries: () => [
        {
          query: REQUESTS_QUERY
        }
      ]
    }).pipe(
      pluck("data", "createDeal")
    );
  }

  cancel(requestId: ExtendedParentRequest["id"]): Observable<boolean> {
    return this.apollo.mutate({
      mutation: CANCEL_REQUEST,
      variables: {
        id: requestId
      },
      optimisticResponse: {
        __typename: "Mutation",
        cancelRequest: true
      },
      update: (proxy, result) => {
        if (!result.data.cancelRequest) {
          return;
        }
        const stored = proxy.readQuery<{ requests: ExtendedParentRequest[] }>({
          query: REQUESTS_QUERY
        });
        const data = {
          ...stored,
          requests: stored.requests.filter(request => request.id !== requestId)
        };
        proxy.writeQuery({
          query: REQUESTS_QUERY,
          data
        });
      },
      refetchQueries: () => [
        {
          query: REQUESTS_QUERY
        }
      ],
      awaitRefetchQueries: true
    }).pipe(
      pluck("data", "cancelRequest")
    );
  }
}
