import { EMPTY, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  CreateDealDto,
  CreateRequestDto,
  ExtendedNannyRequest,
  ExtendedParentRequest,
  RequestsService
} from './requests.service';
import { NannyRequestStatus } from '@es/requests';
import { delay } from 'rxjs/operators';

@Injectable()
export class MockRequestsService extends RequestsService {
  fetch(): Observable<ExtendedParentRequest[]> {
    return of(requests).pipe(
      delay(Math.random() * 1000)
    );
  }

  getDetails(id, nanny): Observable<ExtendedNannyRequest> {
    return of(requests[0].nannyRequests[0]);
  }

  create(data: CreateRequestDto): Observable<any> {
    return of(data).pipe(
      delay(Math.random() * 1000)
    );
  }

  createDeal(param: CreateDealDto): Observable<any> {
    return EMPTY;
  }

  cancel(data: any): Observable<boolean> {
    return EMPTY;
  }
}

const requests: ExtendedParentRequest[] = Array.from({ length: 2 }, (_, id) => {

  const parentRequest = {
    expanded: false,
    id: id + 1,
    date: new Date('2019-11-30T05:00:00.000Z'),
    fromTime: '18:00',
    toTime: '21:00',
    children: [2, 4, 6],
    trial: true,
    address: '',
    parent: {
      name: '',
      id: 1,
      photo: ''
    },
    wishes: ''
  };
  return {
    ...parentRequest,
    nannyRequests: [{
      id: '1',
      status: NannyRequestStatus.New,
      nanny: {
        id: 1,
        name: 'Катя Соколова',
        age: 23,
        photo: 'assets/images/funny-bday-cat.jpg',
        rating: 5
      },
      request: parentRequest,
      rate: 10
    }, {
      id: '2',
      status: NannyRequestStatus.Closed,
      nanny: {
        id: 2,
        name: 'Валя Смирнова',
        age: 28,
        photo: 'assets/images/funny-bday-cat.jpg',
        rating: 4
      },
      request: parentRequest,
      rate: 3
    }, {
      id: '3',
      status: NannyRequestStatus.Accepted,
      nanny: {
        id: 3,
        name: 'Дурдона М.',
        age: 68,
        photo: 'assets/images/funny-bday-cat.jpg',
        rating: 1
      },
      request: parentRequest,
      rate: 34
    }]
  };
});
