import { Observable } from 'rxjs';
import { NannyRequest } from '@es/requests';

export interface CreateRequestDto {
  search: string
  nannies: number[]
  address: string
  phone: string
  wishes: string
  zipCode: string
}

export interface CreateDealDto {
  request: number;
  nanny: number;
}

export interface ExtendedNannyRequest extends NannyRequest {
  nanny: any
}

export interface ExtendedParentRequest {
  nannyRequests: ExtendedNannyRequest[];

  [key: string]: any
}

export abstract class RequestsService {
  abstract fetch(): Observable<ExtendedParentRequest[]>;

  abstract getDetails(id, nanny): Observable<ExtendedNannyRequest>

  abstract create(data: CreateRequestDto): Observable<any>;

  abstract cancel(data: ExtendedParentRequest['id']): Observable<boolean>;

  abstract createDeal(data: CreateDealDto): Observable<any>
}
