import { ErrorHandler, Injectable } from '@angular/core';
import * as Sentry from '@sentry/browser';
import { environment } from '../../environments/environment';

if (environment.production) {
  Sentry.init({
    dsn: 'https://9c6c12c6912848baa376bba885268aa7@o388284.ingest.sentry.io/5225489'
  });
}
@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  handleError(error: any): void {
    console.error(error.originalError || error);
    const eventId = Sentry.captureException(error.originalError || error);
    // Sentry.showReportDialog({ eventId });
  }
}
