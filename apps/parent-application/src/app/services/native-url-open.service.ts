import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';

const { App } = Plugins;

@Injectable()
export class NativeUrlOpenService {

  constructor(
    private router: Router,
    private zone: NgZone
  ) {
  }

  initializeOAuthNativeHandler(): void {

    App.addListener('appUrlOpen', (data: any) => {
      this.zone.run(async () => {
        // Example url: pl.easysitter.parent.cap:/requests
        // slug = /tabs/tab2
        const slug = data.url.split(':').pop();
        if (!slug) return;
        const url = new URL(window.origin + slug);
        if (isResponse(slug)) {
          // @ts-ignore
          const authResponseParamsMap = Array.from(url.searchParams).reduce((params, [key, value]) => {
            params[key] = value;
            return params;
          }, {});
          await this.router.navigate(['.'], { queryParams: authResponseParamsMap });
          window.location.reload();
        }
        if (url.searchParams.has('logout')) {
          await this.router.navigate(['/'], { queryParams: {} });
        }
      });
    });
  }
}

const checks = [
  /[?|&|#]code=/,
  /[?|&|#]error=/,
  /[?|&|#]token=/,
  /[?|&|#]id_token=/
];

export function isResponse(str): boolean {
  if (!str) {
    return false;
  }
  for (let i = 0; i < checks.length; i++) {
    if (str.match(checks[i])) {
      return true;
    }
  }
  return false;
}
