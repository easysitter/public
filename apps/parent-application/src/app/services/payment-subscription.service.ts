import {Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';


import { pluck, shareReplay, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';

const HAS_ACTIVE_SUBSCRIPTION = gql`
  query hasActiveSubscription {
    hasActiveSubscription
  }
`;

const REQUEST_SERVICE_SUBSCRIPTION_PAYMENT_URL = gql`
  mutation requestSubscriptionPaymentUrl($returnUrl: String) {
    requestSubscriptionPaymentUrl(returnUrl: $returnUrl)
  }
`;

const SUBSCRIPTION_ACTIVATED = gql`
  subscription subscriptionActivated {
    subscriptionActivated
  }
`;

@Injectable({
  providedIn: 'root'
})
export class PaymentSubscriptionService {
  private hasActiveSubscriptionQueryRef = this.apollo.watchQuery<{ hasActiveSubscription: boolean }>({
    query: HAS_ACTIVE_SUBSCRIPTION
  });

  private hasActiveSubscriptionSubject = this.hasActiveSubscriptionQueryRef
    .valueChanges
    .pipe(
      pluck('data', 'hasActiveSubscription'),
      startWith(false),
      shareReplay({ bufferSize: 1, refCount: true })
    );

  constructor(
    private readonly apollo: Apollo
  ) {
    this.hasActiveSubscriptionQueryRef.subscribeToMore<{ subscriptionActivated: boolean }>({
      document: SUBSCRIPTION_ACTIVATED,
      updateQuery: (prev, { subscriptionData }) => {
        const { data } = subscriptionData;
        if (!data) {
          return prev;
        }
        return {
          ...prev,
          hasActiveSubscription: data.subscriptionActivated
        };
      }
    });
  }

  hasActiveSubscription() {
    return this.hasActiveSubscriptionSubject;
  }

  requestPaymentLink(returnUrl: string): Observable<string> {
    return this.apollo.mutate({
      mutation: REQUEST_SERVICE_SUBSCRIPTION_PAYMENT_URL,
      variables: {
        returnUrl
      }
    }).pipe(
      pluck('data', 'requestSubscriptionPaymentUrl')
    );
  }
}
