import { EMPTY, Observable, of, throwError, timer } from 'rxjs';
import { Injectable } from '@angular/core';
import { delay, switchMapTo } from 'rxjs/operators';
import { addDays } from 'date-fns';
import { SearchParams, SearchResult, SearchResultNanny, SearchService } from './search.service';

@Injectable()
export class MockSearchService extends SearchService {
  create(params: SearchParams): Observable<SearchResult> {
    if (params.trial) {
      return timer(1000).pipe(
        switchMapTo(throwError(new Error('strange err')))
      );
    }
    return of(mockSearchResult).pipe(
      delay(1000)
    );
  }

  getResult(id: SearchResult['id']): Observable<SearchResult> {
    return of(mockSearchResult).pipe(
      delay(1000)
    );
  }

  approveNannies(searchId: SearchResult['id'], nannyIds: SearchResultNanny[]): Observable<SearchResult> {
    return of(mockSearchResult).pipe(
      delay(1000)
    );
  }

  getPrefilledData(): Observable<any> {
    return EMPTY;
  }
}

const mockSearchResult: SearchResult = {
  id: 1,
  date: addDays(new Date(), 2),
  fromTime: '12:00',
  toTime: '14:10',
  trial: true,
  children: [1, 2],
  nannies: [{
    id: 1,
    description: 'Люблю детей. Быстро устанавливаю контакт с ними.',
    name: 'Anastazja Andrzejewska-Baranowska',
    age: '49',
    rate: 3,
    photo: 'assets/images/funny-bday-cat.jpg',
    isApproved: true
  }, {
    id: 2,
    description: 'Люблю детей. Быстро устанавливаю контакт с ними.',
    name: 'Katarzyna Augustyniak',
    age: '28',
    rate: 9,
    photo: 'assets/images/funny-bday-cat.jpg',
    isApproved: true
  }, {
    id: 3,
    description: 'Люблю детей. Быстро устанавливаю контакт с ними.',
    name: 'Анастасия М.',
    age: '36',
    rate: 9,
    photo: 'assets/images/funny-bday-cat.jpg',
    isApproved: true
  }, {
    id: 4,
    description: 'Люблю детей. Быстро устанавливаю контакт с ними.',
    name: 'Анастасия М.',
    age: '36',
    rate: 9,
    photo: 'assets/images/funny-bday-cat.jpg',
    isApproved: true
  }, {
    id: 5,
    description: 'Люблю детей. Быстро устанавливаю контакт с ними.',
    name: 'Анастасия М.',
    age: '36',
    rate: 29,
    photo: 'assets/images/funny-bday-cat.jpg',
    isApproved: true
  }]
};
