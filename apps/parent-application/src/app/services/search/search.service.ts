import { Observable } from 'rxjs';

export interface SearchParams {
  date: Date;
  fromTime: string;
  toTime: string;
  trial: boolean;
  children: number[]
}

export interface SearchResultNanny {
  id: number;
  rate: number;
  name: string;
  age: string;
  photo: string;
  description: string;
  isApproved: boolean;
}

export interface SearchResult {
  id: number;
  date: Date;
  fromTime: string;
  toTime: string;
  trial: boolean;
  children: number[];
  nannies: SearchResultNanny[]
}

export abstract class SearchService {
  abstract create(params: SearchParams): Observable<SearchResult>;

  abstract getResult(id: SearchResult['id']): Observable<SearchResult>;

  abstract approveNannies(
    searchId: SearchResult['id'],
    nannyIds: SearchResultNanny[]
  ): Observable<SearchResult>;

  abstract getPrefilledData(): Observable<any>
}
