import { gql, Apollo } from 'apollo-angular';
import {
  SearchParams,
  SearchResult,
  SearchResultNanny,
  SearchService,
} from './search.service';

import { Observable } from 'rxjs';
import { map, pluck } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { format } from 'date-fns';
import { formatTime } from '@es/custom-date';

const SEARCH_RESULT_QUERY = gql`
  query search($searchId: ID) {
    searchResult(searchId: $searchId) {
      ... on SearchResult {
        id
        date
        fromTime
        toTime
        children
        nannies {
          id
          age
          name
          photo
          description
          rate
          isApproved
        }
        trial
      }
    }
  }
`;
const RUN_SEARCH = gql`
  mutation search($params: SearchParams) {
    search(params: $params) {
      ... on SearchResult {
        id
      }
    }
  }
`;
const APPROVE_NANNIES = gql`
  mutation approveNannies($searchId: ID, $nannies: [ID!]!) {
    approveNannies(searchId: $searchId, nannies: $nannies) {
      ... on SearchResult {
        id
        nannies {
          id
          isApproved
        }
      }
    }
  }
`;

@Injectable()
export class GqlSearchService extends SearchService {
  constructor(private apollo: Apollo) {
    super();
  }

  approveNannies(
    searchId: number,
    nannies: SearchResultNanny[]
  ): Observable<SearchResult> {
    return this.apollo
      .mutate<{ approveNannies: SearchResult }>({
        mutation: APPROVE_NANNIES,
        variables: {
          searchId,
          nannies: nannies.map((n) => n.id),
        },
      })
      .pipe(pluck('data', 'approveNannies'));
  }

  create(params: SearchParams): Observable<SearchResult> {
    const tz = format(new Date(), 'X');
    return this.apollo
      .mutate<{ search: SearchResult }>({
        mutation: RUN_SEARCH,
        variables: {
          params: {
            ...params,
            fromTime: `${params.fromTime}${tz}`,
            toTime: `${params.toTime}${tz}`,
          } as SearchParams,
        },
      })
      .pipe(pluck('data', 'search'));
  }

  getResult(id: number): Observable<SearchResult> {
    return this.apollo
      .query<{ searchResult: SearchResult }>({
        query: SEARCH_RESULT_QUERY,
        variables: {
          searchId: id,
        },
      })
      .pipe(
        pluck('data', 'searchResult'),
        map(({ fromTime, toTime, ...rest }) => {
          return {
            ...rest,
            fromTime: formatTime(fromTime, rest.date),
            toTime: formatTime(toTime, rest.date),
          };
        })
      );
  }

  getPrefilledData(): Observable<SearchPrefillData> {
    return this.apollo
      .query<{ searchPrefilled: SearchPrefillData }>({
        query: gql`
          query searchPrefilled {
            searchPrefilled {
              address
              zipCode
              phone
              wishes
            }
          }
        `,
      })
      .pipe(pluck('data', 'searchPrefilled'));
  }
}

interface SearchPrefillData {
  address: string;
  phone: string;
  wishes: string;
}
