import {Apollo, gql} from 'apollo-angular';


import { EMPTY, interval, merge, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { filter, map, pluck, shareReplay, switchMap, switchMapTo, tap } from "rxjs/operators";
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../components/confirm-dialog/confirm-dialog.component';
import { formatTime } from "@es/custom-date";

@Injectable({
  providedIn: 'root'
})
export class DealService {
  private updates = this.apollo.subscribe({
    query: DEAL_STATUS_UPDATE
  }).pipe(
    pluck('data', 'dealStatusUpdate')
  );
  private dealsQueryRef = this.apollo.watchQuery<{deals: any[]}>({
    query: DEALS_QUERY
  });

  private deals$ = merge(
    this.dealsQueryRef.valueChanges.pipe(
      pluck('data', 'deals'),
      map((deals) => deals.map(({ fromTime, toTime, ...rest }) => {
        return {
          fromTime: formatTime(fromTime, rest.date),
          toTime: formatTime(toTime, rest.date),
          ...rest
        };
      }))
    ),
    this.updates.pipe(
      tap(() => this.dealsQueryRef.refetch()),
      switchMapTo(EMPTY)
    )
  ).pipe(
    shareReplay({ refCount: true, bufferSize: 1 })
  );

  constructor(
    private readonly apollo: Apollo,
    private readonly dialog: MatDialog
  ) {
    this.apollo.subscribe({
      query: DEAL_STATUS_UPDATE
    }).pipe(
      tap(() => this.dealsQueryRef.refetch())
    ).subscribe();
    interval(10000).subscribe(() => this.dealsQueryRef.refetch());
  }

  fetch(): Observable<any> {
    return this.deals$;
  }

  load(id): Observable<any> {
    const queryRef = this.apollo.watchQuery<{ deal: any }>({
      query: DEAL_DETAILS_QUERY,
      variables: { id }
    });
    queryRef.subscribeToMore({ document: DEAL_STATUS_UPDATE });
    return queryRef.valueChanges.pipe(
      pluck('data', 'deal'),
      map(({ fromTime, toTime, ...rest }) => {
        return {
          fromTime: formatTime(fromTime, rest.date),
          toTime: formatTime(toTime, rest.date),
          ...rest
        };
      })
    );
  }

  cancel(deal): Observable<any> {
    const ref = this.dialog.open(ConfirmDialogComponent);
    return ref.afterClosed().pipe(
      tap(console.log),
      filter((isConfirmed) => !!isConfirmed),
      switchMap(() => this.apollo.mutate({
        mutation: DEAL_CANCEL,
        variables: { id: deal.id },
        optimisticResponse: {
          __typename: 'Mutation',
          cancelVisit: {
            __typename: 'Deal',
            id: deal.id,
            startedAt: deal.startedAt,
            finishedAt: deal.finishedAt,
            isActive: false,
            tag: 'cancelled'

          },
          pendingPayment: null
        },
        refetchQueries: result => [
          {
            query: DEALS_QUERY
          },
          {
            query: DEAL_DETAILS_QUERY,
            variables: {
              id: deal.id
            }
          }
        ],
        awaitRefetchQueries: true
      }))
    );
    return;
  }

  submitReview(deal, review) {
    return this.apollo.mutate<Extends<{ submitReview: any }>>({
      mutation: SUBMIT_REVIEW,
      variables: { deal, review },
      optimisticResponse: {
        __typename: 'Mutation',
        submitReview: {
          __typename: 'Deal',
          id: deal,
          review: {
            __typename: 'Review',
            id: `${Date.now()}`,
            ...review
          }
        }
      }
    });
  }
}

const NANNY_STATIC_FRAGMENT = gql`
  fragment NannyStatic on Nanny {
    id
    name
    photo
  }
`;
const DEAL_STATIC_FRAGMENT = gql`
  fragment DealStatic on Deal{
    id
    date
    fromTime
    toTime
    rate
    children
    wishes
  }
`;
const DEAL_DYNAMIC_FRAGMENT = gql`
  fragment DealDynamic on Deal{
    startedAt
    finishedAt
    isActive
    tag
    review {
      id
      rating
      pros
      cons
    }
    pendingPayment {
      id
      checkoutUrl
      amount
      currency
    }
  }
`;
const DEAL_DETAILS_QUERY = gql`
  query deal($id: ID){
    deal(id: $id) {
      ...DealStatic
      ...DealDynamic
      nanny {
        ...NannyStatic
      }
    }
  }
  ${DEAL_STATIC_FRAGMENT}
  ${DEAL_DYNAMIC_FRAGMENT}
  ${NANNY_STATIC_FRAGMENT}
`;
const DEALS_QUERY = gql`
  query {
    deals {
      ...DealStatic
      ...DealDynamic
      nanny {
        ...NannyStatic
      }
    }
  }
  ${DEAL_STATIC_FRAGMENT}
  ${DEAL_DYNAMIC_FRAGMENT}
  ${NANNY_STATIC_FRAGMENT}
`;

const DEAL_STATUS_UPDATE = gql`
  subscription {
    dealStatusUpdate {
      id
      ...DealDynamic
    }
  }
  ${DEAL_DYNAMIC_FRAGMENT}
`;

const SUBMIT_REVIEW = gql`
  mutation submitReview($deal: ID, $review: ReviewInput) {
    submitReview(deal: $deal, review: $review) {
      id
      review {
        id
        rating
        cons
        pros
      }
    }
  }
`;

const DEAL_CANCEL = gql`
  mutation cancelDeal($id: ID) {
    cancelVisit(id: $id) {
      id
      ...DealDynamic
    }
  }

  ${DEAL_DYNAMIC_FRAGMENT}
`;
type Extends<T> = T & any;
