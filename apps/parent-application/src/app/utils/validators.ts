import { TimeRange } from './time-range';

export const timeRangeValidator = (control) => {
  const { fromTime, toTime } = control.value;
  const timeRange = new TimeRange(fromTime, toTime);

  if (timeRange.getDuration().hours >= 2) return null;
  return {
    timeRange: 'Invalid time range'
  };
};
export const emptyArrayValidator = control => control.value.length ? null : { emptyArray: 'Array should not be empty ' };
