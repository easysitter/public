import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export function extractFromRoute(activatedRoute: ActivatedRoute, param: string): Observable<any> {
  return activatedRoute.paramMap.pipe(
    map(params => params.get(param))
  );
}
