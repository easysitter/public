import { Observable, Subject } from 'rxjs';
import { map, scan, shareReplay, startWith, switchMap } from 'rxjs/operators';
import { SearchResultNanny } from '../services/search/search.service';

export class NanniesSelector {
  private actions = new Subject<{ selected: boolean, nanny: SearchResultNanny }>();

  private selected = this.nannies.pipe(
    switchMap(nannies => {
      return this.actions.pipe(
        startWith(null),
        scan((selected, next: { selected: boolean, nanny: SearchResultNanny }) => {
          if (!next) return selected;
          if (next.selected) {
            if (selected.includes(next.nanny.id)) return selected;
            return [...selected, next.nanny.id];
          } else {
            return selected.filter(nannyId => nannyId !== next.nanny.id);
          }
        }, this.initiallyAllSelected ? nannies.map(nanny => nanny.id) : []),
        map(selected => nannies.filter(({ id }) => selected.includes(id)))
      );
    }),
    startWith([]),
    shareReplay({ bufferSize: 1, refCount: true })
  );

  constructor(
    private nannies: Observable<SearchResultNanny[]>,
    private initiallyAllSelected = false
  ) {
  }

  select(nanny: SearchResultNanny) {
    this.actions.next({ selected: true, nanny });
  }

  unselect(nanny: SearchResultNanny) {
    this.actions.next({ selected: false, nanny });
  }

  getSelected(): Observable<SearchResultNanny[]> {
    return this.selected;
  }

  getSelectedCount() {
    return this.getSelected().pipe(
      map(nannies => nannies.length)
    );
  }
}
