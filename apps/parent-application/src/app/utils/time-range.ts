export class TimeRange {
  private readonly fromHour;
  private readonly fromMinute;
  private readonly toHour;
  private readonly toMinute;

  constructor(
    from: string, to: string
  ) {
    const parsedFrom = this.parseTime(from);
    const parsedTo = this.parseTime(to);
    this.fromHour = parsedFrom[0];
    this.fromMinute = parsedFrom[1];
    this.toHour = parsedTo[0] < 6 ? parsedTo[0] + 24 : parsedTo[0];
    this.toMinute = parsedTo[1];
  }

  getDuration() {
    return {
      hours: this.toHour - this.fromHour,
      minutes: this.toMinute - this.fromMinute
    };
  }

  private parseTime(time: string): [number, number] {
    return time.split(':').map(Number) as [number, number];
  }
}
