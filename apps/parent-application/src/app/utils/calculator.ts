import { Subject } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { TimeRange } from './time-range';

export class Calculator {
  private readonly updatesSubject = new Subject();
  public updates = this.updatesSubject.asObservable();
  public readonly maxRate = this.updates.pipe(pluck('maxRate'));
  public readonly minRate = this.updates.pipe(pluck('minRate'));
  public readonly maxPrice = this.updates.pipe(pluck('maxPrice'));
  public readonly minPrice = this.updates.pipe(pluck('minPrice'));
  private duration = this.timeRange.getDuration();

  constructor(
    private readonly timeRange: TimeRange
  ) {
  }

  updateRates(rates) {
    const { max, min } = rates.reduce((reduced, rate) => {
      if (reduced.min === undefined) reduced.min = rate;
      if (reduced.max === undefined) reduced.max = rate;
      if (rate > reduced.max) {
        reduced.max = rate;
      }
      if (rate < reduced.min) {
        reduced.min = rate;
      }
      return reduced;
    }, { min: undefined, max: undefined });
    this.updatesSubject.next({
      minRate: min,
      maxRate: max,
      minPrice: min * this.duration.hours,
      maxPrice: max * (this.duration.hours * 60 + this.duration.minutes) / 60
    });
  }
}
