import { Component, HostBinding, Inject, OnInit } from '@angular/core';
import { RequestsService } from '../../services/requests/requests.service';
import { NannyRequest } from '@es/requests';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { catchError, filter, switchMap, take, timeout } from 'rxjs/operators';
import { DealService } from '../../services/deal.service';
import { addDays, format, isBefore, parse } from "date-fns";
import { defer, of, throwError, TimeoutError, timer } from 'rxjs';
import { Calculator, CalculatorTracer } from '@es/calculator';

@Component({
  selector: 'pa-create-deal-summary',
  templateUrl: './create-deal-summary.dialog.html',
  styleUrls: ['./create-deal-summary.dialog.css']
})
export class CreateDealSummaryDialog implements OnInit {

  @HostBinding('class.loading')
  processing = false;
  price = defer(() => {
    const tz = format(new Date(), "X");
    const request = this.request.request;
    const date = new Date(request.date);
    const fromDate = parse(`${request.fromTime}${tz}`, 'HH:mmX', date);
    let toDate = parse(`${request.toTime}${tz}`, 'HH:mmX', date);
    if (isBefore(toDate, fromDate)) {
      toDate = addDays(toDate, 1);
    }
    const params = {
      from: fromDate,
      to: toDate,
      children: request.children
    };
    return this.calculator.getPrice(params, this.calculatorTracer);
  })


  constructor(
    private requestsService: RequestsService,
    private dealService: DealService,
    private dialogRef: MatDialogRef<CreateDealSummaryDialog>,
    @Inject(MAT_DIALOG_DATA)
    public readonly request: NannyRequest,
    private calculator: Calculator,
    private calculatorTracer: CalculatorTracer,
  ) {

  }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }

  createDeal() {
    this.processing = true;
    this.requestsService.createDeal({
      request: this.request.request.id,
      nanny: typeof this.request.nanny === 'object' ? this.request.nanny.id : this.request.nanny
    }).pipe(
      switchMap((deal) => {
        return timer(0, 1000).pipe(
          switchMap(() => this.dealService.load(deal.id)),
          filter(({ pendingPayment }) => !!pendingPayment),
          timeout(5000),
          catchError(err => {
            if (err instanceof TimeoutError) {
              return of(deal)
            }
            return throwError(err);
          }),
          take(1),

        );
      })
    ).subscribe(
      (deal) => {
        if (deal.pendingPayment) {
          location.href = deal.pendingPayment.checkoutUrl;
        } else {
          this.dialogRef.close(deal);
        }
      },
      err => {
        this.processing = false;
      }
    );
  }
}
