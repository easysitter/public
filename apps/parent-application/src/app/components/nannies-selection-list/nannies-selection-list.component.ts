import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { SearchResultNanny } from '../../services/search/search.service';

interface SearchResultNannyWithSelection {
  info: SearchResultNanny;
  selected: boolean;
}

@Component({
  selector: 'pa-nannies-selection-list',
  templateUrl: './nannies-selection-list.component.html',
  exportAs: 'nannySelection'
})
export class NanniesSelectionListComponent implements OnChanges {
  @Input() nannies: SearchResultNanny[] = [];
  @Input() selected: SearchResultNanny[] = [];
  @Output() selectionChanged = new EventEmitter<{ nanny: SearchResultNanny, selected: boolean }>();

  selectedSet: Set<SearchResultNanny['id']> = new Set();

  ngOnChanges(changes: SimpleChanges): void {
    if ('selected' in changes) {
      this.selectedSet.clear();
      if (this.selected) {
        this.selected.forEach(nanny => this.selectedSet.add(nanny.id));
      }
    }
  }

  toggleNannySelection(nanny) {
    this.selectionChanged.emit({
      nanny, selected: !this.selectedSet.has(nanny.id)
    });
  }

  selectAll() {
    this.nannies.forEach((nanny) => {
      this.selectionChanged.emit({
        nanny, selected: !this.selectedSet.has(nanny.id)
      });
    });
  }

  trackById(idx, nanny) {
    return nanny.id;
  }

  isSelected(nanny: SearchResultNanny) {
    return this.selectedSet.has(nanny.id);
  }
}
