import { Component, Input } from '@angular/core';

@Component({
  selector: 'pa-deal-time-range',
  templateUrl: './deal-time-range.component.html',
  styleUrls: ['./deal-time-range.component.css']
})
export class DealTimeRangeComponent {
  @Input() deal: any;

}
