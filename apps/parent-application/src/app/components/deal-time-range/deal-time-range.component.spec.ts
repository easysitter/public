import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealTimeRangeComponent } from './deal-time-range.component';

describe('DealTimeRangeComponent', () => {
  let component: DealTimeRangeComponent;
  let fixture: ComponentFixture<DealTimeRangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DealTimeRangeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealTimeRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
