import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPriceSummaryComponent } from './search-price-summary.component';

describe('SearchPriceSummaryComponent', () => {
  let component: SearchPriceSummaryComponent;
  let fixture: ComponentFixture<SearchPriceSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchPriceSummaryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPriceSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
