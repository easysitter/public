import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges
} from '@angular/core';
import { TimeRange } from '../../utils/time-range';
import { Calculator } from '../../utils/calculator';
import { Subscription } from 'rxjs';

@Component({
  selector: 'pa-search-price-summary',
  templateUrl: './search-price-summary.component.html',
  styleUrls: ['./search-price-summary.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchPriceSummaryComponent implements OnChanges, OnDestroy {
  @Input() nannies: { rate: number }[] = [];
  @Input() fromTime: string;
  @Input() toTime: string;
  timeRange: TimeRange;
  duration: { hours: number; minutes: number };
  calculator: Calculator;
  private calculatorSubscription: Subscription;

  constructor(
    private cd: ChangeDetectorRef
  ) {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('fromTime' in changes || 'toTime' in changes) {
      this.applyTimeRangeChanges();
    }
    if ('nannies' in changes && this.calculator) {
      this.updateRates();
    }
  }

  ngOnDestroy(): void {
    this.teardownCalculator();
  }

  private applyTimeRangeChanges() {
    this.teardownCalculator();
    this.setupTimeRange();
    this.setupCalculator();
    this.updateRates();
  }

  private setupTimeRange() {
    this.timeRange = new TimeRange(this.fromTime, this.toTime);
    this.duration = this.timeRange.getDuration();
  }

  private setupCalculator() {
    this.calculator = new Calculator(this.timeRange);
    this.calculatorSubscription = this.calculator.updates.subscribe(() => {
      this.cd.detectChanges();
    });
  }

  private updateRates() {
    const rates = this.nannies.map(nanny => nanny.rate);
    this.calculator.updateRates(rates);
  }

  private teardownCalculator() {
    if (this.calculator) {
      this.calculator = null;
    }
    if (this.calculatorSubscription) {
      this.calculatorSubscription.unsubscribe();
      this.calculatorSubscription = null;
    }
  }
}
