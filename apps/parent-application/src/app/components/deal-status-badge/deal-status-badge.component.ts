import { Component, Input } from '@angular/core';

@Component({
  selector: 'pa-deal-status-badge',
  templateUrl: './deal-status-badge.component.html',
  styleUrls: ['./deal-status-badge.component.css']
})
export class DealStatusBadgeComponent {
  @Input() deal: any;

}
