import {
  AfterViewInit,
  Component,
  EventEmitter,
  Inject,
  Injectable,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TIME_PICKER_THEME } from '../../providers';
import { NgxMaterialTimepickerComponent, NgxMaterialTimepickerTheme } from 'ngx-material-timepicker';
import {
  addDays,
  addHours,
  addMonths,
  differenceInHours,
  endOfDay,
  format,
  isAfter,
  isBefore,
  parse,
  roundToNearestMinutes,
  startOfDay,
  subDays
} from 'date-fns';
import { NgbTimeAdapter, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;

@Injectable()
export class NgbTimeStringAdapter extends NgbTimeAdapter<string> {
  fromModel(value: string | null): NgbTimeStruct | null {
    if (!value) return null;
    const split = value.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: 0
    };
  }

  toModel(time: NgbTimeStruct | null): string | null {
    return time != null ? `${pad(time.hour)}:${pad(time.minute)}` : null;
  }


}

@Component({
  selector: 'pa-search-datetime-selector[formGroup]',
  templateUrl: './search-datetime-selector.component.html',
  styleUrls: ['./search-datetime-selector.component.css'],
  providers: [{
    provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter
  }]
})
export class SearchDatetimeSelectorComponent implements OnInit, AfterViewInit {
  @Input() minutesGap = 10;
  @Input() minDate: Date;
  @Input('formGroup') form: FormGroup;

  @Output() done = new EventEmitter();

  @ViewChild('fromTimePicker') fromTimePickerComponent: NgxMaterialTimepickerComponent;
  @ViewChild('toTimePicker') toTimePickerComponent: NgxMaterialTimepickerComponent;

  constructor(
    @Inject(TIME_PICKER_THEME)
    public readonly timePickerTheme: NgxMaterialTimepickerTheme
  ) {
  }

  dateFilter = (date: Date) => {
    const intervalStart = startOfDay(subDays(new Date(), 1));
    const intervalEnd = endOfDay(addMonths(intervalStart, 1));
    return isAfter(date, intervalStart) && isBefore(date, intervalEnd);
  };

  ngOnInit() {
    this.handleTimeChanges();
  }

  ngAfterViewInit() {
  }

  setDate(newDate) {
    this.form.get('date').patchValue(newDate);
    this.openFromPicker();
  }

  openFromPicker() {
    this.fromTimePickerComponent.open();
  }

  whenFromTimeSet() {
    this.fromTimePickerComponent.close();
    setTimeout(() => this.toTimePickerComponent.open(), 1);
  }

  close() {
    this.done.emit();
  }

  closePicker(picker: NgxMaterialTimepickerComponent) {
    picker.close();
    if (picker === this.toTimePickerComponent) {
      this.fromTimePickerComponent.open();
    }
  }

  private handleTimeChanges() {
    this.form.get('fromTime').valueChanges.subscribe(
      fromTime => {
        this.correctFromTime(fromTime);
        this.correctToTime(fromTime, this.form.value.toTime);
      }
    );
    this.form.get('toTime').valueChanges.subscribe(
      toTime => this.correctToTime(this.form.value.fromTime, toTime)
    );
  }

  private correctFromTime(fromTime) {
    const { date } = this.form.value;
    const fromTimeDate = parse(fromTime, 'HH:mm', date);
    if (fromTimeDate < this.minDate) {
      this.form.patchValue({
        fromTime: format(this.minDate, 'HH:mm')
      });
    }
  }

  private correctToTime(fromTime, toTime) {
    const { date } = this.form.value;
    const fromTimeDate = parse(fromTime, 'HH:mm', date);
    let toTimeDate = parse(toTime, 'HH:mm', date);
    if (isBefore(toTimeDate, fromTimeDate)) {
      toTimeDate = addDays(toTimeDate, 1)
    }
    if (differenceInHours(toTimeDate, fromTimeDate) < 2) {
      this.form.patchValue({
        toTime: this.formatTime(addHours(fromTimeDate, 2))
      });
    }
  }

  private formatTime(fromTime: Date) {
    return format(roundToNearestMinutes(fromTime, { nearestTo: this.minutesGap }), 'HH:mm');
  }
}
