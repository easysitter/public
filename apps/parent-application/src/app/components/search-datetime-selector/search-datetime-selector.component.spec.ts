import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDatetimeSelectorComponent } from './search-datetime-selector.component';

describe('SearchDatetimeSelectorComponent', () => {
  let component: SearchDatetimeSelectorComponent;
  let fixture: ComponentFixture<SearchDatetimeSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchDatetimeSelectorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDatetimeSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
