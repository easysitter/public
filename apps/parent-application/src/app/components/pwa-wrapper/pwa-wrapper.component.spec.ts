import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PwaWrapperComponent } from './pwa-wrapper.component';

describe('PwaWrapperComponent', () => {
  let component: PwaWrapperComponent;
  let fixture: ComponentFixture<PwaWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PwaWrapperComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PwaWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
