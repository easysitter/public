import { Component } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'pa-pwa-wrapper',
  animations: [
    trigger('installPanel', [
      state('false', style({
        transform: 'translateY(-100%)',
        opacity: 0,
        height: 0,
        padding: 0
      })),
      transition('* => false', [
        animate('.2s ease-out', style({
          transform: 'translateY(-100%)',
          opacity: 0,
          height: 0,
          padding: 0
        }))
      ]),
      transition(':leave', [
        animate('.2s ease-out', style({
          transform: 'translateY(-100%)',
          opacity: 0
        }))
      ]),
      transition('false => *', [
        animate('.2s ease-out')
      ])
    ])
  ],
  template: `
    <ngx-pwa-install #pwa>
      <div class="pwa-install-panel flex justify-start align-center" [@installPanel]="showInstallPanel"
           (click)="pwa.install()">

        <div class="app-body">
          <div class="app-body-inner flex" style="align-items: baseline">
            <i class="fa fa-download"></i>
            <span class="f-grow-1">Нажмите чтобы установить приложение</span>
            <i class="fa fa-close" (click)="close($event)"></i>
          </div>
        </div>
      </div>
    </ngx-pwa-install>
  `,
  styleUrls: ['./pwa-wrapper.component.scss']
})
export class PwaWrapperComponent {

  showInstallPanel = true;

  close(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.showInstallPanel = false;
  }
}
