import { ChangeDetectionStrategy, Component, forwardRef, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { addDays, getDaysInMonth, isSameDay } from 'date-fns';

@Component({
  selector: 'pa-line-date-piker',
  templateUrl: './line-date-piker.component.html',
  styleUrls: ['./line-date-piker.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => LineDatePikerComponent),
    multi: true
  }],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LineDatePikerComponent implements OnInit, ControlValueAccessor {
  val = null;

  days: Date[];

  constructor(
    @Inject(LOCALE_ID)
    private loc: string
  ) {
  }

  set value(val: any) {
    this.val = val;
    this.onChange(val);
    this.onTouched(val);
  }

  ngOnInit(): void {
    const today = new Date();
    const daysInMonth = getDaysInMonth(today);
    this.days = Array.from({ length: daysInMonth }, (_, i) => {
      return addDays(today, i);
    });
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: any): void {
    this.value = value;
  }

  isSelected(day: Date) {
    return isSameDay(day, this.val);
  }

  select(day: Date) {
    this.value = day;
  }

  private onChange = (value: any) => {
  };

  private onTouched = (value: any) => {
  };
}
