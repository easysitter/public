import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineDatePikerComponent } from './line-date-piker.component';

describe('LineDatePikerComponent', () => {
  let component: LineDatePikerComponent;
  let fixture: ComponentFixture<LineDatePikerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LineDatePikerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineDatePikerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
