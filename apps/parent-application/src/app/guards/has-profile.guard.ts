import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ProfileService } from '../services/profile/profile.service';
import { map, take } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HasProfileGuard implements CanActivate {
  constructor(
    private router: Router,
    private profile: ProfileService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> {
    return this.profile.load().pipe(
      take(1),
      map((profile) => {
        if (!profile) {
          return this.router.createUrlTree(['/profile'], {
            queryParams: {
              redirectTo: state.url
            }
          });
        }
        return true;
      })
    );
  }
}
