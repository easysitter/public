import { Apollo, gql } from 'apollo-angular';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, EMPTY, Subject } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import { AuthenticationService } from '@es/authentication';
import { ActivatedRoute, Router } from '@angular/router';

enum RegistrationErrors {
  EMAIL_EXISTS = 'User exists with same email',
  USERNAME_EXISTS = 'User exists with same username',
}

interface RegistrationSuccess {
  success: true;
}

interface RegistrationFailure {
  success: false;
  error: string;
}

type RegistrationResult = RegistrationSuccess | RegistrationFailure;

@Component({
  selector: 'pa-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css'],
})
export class RegistrationPageComponent implements OnInit {
  public readonly registrationErrors = RegistrationErrors;
  loading = new BehaviorSubject(false);
  submitRegister = new Subject<void>();
  form = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    password: ['', Validators.required],
  });
  tryRegister = this.submitRegister.pipe(
    map(() => this.form.value),
    exhaustMap((formValue) => {
      this.loading.next(true);
      return this.apollo
        .mutate<{ registerUser: RegistrationResult }>({
          mutation: gql`
            mutation register(
              $registerData: RegisterUserInput
              $redirect: String
            ) {
              registerUser(data: $registerData, redirect: $redirect) {
                success
                error
              }
            }
          `,
          variables: {
            registerData: {
              ...formValue,
            },
            redirect: location.origin + '/login',
          },
        })
        .pipe(
          catchError(() => {
            this.loading.next(false);
            return EMPTY;
          })
        );
    })
  );
  public registrationResult: RegistrationResult;
  passwordHidden = true;

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private apollo: Apollo
  ) {}

  ngOnInit(): void {
    this.tryRegister.subscribe((result) => {
      this.loading.next(false);
      this.registrationResult = result.data.registerUser;
    });
  }

  register() {
    if (this.form.invalid) return;
    this.submitRegister.next();
  }
}
