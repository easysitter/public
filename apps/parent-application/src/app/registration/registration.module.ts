import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PasswordStrengthMeterModule } from 'angular-password-strength-meter';


@NgModule({
  declarations: [
    RegistrationPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PasswordStrengthMeterModule,
    RouterModule.forChild([{
      path: '',
      component: RegistrationPageComponent
    }])
  ]
})
export class RegistrationModule {
}
