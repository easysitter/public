import { Apollo, gql } from 'apollo-angular';
import { Component, Optional, ViewChild } from '@angular/core';
import { SwPush, SwUpdate } from '@angular/service-worker';

import { filter, map, shareReplay, switchMap, take } from 'rxjs/operators';
import 'firebase/messaging';
import {
  ActivatedRoute,
  NavigationEnd,
  NavigationStart,
  Router,
} from '@angular/router';
import { LayoutHeader } from '@es/layout';
import { AuthenticationService } from '@es/authentication';
import { NativeUrlOpenService } from './services/native-url-open.service';
import { ProfileService } from './services/profile/profile.service';
import { DirectChat } from '@easysitter-chat/chat';
import {
  NotificationsProvider,
  NotificationsTapSubscriber,
} from '@es/notifications/core';

@Component({
  selector: 'pa-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  @ViewChild('headerComponent') layoutHeader: LayoutHeader;
  title = 'parent-application';
  profile = this.profileService.load();
  totalUnseen = this.directChat.totalUnseen.pipe(
    shareReplay({ bufferSize: 1, refCount: true })
  );

  isShowNav = this.router.events.pipe(
    filter((event) => event instanceof NavigationEnd),
    map((event: NavigationEnd) => event.urlAfterRedirects),
    map((url) => !['/login', '/registration'].includes(url))
  );

  constructor(
    private auth: AuthenticationService,
    private profileService: ProfileService,
    private directChat: DirectChat,
    private push: SwPush,
    private update: SwUpdate,
    private apollo: Apollo,
    private router: Router,
    private route: ActivatedRoute,
    private notificationsProvider: NotificationsProvider,
    private notificationTapSubscriber: NotificationsTapSubscriber,
    @Optional()
    nativeUrlOpenService: NativeUrlOpenService
  ) {
    if (update.isEnabled) {
      this.listenUpdates();
    }
    console.log(nativeUrlOpenService);
    if (nativeUrlOpenService) {
      nativeUrlOpenService.initializeOAuthNativeHandler();
    }
    this.router.events
      .pipe(filter((event) => event instanceof NavigationStart))
      .subscribe(() => this.layoutHeader.showBack(false));
    this.initializeNotifications();
  }

  async login() {
    // await this.auth.login();
    this.router.navigateByUrl('/login');
  }

  async logout() {
    await this.auth.logout();
    this.router.navigateByUrl('/login');
  }

  private listenUpdates() {
    this.update.available
      .pipe(switchMap(() => this.update.activateUpdate()))
      .subscribe(() => {
        console.log('reload for update');
        document.location.reload();
      });
  }

  private initializeNotifications() {
    this.auth
      .isLoggedIn()
      .pipe(
        filter((isLoggedIn) => !!isLoggedIn),
        take(1),
        switchMap(() => this.notificationsProvider.initialize()),
        switchMap((token) =>
          this.apollo.mutate({
            mutation: gql`
              mutation saveNotificationToken($token: String) {
                saveNotificationToken(token: $token)
              }
            `,
            variables: {
              token,
            },
          })
        )
      )
      .subscribe(() => {
        this.notificationTapSubscriber.subscribe(this.notificationsProvider);
      });
  }
}
