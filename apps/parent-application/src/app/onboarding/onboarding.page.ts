import { Component, ElementRef, OnInit } from '@angular/core';
import { LayoutService } from '@es/layout';
import { OnboardingService } from './onboarding.service';

@Component({
  selector: 'pa-onboarding-page',
  template: `
    <h1 class="page-title">Знакомство с системой</h1>
    <es-slider (navigated)="navigated($event)" class="flex flex-column f-grow-1">
      <div *esSlide class="slide flex flex-column justify-between align-center">
        <div class="f-grow-1 onboarding-image-container"
             [style.background-image]="'url(assets/onboarding/on-01.png)'"></div>
        <div class="text-center text-basic px-4 text-large color-dark">
          Укажите дату и время когда вам нужна няня, а также количество детей
        </div>
      </div>
      <div *esSlide class="slide flex flex-column justify-between align-center">
        <div class="f-grow-1 onboarding-image-container"
             [style.background-image]="'url(assets/onboarding/on-02.png)'"></div>
        <div class="text-center text-basic px-4 text-large color-dark">
          Получите список доступных нянь, подходящих под ваши требования
        </div>
      </div>
      <div *esSlide class="slide flex flex-column justify-between align-center">
        <div class="f-grow-1 onboarding-image-container"
             [style.background-image]="'url(assets/onboarding/on-03.png)'"></div>
        <div class="text-center text-basic px-4 text-large color-dark">
          Выберите одну или несколько понравившихся нянь и назначьте им видео собеседования
        </div>
      </div>
      <div *esSlide class="slide flex flex-column justify-between align-center">
        <div class="f-grow-1 onboarding-image-container"
             [style.background-image]="'url(assets/onboarding/on-04.png)'"></div>
        <div class="text-center text-basic px-4 text-large color-dark">
          Подтвердите визит
        </div>
      </div>
      <div *esSlide class="slide flex flex-column justify-between align-center">
        <div class="f-grow-1 onboarding-image-container"
             [style.background-image]="'url(assets/onboarding/on-05.png)'"></div>
        <div class="text-center text-basic px-4 text-large color-dark">
          По окончанию визита оплатите услуги няни напрямую из приложения
        </div>
      </div>
    </es-slider>
    <div class="p-4 text-center f-basis-mh-20 f-shrink-0">
      <button *ngIf="!tutorialFinished" class="btn btn-mini btn-default btn-link" routerLink="/" (click)="skip()">
        Пропустить
      </button>
      <button *ngIf="tutorialFinished" class="btn btn-mini btn-secondary btn-block" routerLink="/">Начать
      </button>
    </div>

  `,
  styles: [
      `
      .onboarding-image-container {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: flex-end;
        margin-bottom: 3rem;
        position: relative;

        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
      }

      .onboarding-image-container:after {
        content: '';
        position: absolute;
        bottom: -1rem;
        height: 25%;
        width: 100%;
        background: linear-gradient(180deg, rgba(246, 246, 246, 0) 0%, #F6F6F6 90%);
      }
    `
  ]
})
export class OnboardingPage implements OnInit {
  tutorialFinished = false;

  constructor(
    private readonly elRef: ElementRef,
    private readonly layout: LayoutService,
    private readonly onboarding: OnboardingService
  ) {
  }

  ngOnInit() {
    this.layout.makeScreen(this.elRef);
  }


  navigated($event: { idx: number; last: boolean }) {
    if ($event.last) {
      this.tutorialFinished = true;
      this.onboarding.complete();
    }
  }

  skip() {
    this.onboarding.complete();
  }
}
