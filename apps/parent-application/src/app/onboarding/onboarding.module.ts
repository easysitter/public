import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingPage } from './onboarding.page';
import { SliderModule } from '@es/slider';


@NgModule({
  declarations: [OnboardingPage],
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    SliderModule
  ]
})
export class OnboardingModule {
}
