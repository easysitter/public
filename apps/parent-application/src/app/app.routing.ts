import { RouterModule } from '@angular/router';
import { RequestsPageComponent } from './pages/requests-page/requests-page.component';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { SearchResultPageComponent } from './pages/search-result-page/search-result-page.component';
import { CreateRequestPageComponent } from './pages/create-request-page/create-request-page.component';
import { AUTH_GUARD } from './injection-tokens';
import { ChatPageComponent } from './pages/chat-page/chat-page.component';
import { DealsPageComponent } from './pages/deals-page/deals-page.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { HasProfileGuard } from './guards/has-profile.guard';
import { NannyProfilePageComponent } from './pages/nanny-profile-page/nanny-profile-page.component';
import { DealDetailsPageComponent } from './pages/deal-details-page/deal-details-page.component';
import { OnboardingGuard } from './onboarding/onboarding.guard';

export const AppRouting = RouterModule.forRoot(
  [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: 'search',
    },
    {
      path: 'onboarding',
      loadChildren: async () =>
        import('./onboarding/onboarding.module').then(
          (m) => m.OnboardingModule
        ),
    },
    {
      path: 'only-video-chat',
      loadChildren: () =>
        import('./videochat-page/videochat-native-page.module').then(
          (m) => m.VideochatNativePageModule
        ),
    },
    {
      path: '',
      canActivate: [OnboardingGuard],
      canActivateChild: [OnboardingGuard],
      canLoad: [OnboardingGuard],
      children: [
        {
          path: 'login',
          loadChildren: () =>
            import('./login/login.module').then((m) => m.LoginModule),
        },
        {
          path: 'registration',
          loadChildren: () =>
            import('./registration/registration.module').then(
              (m) => m.RegistrationModule
            ),
        },
        {
          path: 'search',
          children: [
            {
              path: '',
              pathMatch: 'full',
              component: SearchPageComponent,
            },
            {
              path: ':searchId',
              component: SearchResultPageComponent,
            },
          ],
        },
        {
          path: '',
          canActivate: [AUTH_GUARD],
          data: {
            authRedirect: '/login',
          },
          children: [
            {
              path: 'profile',
              component: ProfilePageComponent,
            },
            {
              path: '',
              canActivate: [HasProfileGuard],
              children: [
                {
                  path: 'requests',
                  component: RequestsPageComponent,
                },
                {
                  path: 'deals',
                  component: DealsPageComponent,
                },
                {
                  path: 'nanny/:nannyId/chat',
                  loadChildren: () =>
                    import('./direct-chat/direct-chat.module').then(
                      (m) => m.DirectChatModule
                    ),
                },
                {
                  path: 'chat',
                  children: [
                    {
                      path: ':requestId/:nannyId',
                      component: ChatPageComponent,
                    },
                    {
                      path: '',
                      loadChildren: () =>
                        import('./direct-chat/direct-chat-list.module').then(
                          (m) => m.DirectChatListModule
                        ),
                    },
                  ],
                },
                {
                  path: 'search/:searchId/create-request',
                  component: CreateRequestPageComponent,
                },
                {
                  path: 'nanny/:nannyId',
                  component: NannyProfilePageComponent,
                },
                {
                  path: 'deals/details/:dealId',
                  component: DealDetailsPageComponent,
                },
                {
                  path: 'nanny/:nannyId/videochat',
                  loadChildren: () =>
                    import('./videochat-page/videochat-page.module').then(
                      (m) => m.VideochatPageModule
                    ),
                },
              ],
            },
          ],
        },
      ],
    },
  ],
  {
    initialNavigation: 'enabled',
    relativeLinkResolution: 'legacy'
}
);
