import { Component, Inject, OnInit } from '@angular/core';
import { filter, map, pluck, shareReplay, switchMap } from 'rxjs/operators';
import { defer, timer } from 'rxjs';
import { formatDuration, intervalToDuration, parse } from 'date-fns';
import { ActivatedRoute } from '@angular/router';
import { DealService } from '../../services/deal.service';
import { ReviewFormResult } from '@es/rating';
import { HEADER, LayoutHeader } from '@es/layout';
import ru from 'date-fns/locale/ru';
import { Calculator, CalculatorTracer } from '@es/calculator';

@Component({
  selector: 'pa-deal-details-page',
  templateUrl: './deal-details-page.component.html',
  styleUrls: ['./deal-details-page.component.css'],
})
export class DealDetailsPageComponent implements OnInit {
  deal$ = this.route.params.pipe(
    pluck('dealId'),
    switchMap((id) => this.dealService.load(id)),
    shareReplay({ bufferSize: 1, refCount: true })
  );

  durationAndCost$ = this.deal$.pipe(
    filter((deal) => !!deal.startedAt),
    switchMap((deal) =>
      timer(0, 10000).pipe(
        switchMap(() => {
          const startedAt = new Date(deal.startedAt);
          const date = new Date(deal.date);
          const from = parse(`${deal.fromTime}+02`, 'HH:mmX', date);
          const to = parse(`${deal.toTime}+02`, 'HH:mmX', date);
          const finishAt = deal.finishedAt
            ? new Date(deal.finishedAt)
            : new Date();
          const interval = {
            start: startedAt,
            end: finishAt,
          };
          const duration = formatDuration(intervalToDuration(interval), {
            format: ['years', 'months', 'days', 'hours', 'minutes'],
            // @ts-ignore
            locale: ru,
          });
          return defer(() =>
            this.calculator.getPrice(
              {
                from,
                to,
                startedAt,
                finishedAt: finishAt,
                nanny: deal.nanny,
                children: deal.children,
              },
              this.calculatorTracer
            )
          ).pipe(map((cost) => ({ duration, cost })));
        })
      )
    )
  );

  constructor(
    private route: ActivatedRoute,
    @Inject(HEADER)
    private layoutHeader: LayoutHeader,
    private dealService: DealService,
    private calculator: Calculator,
    private calculatorTracer: CalculatorTracer
  ) {}

  ngOnInit() {
    this.layoutHeader.showBack(true);
  }

  submitReview(review: ReviewFormResult, deal: any) {
    this.dealService.submitReview(deal.id, review).subscribe();
  }

  cancel(deal: any) {
    this.dealService.cancel(deal).subscribe();
  }
}
