import {Apollo} from 'apollo-angular';
import { Component, ElementRef, HostBinding, HostListener, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ExtendedNannyRequest, ExtendedParentRequest, RequestsService } from '../../services/requests/requests.service';

import { Router } from '@angular/router';
import { LayoutService } from '@es/layout';
import { MatDialog } from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import { NannyRequest } from '@es/requests';
import { CreateDealSummaryDialog } from '../../components/create-deal-summary/create-deal-summary.dialog';

@Component({
  selector: 'pa-requests-page',
  templateUrl: './requests-page.component.html',
  styleUrls: ['./requests-page.component.css']
})
export class RequestsPageComponent implements OnInit {

  @ViewChild('cancelRequestConfirm') cancelRequestConfirmTpl: TemplateRef<any>;

  private readonly processed: any[] = [];
  requests = this.requestsService.fetch().pipe(
    map((list) => list.filter(item => !this.processed.includes(item.id)))
  );
  private toggled = new Set();
  @HostBinding('class.loading')
  processing = false;

  constructor(
    private layout: LayoutService,
    private elRef: ElementRef,
    private apollo: Apollo,
    private requestsService: RequestsService,
    private dialog: MatDialog,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.layout.makeScreen(this.elRef);
  }

  createDeal(request: NannyRequest) {
    this.dialog.open(CreateDealSummaryDialog, {
      data: request,
      closeOnNavigation: true,
      disableClose: true
    }).afterClosed().subscribe(
      (result) => {
        this.router.navigate(['/deals/details', result.id]);
      }
    );
  }

  toggle(request) {
    if (this.isToggled(request)) {
      this.toggled.delete(request.id);
    } else {
      this.toggled.add(request.id);
    }
  }

  isToggled(request) {
    return this.toggled.has(request.id);
  }

  cancel(request: ExtendedParentRequest) {
    this.requestsService.cancel(request.id).subscribe();
  }

  openCancelRequestDialog(request: ExtendedNannyRequest) {
    this.dialog.open(this.cancelRequestConfirmTpl, {
      data: {
        request
      }
    });
  }

  @HostListener('swipeleft')
  onSwipeLeft() {
    this.router.navigateByUrl('/deals');
  }
}
