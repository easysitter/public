import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { HEADER, Layout, LAYOUT, LayoutHeader } from '@es/layout';
import { ActivatedRoute, Router } from '@angular/router';
import {
  pluck,
  shareReplay,
  switchMap,
  switchMapTo,
  takeUntil,
} from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { extractFromRoute } from '../../utils/operators';
import {
  SearchResult,
  SearchService,
} from '../../services/search/search.service';

@Component({
  selector: 'pa-search-result-page',
  templateUrl: './search-result-page.component.html',
})
export class SearchResultPageComponent implements OnInit, OnDestroy {
  private destroy = new Subject<void>();
  public readonly searchResult: Observable<SearchResult> = extractFromRoute(
    this.route,
    'searchId'
  ).pipe(
    switchMap((id) => this.searchService.getResult(+id)),
    shareReplay({ refCount: true, bufferSize: 1 }),
    takeUntil(this.destroy)
  );
  public nannies = this.searchResult.pipe(pluck('nannies'));
  private submitSubject = new Subject<void>();
  private submit = this.submitSubject.pipe(
    switchMapTo(this.searchResult),
    takeUntil(this.destroy)
  );

  constructor(
    @Inject(LAYOUT)
    private layout: Layout,
    private elRef: ElementRef,
    private route: ActivatedRoute,
    private router: Router,
    private searchService: SearchService,
    @Inject(HEADER)
    private header: LayoutHeader
  ) {}

  ngOnInit() {
    this.header.showBack(true);
    this.layout.addPageClasses(this.elRef);
    this.submit.subscribe((result) => {
      this.router.navigate(['search', result.id, 'create-request']);
    });
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  trackById(idx, nanny) {
    return nanny.id;
  }

  proceed() {
    this.submitSubject.next();
  }
}
