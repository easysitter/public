import { Component, Inject, OnInit } from '@angular/core';
import { NannyProfileService } from '../../services/nanny-profile/nanny-profile.service';
import { ActivatedRoute } from '@angular/router';
import { pluck, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HEADER, LayoutHeader } from '@es/layout';

@Component({
  selector: 'pa-nanny-profile-page',
  templateUrl: './nanny-profile-page.component.html',
})
export class NannyProfilePageComponent implements OnInit {
  nanny: Observable<any> = this.route.params.pipe(
    pluck('nannyId'),
    switchMap((id) => this.nannyProfileService.load(id))
  );

  constructor(
    private route: ActivatedRoute,
    private nannyProfileService: NannyProfileService,
    @Inject(HEADER)
    private header: LayoutHeader
  ) {}

  ngOnInit() {
    this.header.showBack(true);
  }
}
