import { Component, ElementRef, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { HEADER, LayoutHeader, LayoutService } from '@es/layout';
import {
  ExtendedNannyRequest,
  RequestsService,
} from '../../services/requests/requests.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'pa-chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.css'],
})
export class ChatPageComponent implements OnInit {
  requestId = this.activatedRoute.paramMap.pipe(
    map((params) => params.get('requestId'))
  );
  nannyId = this.activatedRoute.paramMap.pipe(
    map((params) => params.get('nannyId'))
  );
  request$ = combineLatest(this.requestId, this.nannyId).pipe(
    switchMap(([requestId, nannyId]) =>
      this.requestsService.getDetails(requestId, nannyId)
    )
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private layout: LayoutService,
    @Inject(HEADER)
    private layoutHeader: LayoutHeader,
    private elRef: ElementRef,
    private requestsService: RequestsService
  ) {}

  ngOnInit() {
    this.layout.makeScreen(this.elRef);
    this.layoutHeader.showBack(true);
  }

  createDeal(request: ExtendedNannyRequest) {
    this.requestsService
      .createDeal({
        request: request.request.id,
        nanny:
          typeof request.nanny === 'object' ? request.nanny.id : request.nanny,
      })
      .subscribe();
  }
}
