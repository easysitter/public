import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { HEADER, Layout, LAYOUT, LayoutHeader } from '@es/layout';
import { ActivatedRoute, Router } from '@angular/router';
import {
  combineLatest,
  defer,
  EMPTY,
  merge,
  Observable,
  of,
  Subject,
} from 'rxjs';
import { extractFromRoute } from '../../utils/operators';
import {
  exhaustMap,
  first,
  map,
  shareReplay,
  switchMap,
  switchMapTo,
  take,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { NanniesSelector } from '../../utils/nannies-selector';
import {
  FormBuilder,
  FormControlName,
  FormGroup,
  Validators,
} from '@angular/forms';
import { RequestsService } from '../../services/requests/requests.service';
import {
  SearchResult,
  SearchResultNanny,
  SearchService,
} from '../../services/search/search.service';

@Component({
  selector: 'pa-create-request-page',
  templateUrl: './create-request-page.component.html',
})
export class CreateRequestPageComponent implements OnInit, OnDestroy {
  private destroy = new Subject<void>();
  public searchResult = this.getSearchResult();
  public nannies = this.getFoundNannies(this.searchResult);
  public selector = new NanniesSelector(this.nannies, true);
  public form = this.setupForm();
  public invalid = this.createInvalidStateStream();
  public inProgress = false;
  public nanniesSelectionCloseDisabled = this.selector
    .getSelectedCount()
    .pipe(map((count) => count < 1));
  private formDataStream = combineLatest([
    this.selector.getSelected(),
    this.searchResult,
    defer(() => of(this.form.value)),
  ]).pipe(
    map(([selected, searchResult, formValue]) => ({
      ...formValue,
      nannies: selected.map((nanny) => nanny.id),
      search: searchResult.id.toString(),
    }))
  );
  private submit = new Subject<void>();
  @ViewChildren(FormControlName, { read: ElementRef })
  private invalidFormElements: QueryList<ElementRef<HTMLElement>>;

  private _showErrors = false;
  get showErrors() {
    return this._showErrors && this.form.dirty;
  }

  constructor(
    @Inject(LAYOUT)
    private layout: Layout,
    @Inject(HEADER)
    private layoutHeader: LayoutHeader,
    private elRef: ElementRef,
    private searchService: SearchService,
    private requestsService: RequestsService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.layout.addPageClasses(this.elRef);
    this.setupSubmitSubscription();
    this.layoutHeader.showBack(true);
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  public createInvalidStateStream() {
    return combineLatest([
      merge(of(this.form.value), this.form.valueChanges).pipe(
        map(() => this.form.invalid)
      ),
      this.selector.getSelectedCount().pipe(map((count) => count < 1)),
    ]).pipe(
      map(
        ([formInvalid, selectedNanniesInvalid]) =>
          formInvalid || selectedNanniesInvalid
      ),
      shareReplay({ bufferSize: 1, refCount: true })
    );
  }

  createRequest() {
    this.submit.next();
  }

  private setupForm() {
    const form = this.formBuilder.group({
      address: ['', Validators.required],
      phone: ['', Validators.required],
      wishes: [''],
      zipCode: [''],
    });
    this.prefillForm(form);
    return form;
  }

  private prefillForm(form: FormGroup) {
    this.searchService
      .getPrefilledData()
      .pipe(first(), takeUntil(form.valueChanges), takeUntil(this.destroy))
      .subscribe((data) => {
        form.patchValue(data);
      });
  }

  private getSearchResult() {
    return extractFromRoute(this.route, 'searchId').pipe(
      switchMap((id) => this.searchService.getResult(id)),
      takeUntil(this.destroy)
    );
  }

  private getFoundNannies(
    searchResult$: Observable<SearchResult>
  ): Observable<SearchResultNanny[]> {
    return searchResult$.pipe(
      map((searchResult) =>
        searchResult.nannies.filter((nanny) => nanny.isApproved)
      ),
      shareReplay({ bufferSize: 1, refCount: true })
    );
  }

  private setupSubmitSubscription() {
    this.submit
      .pipe(
        switchMapTo(this.invalid.pipe(take(1))),
        switchMap((invalid) => {
          if (!invalid) {
            return this.formDataStream;
          }
          this.displayErrors();
          return EMPTY;
        }),
        tap(() => (this.inProgress = true)),
        exhaustMap((data) => this.requestsService.create(data))
      )
      .subscribe(() => {
        this.inProgress = false;
        this.router.navigate(['/requests']);
      });
  }

  private displayErrors() {
    this._showErrors = true;
    this.form.markAsDirty();
    const formControls = this.invalidFormElements.toArray();
    const firstInvalid = formControls.find((formControl) =>
      formControl.nativeElement.classList.contains('ng-invalid')
    );
    if (firstInvalid) {
      firstInvalid.nativeElement.parentElement.parentElement.scrollIntoView({
        behavior: 'smooth',
      });
    }
  }
}
