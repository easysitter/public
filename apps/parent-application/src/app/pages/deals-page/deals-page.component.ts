import { Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { LayoutService } from '@es/layout';
import { DealService } from '../../services/deal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'pa-deals-page',
  templateUrl: './deals-page.component.html',
  styleUrls: ['./deals-page.component.css']
})
export class DealsPageComponent implements OnInit {

  deals$ = this.dealService.fetch();

  constructor(
    private layout: LayoutService,
    private elRef: ElementRef,
    private dealService: DealService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.layout.makeScreen(this.elRef);
  }

  @HostListener('swiperight')
  onSwipeRight() {
    this.router.navigateByUrl('/requests');
  }

  cancel(deal: any) {
    this.dealService.cancel(deal)
      .subscribe();
  }
}
