import { gql, Apollo } from 'apollo-angular';
import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { defer, of, Subject, Subscription } from 'rxjs';
import { exhaustMap, map, pluck, switchMap, takeUntil } from 'rxjs/operators';
import { ProfileService } from '../../services/profile/profile.service';
import { ActivatedRoute, Router } from '@angular/router';

import { HEADER, LayoutHeader } from '@es/layout';
import { OIDCService } from '@es/authentication';

@Component({
  selector: 'pa-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent implements OnInit, OnDestroy {
  form = this.formBuilder.group({
    name: '',
    birthDate: '',
    phone: '',
    address: '',
    zipCode: '',
    email: '',
    photo: '',
    children: this.formBuilder.control([]),
    description: '',
  });
  private matDialogRef: MatDialogRef<any, any>;
  private destroy = new Subject<void>();
  private submit = new Subject<void>();
  photoUploading = false;
  private photoUploadSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private oidc: OIDCService,
    private profileService: ProfileService,
    private route: ActivatedRoute,
    private router: Router,
    private apollo: Apollo,
    @Inject(HEADER)
    private header: LayoutHeader
  ) {}

  get children(): FormControl {
    return this.form.get('children') as FormControl;
  }

  ngOnInit() {
    this.header.showBack(true);
    this.profileService
      .load()
      .pipe(
        switchMap((profile) =>
          profile ? of(profile) : defer(() => this.oidc.getIdentity())
        ),
        takeUntil(this.destroy)
      )
      .subscribe((data) => {
        this.form.patchValue(data);
      });

    this.submit
      .pipe(
        exhaustMap((data) => this.profileService.save(data as any)),
        switchMap(() => this.route.queryParams),
        map((params) => ({ redirectTo: params['redirectTo'] })),
        takeUntil(this.destroy)
      )
      .subscribe(({ redirectTo }) => {
        console.log(redirectTo);
        return this.router.navigateByUrl(redirectTo || '/requests');
      });
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
    this.destroy = null;
  }

  handleSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.submit.next(this.form.value);
  }

  openChildSelector(dialogTpl: TemplateRef<any>) {
    this.openDialog(dialogTpl);
  }

  selectChild(child: number) {
    this.addChild(child);
    this.matDialogRef.close();
  }

  removeChild(idx: number) {
    const control = this.children;
    const newValue = control.value.filter((_, i) => i !== idx);
    control.setValue(newValue);
  }

  updatePhoto($event: Event) {
    const { target } = $event as unknown as { target: HTMLInputElement };
    if (!target.files.length) return;
    const file = target.files[0];
    target.value = null;
    if (this.photoUploadSubscription) {
      this.photoUploadSubscription.unsubscribe();
    }
    this.photoUploading = true;
    this.photoUploadSubscription = this.upload(file, 'photo').subscribe(
      (src) => {
        this.form.patchValue({
          photo: src,
        });
        this.photoUploading = false;
      }
    );
  }

  private openDialog(dialogTpl: TemplateRef<any>) {
    this.matDialogRef = this.dialog.open(dialogTpl, {
      maxWidth: '100vw',
      panelClass: ['modal-container'],
      hasBackdrop: true,
      disableClose: true,
    });
  }

  private addChild(child) {
    this.children.setValue([...this.children.value, child]);
  }

  private upload(file: File, resourceType: string) {
    return this.apollo
      .mutate({
        mutation: gql`
          mutation upload($file: Upload!, $resourceType: String) {
            uploadFile(file: $file, resourceType: $resourceType) {
              public_url
            }
          }
        `,
        variables: { file, resourceType },
        context: {
          useMultipart: true,
        },
      })
      .pipe(pluck('data', 'uploadFile', 'public_url'));
  }
}
