import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Layout, LAYOUT } from '@es/layout';
import { FormBuilder, FormControl } from '@angular/forms';
import {
  addHours,
  addMinutes,
  format,
  roundToNearestMinutes,
  startOfMinute,
} from 'date-fns';
import { EMPTY, Subject } from 'rxjs';
import {
  catchError,
  exhaustMap,
  filter,
  first,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  emptyArrayValidator,
  timeRangeValidator,
} from '../../utils/validators';
import {
  SearchParams,
  SearchService,
} from '../../services/search/search.service';
import { ProfileService } from '../../services/profile/profile.service';
import { AuthenticationService } from '@es/authentication';

@Component({
  selector: 'pa-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent implements OnInit, OnDestroy {
  private destroy = new Subject<void>();
  public searching = false;
  public readonly minutesGap = 30;
  form = this.formBuilder.group(
    {
      date: new Date(),
      fromTime: '',
      toTime: '',
      children: this.formBuilder.control([], [emptyArrayValidator]),
      trial: this.formBuilder.control(false),
    },
    {
      validators: [timeRangeValidator],
    }
  );
  private searchSubject = new Subject();
  private matDialogRef: MatDialogRef<any, any>;
  public minDate: Date;

  constructor(
    private auth: AuthenticationService,
    private profile: ProfileService,
    private searchService: SearchService,
    private formBuilder: FormBuilder,
    private router: Router,
    private elRef: ElementRef,
    private dialog: MatDialog,
    @Inject(LAYOUT)
    private layout: Layout
  ) {}

  get children(): FormControl {
    return this.form.get('children') as FormControl;
  }

  ngOnInit() {
    this.setMinDate();
    this.initializePage();
    this.setupFormValues();
    this.setupSubmissions();
  }

  ngOnDestroy(): void {
    this.layout.darkThemeOff();
    this.destroy.next();
    this.destroy.complete();
    this.destroy = null;
  }

  openChildSelector(dialogTpl: TemplateRef<any>) {
    this.openDialog(dialogTpl);
  }

  selectChild(child: number) {
    this.addChild(child);
    this.matDialogRef.close();
  }

  removeChild(idx: number) {
    const control = this.children;
    const newValue = control.value.filter((_, i) => i !== idx);
    control.setValue(newValue);
  }

  runSearch() {
    if (this.form.invalid) return;
    const { fromTime, toTime, date, children, trial } = this.form.value;
    this.searchSubject.next({
      fromTime,
      toTime,
      date: format(date, 'yyyy-MM-dd'),
      children,
      trial,
    });
  }

  login() {
    this.auth.login();
  }

  private setupFormValues() {
    const fromTime = this.minDate;
    const toTime = addHours(fromTime, 2);
    this.form.setValue({
      ...this.form.value,
      date: fromTime,
      fromTime: this.formatTime(fromTime),
      toTime: this.formatTime(toTime),
    });
    this.auth
      .isLoggedIn()
      .pipe(
        filter((isLoggedIn) => isLoggedIn),
        first(),
        switchMap(() => this.profile.load()),
        filter((profile) => !!profile),
        takeUntil(this.destroy)
      )
      .subscribe((profile) => {
        this.form.patchValue({
          children: profile.children,
        });
      });
  }

  private setMinDate() {
    const now = startOfMinute(new Date());
    let minDate = roundToNearestMinutes(now, { nearestTo: this.minutesGap });
    if (minDate < now) {
      minDate = roundToNearestMinutes(addMinutes(now, this.minutesGap / 2), {
        nearestTo: this.minutesGap,
      });
    }
    this.minDate = addHours(minDate, 3);
  }

  private initializePage() {
    this.layout.darkThemeOn();
    this.layout.addPageClasses(this.elRef);
  }

  private openDialog(dialogTpl: TemplateRef<any>) {
    this.matDialogRef = this.dialog.open(dialogTpl, {
      maxWidth: '100vw',
      panelClass: ['modal-container'],
      hasBackdrop: true,
      disableClose: false,
    });
  }

  private setupSubmissions() {
    this.searchSubject
      .pipe(
        tap(() => (this.searching = true)),
        exhaustMap((formData: SearchParams) =>
          this.searchService.create(formData).pipe(
            catchError(() => {
              this.searching = false;
              return EMPTY;
            })
          )
        ),
        takeUntil(this.destroy)
      )
      .subscribe({
        next: (result) => {
          this.searching = false;
          this.router.navigate(['search', result.id]);
        },
      });
  }

  private formatTime(fromTime: Date) {
    return format(
      roundToNearestMinutes(fromTime, { nearestTo: this.minutesGap }),
      'HH:mm'
    );
  }

  private addChild(child) {
    this.children.setValue([...this.children.value, child]);
  }
}
