import { AuthConfig } from 'angular-oauth2-oidc';

export function authCodeFlowConfigFactory(): AuthConfig {
  return {
    // Url of the Identity Provider
    issuer: 'https://auth.easysitter.pl/auth/realms/parents',

    // URL of the SPA to redirect the user to after login
    redirectUri: `${window.location.origin}`,

    // URL of the SPA to redirect the user after silent refresh
    silentRefreshRedirectUri: `${window.location.origin}/silent-refresh.html`,

    // defaults to true for implicit flow and false for code flow
    // as for code code the default is using a refresh_token
    // Also see docs section 'Token Refresh'
    useSilentRefresh: false,

    oidc: false,

    // The SPA's id. The SPA is registerd with this id at the auth-server
    // clientId: 'server.code',
    clientId: 'parent-application',

    // Just needed if your auth server demands a secret. In general, this
    // is a sign that the auth server is not configured with SPAs in mind
    // and it might not enforce further best practices vital for security
    // such applications.
    // dummyClientSecret: 'secret',

    responseType: 'code',

    // set the scope for the permissions the client should request
    // The first four are defined by OIDC.
    // Important: Request offline_access to get a refresh token
    // The api scope is a usecase specific one
    scope: 'openid profile email offline_access',

    showDebugInformation: true
  };
}
