import { Component, OnDestroy } from '@angular/core';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DirectChat } from '@easysitter-chat/chat';

@Component({
  selector: 'pa-direct-chat-list',
  templateUrl: './direct-chat-list.page.html',
  styleUrls: ['./direct-chat-list.page.css'],
})
export class DirectChatListPage implements OnDestroy {
  destroy = new Subject<void>();
  chatList = this.directChat.list.pipe(
    map((chatList) => {
      return chatList.map(({ users, ...chat }) => {
        return {
          ...chat,
          user: users.find((user) => user.type === 'nanny'),
        };
      });
    }),
    takeUntil(this.destroy)
  );

  constructor(private directChat: DirectChat) {}

  ngOnDestroy() {
    this.destroy.next();
    this.destroy.complete();
    this.destroy = null;
  }
}
