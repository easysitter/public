import {
  Directive,
  EmbeddedViewRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { NannyProfile, NannyProfileService } from '../services/nanny-profile/nanny-profile.service';
import { Subscription } from 'rxjs';

interface NannyProfileContext {
  $implicit: NannyProfile
}

@Directive({
  selector: '[paNannyProfile]'
})
export class NannyProfileDirective implements OnChanges, OnDestroy {

  @Input('paNannyProfileId')
  id: string;
  private context: NannyProfileContext;
  private subscription: Subscription;
  private viewRef: EmbeddedViewRef<NannyProfileContext>;

  constructor(
    private nannyProfile: NannyProfileService,
    private templateRef: TemplateRef<NannyProfileContext>,
    private vcr: ViewContainerRef
  ) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('id' in changes) {
      this.init();
    }
  }

  ngOnDestroy() {
    this.destroy();
  }

  private init() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = this.nannyProfile.load(this.id).subscribe(
      profile => {
        if (!this.viewRef) {
          this.context = { $implicit: profile };
          this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
        } else {
          this.context.$implicit = profile;
        }
      }
    );
  }

  private destroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.viewRef) {
      this.viewRef.destroy();
    }
  }
}
