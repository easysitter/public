import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChatModule } from '@easysitter-chat/chat';
import { DirectChatPageComponent } from './direct-chat-page/direct-chat-page.component';
import { NannyProfileDirective } from './nanny-profile.directive';


@NgModule({
  declarations: [
    DirectChatPageComponent,
    NannyProfileDirective
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: DirectChatPageComponent
    }]),
    ChatModule
  ]
})
export class DirectChatModule {
}
