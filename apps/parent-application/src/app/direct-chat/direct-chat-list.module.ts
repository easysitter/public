import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DirectChatListPage } from "./direct-chat-list/direct-chat-list.page";
import { RouterModule } from "@angular/router";
import { ProfileUiModule } from "@es/profile";


@NgModule({
  declarations: [DirectChatListPage],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: "",
        pathMatch: "full",
        component: DirectChatListPage
      },
      {
        path: "nanny/direct/:nannyId",
        redirectTo: "/nanny/:nannyId/chat"
      }
    ]),
    ProfileUiModule
  ]
})
export class DirectChatListModule {
}
