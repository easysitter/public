import { Component, ElementRef, Inject, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { HEADER, LayoutHeader, LayoutService } from '@es/layout';

@Component({
  selector: 'pa-direct-chat-page',
  templateUrl: './direct-chat-page.component.html',
  styleUrls: ['./direct-chat-page.component.css'],
})
export class DirectChatPageComponent implements OnInit {
  nannyId = this.activatedRoute.paramMap.pipe(
    map((params) => params.get('nannyId'))
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private layout: LayoutService,
    @Inject(HEADER)
    private layoutHeader: LayoutHeader,
    private elRef: ElementRef
  ) {}

  ngOnInit() {
    this.layout.makeScreen(this.elRef);
    this.layoutHeader.showBack(true);
  }
}
