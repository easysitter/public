import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import 'hammerjs';
import { hmrBootstrap } from './hmr';
import { DEFAULT_CONFIG } from './app/services/runtime-config';

if (environment.production) {
  enableProdMode();
}

const bootstrap = () =>
  fetch(environment.configUrl)
    .then((r) => r.json())
    .then((config) =>
      platformBrowserDynamic([
        {
          provide: DEFAULT_CONFIG,
          useValue: config,
        },
      ]).bootstrapModule(AppModule)
    );

if (environment.hmr) {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    console.error('HMR is not enabled for webpack-dev-server!');
    console.log('Are you using the --hmr flag for ng serve?');
  }
} else {
  bootstrap().catch((err) => console.error(err));
}
