try {
  importScripts('./ngsw-worker.js');
} catch (e) {
  // console.error('Failed to load ngsw');
}
self.addEventListener('notificationclick', (event) => {
  /**
   * @var notification Notification
   */
  const notification = event.notification;
  notification.close();
  console.log('This is custom service worker notificationclick method.');
  console.log('Notification details: ', notification);
  // Write the code to open
  if (clients.openWindow) {
    let url;
    try {
      switch (notification.data.type) {
        case 'chat-message':
          const {
            chat_type,
            message_user,
            request,
            chat_users,
          } = notification.data;
          if (chat_type === 'direct') {
            const user = chat_users.split(':').find((id) => id.startsWith('n'));
            url = '/nanny/' + user + '/chat';
            break;
          }
          url = '/chat/' + request + '/' + message_user.slice(1);
          break;
        case 'request-accepted':
          url = '/requests/';
          break;
        case 'new-offer':
          url = '/requests/';
          break;
        case 'new-deal':
          url = '/deals/details/' + notification.data.deal;
          break;
        case 'new-payment':
          url = notification.data.paymentUrl
            ? notification.data.paymentUrl
            : '/deals/details/' + notification.data.deal;
          break;
        default:
          url = notification.data.url || '/requests';
      }
    } catch (e) {}
    event.waitUntil(clients.openWindow(url));
  }
});
