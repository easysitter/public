export const environment = {
  production: true,
  native: true,
  dataSource: {
    host: 'nanny.easysitter.pl',
    secured: true
  },
  firebase: {
    apiKey: 'AIzaSyAOpLiFqQ86-4cuRDVn7LwNmKHUGYftesw',
    authDomain: 'easysitter.firebaseapp.com',
    databaseURL: 'https://easysitter.firebaseio.com',
    projectId: 'easysitter',
    storageBucket: 'easysitter.appspot.com',
    messagingSenderId: '599968041570',
    appId: '1:599968041570:web:07b74fdc3a55d636319f3b',
    measurementId: 'G-1NFCM12PZS'
  },
  opentokApiKey: '46883724',
};
