import { Directive, EmbeddedViewRef, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from "@angular/core";
import { map } from "rxjs/operators";
import { combineLatest, Observable, Subscription } from "rxjs";
import { RequestService } from "../services/requests/requests.service";
import { DealService } from "../services/deals/deal.service";
import { count } from "../utils/operators";
import { DirectChat } from "@easysitter-chat/chat";

interface CurrentCounters {
  requests: number
  conversations: number
  visits: number
  newMessages: number
}

interface CurrentCountersContext {
  $implicit: CurrentCounters
}

@Directive({
  selector: "[naCurrentCounters]"
})
export class CurrentCountersDirective implements OnInit, OnDestroy {

  public readonly requestsCount$ = this.requestService.getRequests().pipe(
    count()
  );
  public readonly conversationsCount$ = this.requestService.getConversations().pipe(
    count()
  );
  public readonly visitsCount$ = this.visitsService.getActive().pipe(
    count()
  );
  counters$: Observable<CurrentCounters> = combineLatest(
    this.requestsCount$,
    this.conversationsCount$,
    this.visitsCount$,
    this.directChat.totalUnseen
  ).pipe(
    map(([requests, conversations, visits, newMessages]) => ({ requests, conversations, visits, newMessages }))
  );
  private context: CurrentCountersContext = {
    $implicit: {
      conversations: 0,
      requests: 0,
      visits: 0,
      newMessages: 0
    }
  };
  private viewRef: EmbeddedViewRef<CurrentCountersContext>;
  private subscription: Subscription;


  constructor(
    private requestService: RequestService,
    private visitsService: DealService,
    private directChat: DirectChat,
    private templateRef: TemplateRef<CurrentCountersContext>,
    private vcr: ViewContainerRef
  ) {
  }

  ngOnInit(): void {
    this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
    this.subscription = this.counters$.subscribe(v => this.context.$implicit = v);
  }


  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    if (this.viewRef) {
      this.viewRef.destroy();
      this.viewRef = null;
    }
  }

}

