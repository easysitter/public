import {Apollo, gql} from 'apollo-angular';
import { Directive, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';


import { pluck } from 'rxjs/operators';

interface UserInfo {
  id: string;
  profile: string;
}

interface UserDirectiveContext {
  $implicit: UserInfo
}

@Directive({
  selector: '[naUser]'
})
export class UserDirective implements OnInit {
  private context: UserDirectiveContext = {
    $implicit: null
  };

  constructor(
    private templateRef: TemplateRef<any>,
    private vcr: ViewContainerRef,
    private apollo: Apollo
  ) {
  }

  ngOnInit() {
    this.apollo.query<{ me: UserInfo }>({
      query: gql`{
          me {
              id
              profile
          }
      }
      `
    })
    .pipe(pluck('data', 'me'))
    .subscribe(user => {
      this.context.$implicit = user;
      this.vcr.createEmbeddedView(this.templateRef, this.context);
    });
  }

}
