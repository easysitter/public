import { Directive, EmbeddedViewRef, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { OAuthService } from 'angular-oauth2-oidc';
import { AuthenticationService } from '@es/authentication';
import { map } from 'rxjs/operators';

@Directive({
  selector: '[naHasRole]'
})
export class HasRoleDirective implements OnInit, OnDestroy {

  @Input()
  naHasRole: string;
  private viewRef: EmbeddedViewRef<void>;
  private subscription: Subscription;

  constructor(
    private authService: AuthenticationService,
    private oAuthService: OAuthService,
    private templateRef: TemplateRef<void>,
    private vcr: ViewContainerRef
  ) {
  }

  ngOnInit(): void {
    const roles$ = this.getRolesStream();
    this.subscription = roles$.subscribe(
      roles => this.onRolesChanged(roles)
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe();
    this.destroyView();
  }

  private getRolesStream() {
    return this.authService.isLoggedIn().pipe(
      map((isLoggedIn) => {
        if (!isLoggedIn) {
          return [];
        }
        const claims = this.oAuthService.getIdentityClaims();
        return claims && claims['roles'] || [];
      })
    );
  }

  private onRolesChanged(roles: string[]) {
    const hasRole = roles.includes(this.naHasRole);
    if (hasRole) {
      this.createView();
    } else {
      this.destroyView();
    }
  }

  private unsubscribe() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private destroyView() {
    if (this.viewRef) {
      this.viewRef.destroy();
    }
  }

  private createView() {
    if (!this.viewRef) {
      this.viewRef = this.vcr.createEmbeddedView(this.templateRef);
    }
    this.viewRef.detectChanges();
    this.viewRef.markForCheck();
  }
}
