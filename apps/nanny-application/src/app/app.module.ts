import { HttpLink } from 'apollo-angular/http';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { WebSocketLink } from '@apollo/client/link/ws';
import { split, InMemoryCache } from '@apollo/client/core';
import { getMainDefinition } from '@apollo/client/utilities';
import {
  BrowserModule,
  HAMMER_GESTURE_CONFIG,
  HammerGestureConfig,
} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, Injectable, LOCALE_ID, NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import localeRu from '@angular/common/locales/ru';
import localePl from '@angular/common/locales/pl';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

import { LayoutModule } from '@es/layout';
import { ProfileUiModule } from '@es/profile';
import { SliderModule } from '@es/slider';
import { RatingModule } from '@es/rating';
import { ChildrenModule } from '@es/children';
import { NannyModule } from '@es/nanny';
import { RequestsModule } from '@es/requests';
import { VideochatModule } from '@es/videochat';
import { provideNotificationHandler } from '@es/notifications/core';
import { WebNotificationsModule } from '@es/notifications/web';
import { NativeNotificationsModule } from '@es/notifications/native';

import { AppComponent } from './app.component';
import { AppRouting } from './app.routing';
import { RequestsPageComponent } from './pages/requests-page/requests-page.component';
import { RequestInnerPageComponent } from './pages/request-inner-page/request-inner-page.component';
import { ConversationsPageComponent } from './pages/conversations-page/conversations-page.component';
import { ChatPageComponent } from './pages/chat-page/chat-page.component';
import { VisitsPageComponent } from './pages/visits-page/visits-page.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { VisitCommentPageComponent } from './pages/visit-comment-page/visit-comment-page.component';
import { ParentPageComponent } from './pages/parent-page/parent-page.component';

import { ListsLayoutComponent } from './components/lists-navigation/lists-layout.component';
import { RequestDetailsComponent } from './components/request-details/request-details.component';
import { ScheduleTimerangeFormComponent } from './components/schedule-timerange-form/schedule-timerange-form.component';
import { ScheduleDayComponent } from './components/schedule-day/schedule-day.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { CurrentCountersDirective } from './directives/current-counters.directive';
import { UserDirective } from './directives/user.directive';

import { RequestService } from './services/requests/requests.service';
import { DealService } from './services/deals/deal.service';
import { GqlRequestsService } from './services/requests/gql-requests.service';
import { GqlDealService } from './services/deals/gql-deal.service';

import { AUTH_GUARD } from './injection-tokens';

import { environment } from '../environments/environment';
import { GqlNannyProfileService } from './services/profile/gql-nanny-profile.service';
import { OperationDefinitionNode } from 'graphql';
import { NgxApolloQueryUpdatesModule } from '@shtrih/ngx-apollo-query-updates';
import { ChatGqlRepo, ChatModule } from '@easysitter-chat/chat';
import { DealsDetailsPageComponent } from './pages/deals-details-page/deals-details-page.component';
import { FilenameFromUrlPipe } from './pipes/filename-from-url.pipe';
import { PwaWrapperComponent } from './components/pwa-wrapper/pwa-wrapper.component';
import { NgxPwaInstallModule } from 'ngx-pwa-install';
import { HasRoleDirective } from './directives/has-role.directive';
import { EsFormsModule } from '@es/forms';
import { SentryErrorHandler } from './services/sentry-error-handler';
import Hammer from 'hammerjs';
import { DealTimeRangeComponent } from './components/deal-time-range/deal-time-range.component';
import { DealStatusBadgeComponent } from './components/deal-status-badge/deal-status-badge.component';
import { DealRealCostComponent } from './components/deal-real-cost/deal-real-cost.component';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import {
  AuthenticationGuard,
  AuthenticationModule,
  AuthenticationService,
} from '@es/authentication';
import { authCodeFlowConfigFactory } from './code-flow.config';
import { take } from 'rxjs/operators';
import {
  DealNotificationTapHandler,
  NewChatMessageNotificationTapHandler,
  RequestNotificationTapHandler,
  VideochatNotificationTapHandler,
} from './notifications/handlers';

import { CarrotquestModule } from '@es/carrotquest';
import { VideochatRequestModule } from './videochat/videochat-request/videochat-request.module';
import {
  Calculator,
  calculatorFactory,
  CalculatorTracer,
  NoopTracer,
} from '@es/calculator';

registerLocaleData(localeRu);
registerLocaleData(localePl);

@Injectable()
export class AppHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    swipe: { direction: Hammer.DIRECTION_HORIZONTAL },
    pinch: { enable: false },
    rotate: { enable: false },
    pan: { enable: false },
  };
}

@NgModule({
  declarations: [
    AppComponent,
    RequestsPageComponent,
    RequestInnerPageComponent,
    ConversationsPageComponent,
    ChatPageComponent,
    VisitsPageComponent,
    ProfilePageComponent,
    VisitCommentPageComponent,
    ParentPageComponent,
    ListsLayoutComponent,
    CurrentCountersDirective,
    RequestDetailsComponent,
    UserDirective,
    ScheduleTimerangeFormComponent,
    ScheduleDayComponent,
    DatePickerComponent,
    DealsDetailsPageComponent,
    FilenameFromUrlPipe,
    PwaWrapperComponent,
    HasRoleDirective,
    DealTimeRangeComponent,
    DealStatusBadgeComponent,
    DealRealCostComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    OAuthModule.forRoot({
      resourceServer: {
        sendAccessToken: true,
      },
    }),
    AuthenticationModule.oidcForRoot(authCodeFlowConfigFactory),
    NgxApolloQueryUpdatesModule.forRoot(),
    AppRouting,
    LayoutModule,
    ServiceWorkerModule.register('sw.js', {
      enabled: environment.production && !environment.native,
    }),
    CarrotquestModule.forRoot('37399-bf6dd71c4c70de19a916a65de3'),
    environment.native
      ? NativeNotificationsModule.forRoot()
      : WebNotificationsModule.forRoot({
          firebase: environment.firebase,
        }),
    SliderModule,
    ProfileUiModule,
    RatingModule,
    HttpClientModule,
    TextFieldModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    ChildrenModule,
    NannyModule.forRoot({
      useForProfile: GqlNannyProfileService,
    }),
    RequestsModule,
    ChatModule.forRoot(ChatGqlRepo),
    NgxPwaInstallModule.forRoot(),
    EsFormsModule,
    VideochatModule.forRoot(environment.opentokApiKey),
    VideochatRequestModule,
  ],
  providers: [
    { provide: ErrorHandler, useClass: SentryErrorHandler },
    { provide: RequestService, useClass: GqlRequestsService },
    { provide: DealService, useClass: GqlDealService },
    { provide: HAMMER_GESTURE_CONFIG, useClass: AppHammerConfig },
    { provide: LOCALE_ID, useValue: 'ru' },
    {
      provide: APOLLO_OPTIONS,
      useFactory: (
        httpLink: HttpLink,
        authentication: AuthenticationService
      ) => {
        let wsApiProtocol = 'ws://';
        let httpApiProtocol = 'http://';
        if (location.protocol === 'https:') {
          wsApiProtocol = 'wss://';
          httpApiProtocol = 'https://';
        }
        let wsUri = wsApiProtocol + location.host;
        let httpUri = httpApiProtocol + location.host;
        if (environment.dataSource) {
          const { dataSource } = environment;
          if (dataSource.secured) {
            wsApiProtocol = 'wss://';
            httpApiProtocol = 'https://';
          }
          wsUri = wsApiProtocol + dataSource.host;
          httpUri = httpApiProtocol + dataSource.host;
        }
        const wsClient = new WebSocketLink({
          uri: `${wsUri}/graphql`,
          options: {
            lazy: true,
            reconnect: true,
            connectionParams: async () => {
              const isLoggedIn = await authentication
                .isLoggedIn()
                .pipe(take(1))
                .toPromise();
              if (!isLoggedIn) {
                return {};
              }

              const token = await authentication
                .getAccessToken()
                .pipe(take(1))
                .toPromise();
              const Authorization = !!token ? `Bearer ${token}` : false;
              const params: any = {};

              if (Authorization) {
                params['Authorization'] = Authorization;
              }
              return params;
            },
          },
        });

        const httpClient = httpLink.create({
          uri: `${httpUri}/graphql`,
        });
        const link = split(
          // split based on operation type
          ({ query }) => {
            const { kind, operation } = getMainDefinition(
              query
            ) as OperationDefinitionNode;
            return (
              kind === 'OperationDefinition' && operation === 'subscription'
            );
          },
          wsClient,
          httpClient
        );

        return {
          cache: new InMemoryCache({
            possibleTypes: {
              ChatUser: ['ChatParentUser', 'ChatNannyUser'],
            },
          }),
          link,
        };
      },
      deps: [HttpLink, AuthenticationService],
    },
    { provide: AUTH_GUARD, useClass: AuthenticationGuard },
    { provide: OAuthStorage, useValue: localStorage },
    provideNotificationHandler({
      type: 'chat-message',
      handler: NewChatMessageNotificationTapHandler,
    }),
    provideNotificationHandler({
      type: 'new-request',
      handler: RequestNotificationTapHandler,
    }),
    provideNotificationHandler({
      type: 'new-offer',
      handler: RequestNotificationTapHandler,
    }),
    provideNotificationHandler({
      type: 'new-deal',
      handler: DealNotificationTapHandler,
    }),
    provideNotificationHandler({
      type: 'deal-reminder',
      handler: DealNotificationTapHandler,
    }),
    provideNotificationHandler({
      type: 'videochat',
      handler: VideochatNotificationTapHandler,
    }),
    {
      provide: Calculator,
      useFactory: () =>
        calculatorFactory({
          dayRate: 34,
          nightRate: 46,
          overtimeStep: 30,
        }),
    },
    {
      provide: CalculatorTracer,
      useClass: NoopTracer,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
