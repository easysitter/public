import { RouterModule, Routes } from '@angular/router';
import { FillProfileGuard } from './guards/fill-profile.guard';
import { RequestsPageComponent } from './pages/requests-page/requests-page.component';
import { RequestInnerPageComponent } from './pages/request-inner-page/request-inner-page.component';
import { ConversationsPageComponent } from './pages/conversations-page/conversations-page.component';
import { ChatPageComponent } from './pages/chat-page/chat-page.component';
import { VisitsPageComponent } from './pages/visits-page/visits-page.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { VisitCommentPageComponent } from './pages/visit-comment-page/visit-comment-page.component';
import { ParentPageComponent } from './pages/parent-page/parent-page.component';
import { ListsLayoutComponent } from './components/lists-navigation/lists-layout.component';
import { AUTH_GUARD } from './injection-tokens';
import { DealsDetailsPageComponent } from './pages/deals-details-page/deals-details-page.component';
import { OnboardingGuard } from './onboarding/onboarding.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'conversations'
  },
  {
    path: 'onboarding',
    loadChildren: () => import('./onboarding/onboarding.module').then(m => m.OnboardingModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: '',
    canActivate: [OnboardingGuard],
    canActivateChild: [OnboardingGuard],
    canLoad: [OnboardingGuard],
    children: [
      // protected area
      {
        path: '',
        canActivateChild: [AUTH_GUARD],
        data: {
          authRedirect: '/login'
        },
        children: [
          {
            path: '',
            canActivate: [FillProfileGuard],
            children: [
              {
                path: '',
                component: ListsLayoutComponent,
                children: [{
                  path: 'requests',
                  component: RequestsPageComponent
                }, {
                  path: 'conversations',
                  component: ConversationsPageComponent
                }, {
                  path: 'visits',
                  children: [{
                    path: '',
                    pathMatch: 'full',
                    redirectTo: 'active'
                  }, {
                    path: 'details/:visitId',
                    component: DealsDetailsPageComponent
                  }, {
                    path: ':filter',
                    component: VisitsPageComponent
                  }]
                }]
              }, {
                path: 'request/:requestId',
                component: RequestInnerPageComponent
              }, {
                path: "chat",
                loadChildren: () => import("./direct-chat/direct-chat-list.module")
                  .then(m => m.DirectChatListModule)
              }, {
                path: 'conversations/:requestId/chat',
                component: ChatPageComponent
              }, {
                path: 'parent/:parentId/chat',
                loadChildren: () => import('./direct-chat/direct-chat.module')
                  .then(m => m.DirectChatModule)
              }, {
                path: 'visit/:visitId/comment',
                component: VisitCommentPageComponent
              }, {
                path: 'parent/:parentId',
                component: ParentPageComponent
              }, {
                path: 'videochat',
                loadChildren: () => import('./videochat/videocall-page/videocall.module')
                  .then(m => m.VideocallModule)
              }
            ]
          },
          {
            path: 'profile',
            component: ProfilePageComponent
          }
        ]
      }
    ]
  }

];

export const AppRouting = RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    relativeLinkResolution: 'legacy'
});
