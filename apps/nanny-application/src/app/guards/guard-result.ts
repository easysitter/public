import { UrlTree } from '@angular/router';

export type GuardResult = boolean | UrlTree;
