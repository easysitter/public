import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GuardResult } from './guard-result';
import { NannyProfileService } from '@es/nanny';

@Injectable({
  providedIn: 'root'
})
export class FillProfileGuard implements CanActivate {
  constructor(
    private router: Router,
    private profileService: NannyProfileService
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<GuardResult> | Promise<GuardResult> | GuardResult {

    return this.profileService.load()
      .pipe(
        map(profile => {
          if (profile && profile.id) return true;
          return this.router.parseUrl('/profile');
        })
      );
  }
}
