import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { GuardResult } from './guard-result';

@Injectable({
  providedIn: 'root'
})
export class WelcomeViewedGuard implements CanActivate {
  constructor(private readonly router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<GuardResult> | Promise<GuardResult> | GuardResult {

    if (localStorage.getItem('welcome viewed')) {
      return this.router.parseUrl('/requests');
    }
    return true;
  }

}
