import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ParentProfileService } from '../../services/parent-profile/parent-profile.service';
import { pluck, switchMap } from 'rxjs/operators';
import { HEADER, LayoutHeader } from '@es/layout';

@Component({
  selector: 'na-parent-page',
  templateUrl: './parent-page.component.html',
  styleUrls: ['./parent-page.component.css'],
})
export class ParentPageComponent implements OnInit {
  parent = this.route.params.pipe(
    pluck('parentId'),
    switchMap((id) => this.parentProfileService.load(id))
  );

  constructor(
    private route: ActivatedRoute,
    private parentProfileService: ParentProfileService,
    @Inject(HEADER)
    private header: LayoutHeader
  ) {}

  ngOnInit() {
    this.header.showBack(true);
  }
}
