import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestInnerPageComponent } from './request-inner-page.component';

describe('RequestInnerPageComponent', () => {
  let component: RequestInnerPageComponent;
  let fixture: ComponentFixture<RequestInnerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RequestInnerPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestInnerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
