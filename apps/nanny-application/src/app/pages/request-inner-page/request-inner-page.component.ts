import { Component, Inject, OnInit } from '@angular/core';
import { RequestService } from '../../services/requests/requests.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { NannyRequestStatus } from '@es/requests';
import { DealService } from '../../services/deals/deal.service';
import { HEADER, LayoutHeader } from '@es/layout';

@Component({
  selector: 'na-request-inner-page',
  templateUrl: './request-inner-page.component.html',
  styleUrls: ['./request-inner-page.component.css'],
})
export class RequestInnerPageComponent implements OnInit {
  Statuses = NannyRequestStatus;
  readonly request$ = this.route.paramMap.pipe(
    switchMap((params) =>
      this.requestService.getRequestDetails(params.get('requestId'))
    )
  );

  constructor(
    private requestService: RequestService,
    private dealService: DealService,
    private route: ActivatedRoute,
    @Inject(HEADER)
    private header: LayoutHeader
  ) {}

  ngOnInit() {
    this.header.showBack(true);
  }
}
