import { Component, ElementRef, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { HEADER, LayoutHeader, LayoutService } from '@es/layout';
import { RequestService } from '../../services/requests/requests.service';

@Component({
  selector: 'na-chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.css'],
})
export class ChatPageComponent implements OnInit {
  requestId = this.activatedRoute.paramMap.pipe(
    map((params) => params.get('requestId'))
  );

  request$ = this.requestId.pipe(
    switchMap((id) => this.requestService.getRequestDetails(id))
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private layout: LayoutService,
    private elRef: ElementRef,
    private requestService: RequestService,
    @Inject(HEADER)
    private header: LayoutHeader
  ) {}

  ngOnInit() {
    this.header.showBack(true);
    this.layout.makeScreen(this.elRef);
  }
}
