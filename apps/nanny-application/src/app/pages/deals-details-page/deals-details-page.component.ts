import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { DealService } from '../../services/deals/deal.service';
import { ActivatedRoute } from '@angular/router';
import { filter, map, pluck, shareReplay, switchMap } from 'rxjs/operators';
import { defer, timer } from 'rxjs';
import { differenceInSeconds, formatDuration, intervalToDuration, parse } from 'date-fns';
import { ru } from 'date-fns/locale';
import { Deal } from '../../models/deal';
import { ReviewFormResult } from '@es/rating';
import { Calculator, CalculatorTracer } from '@es/calculator';

@Component({
  selector: 'na-deals-details-page',
  templateUrl: './deals-details-page.component.html',
  styleUrls: ['./deals-details-page.component.css']
})
export class DealsDetailsPageComponent implements OnInit {

  deal$ = this.route.params.pipe(
    pluck('visitId'),
    switchMap(id => this.dealService.load(id)),
    shareReplay({ bufferSize: 1, refCount: true })
  );

  durationAndCost$ = this.deal$.pipe(
    filter(deal => !!deal.startedAt),
    switchMap(deal => timer(0, 10000).pipe(
      switchMap(() => {
        const startedAt = new Date(deal.startedAt);
        const date = new Date(deal.date);
        const from = parse(`${deal.fromTime}+02`, 'HH:mmX', date);
        const to = parse(`${deal.toTime}+02`, 'HH:mmX', date);
        const finishAt = deal.finishedAt ? new Date(deal.finishedAt) : new Date();
        const interval = {
          start: startedAt,
          end: finishAt
        };
        const duration = formatDuration(intervalToDuration(interval), {
          format: ['years', 'months', 'days', 'hours', 'minutes'],
          // @ts-ignore
          locale: ru
        });
        return defer(() => this.calculator.getPrice({
          from, to, startedAt,
          finishedAt: finishAt,
          children: deal.children
        }, this.calculatorTracer)).pipe(
          map(cost => ({ duration, cost }))
        );
      })
    ))
  );

  constructor(
    private route: ActivatedRoute,
    private dealService: DealService,
    @Inject(LOCALE_ID) private locale,
    private calculator: Calculator,
    private calculatorTracer: CalculatorTracer,
  ) {
  }

  ngOnInit() {
  }

  startVisit(deal: Deal) {
    this.dealService.startVisit(deal.id).subscribe();
  }

  finishVisit(deal: Deal) {
    this.dealService.finishVisit(deal).subscribe();
  }

  submitReview(review: ReviewFormResult, deal: Deal) {
    this.dealService.submitReview(deal.id, review).subscribe();
  }
}
