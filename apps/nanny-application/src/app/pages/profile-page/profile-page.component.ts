import { Apollo, gql } from 'apollo-angular';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NannyProfile, NannyProfileService } from '@es/nanny';
import { EMPTY, Observable, Subject, Subscription, zip } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  exhaustMap,
  first,
  map,
  pluck,
  shareReplay,
  startWith,
  takeUntil,
} from 'rxjs/operators';

import { Router } from '@angular/router';
import { HEADER, LayoutHeader } from '@es/layout';

const AgeCategories = [0, 1, 2, 3, 4, 5];

@Component({
  selector: 'na-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css'],
})
export class ProfilePageComponent implements OnInit, OnDestroy {
  private destroy = new Subject<void>();
  private save: Subject<NannyProfile> = new Subject<NannyProfile>();
  form = this.buildForm();

  districtsChecked = this.form.valueChanges.pipe(
    startWith(this.form.value),
    pluck('districts'),
    map((controls: { isSelected: boolean }[]) => ({
      all: controls.length,
      checked: controls.filter(({ isSelected }) => isSelected).length,
    }))
  );
  allDistrictsChecked: Observable<boolean> = this.districtsChecked.pipe(
    map(({ all, checked }) => all === checked)
  );
  districtsCollapsed = true;
  private districts$ = this.apollo
    .query<{ districts: { id: string; title: string }[] }>({
      query: gql`
        query {
          districts {
            id
            title
          }
        }
      `,
      fetchPolicy: 'cache-first',
    })
    .pipe(
      pluck('data', 'districts'),
      shareReplay({ bufferSize: 1, refCount: true })
    );
  photoUploading = false;
  private photoUploadSubscription: Subscription;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private profile: NannyProfileService,
    private apollo: Apollo,
    @Inject(HEADER)
    private header: LayoutHeader
  ) {}

  get schedule() {
    return this.form.get('schedule') as FormArray;
  }

  get ageCategories() {
    return this.form.get('ageCategories') as FormArray;
  }

  get districts() {
    return this.form.get('districts') as FormArray;
  }

  ngOnInit() {
    this.header.showBack(true);
    this.districts$.subscribe((districtsData) => {
      const districtsGroup = this.formBuilder.array(
        districtsData.map((item) =>
          this.formBuilder.group({
            id: item.id,
            title: item.title,
            isSelected: true,
          })
        )
      );
      this.form.setControl('districts', districtsGroup);
    });
    this.preloadProfile();
    this.handleSave();
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  submit() {
    if (this.form.invalid) return;
    const { value } = this.form;
    const profile: NannyProfile = {
      name: value.first_name,
      birthDate: value.birth_date,
      description: value.about,
      photo: value.photo,
      phone: value.phone,
      price: {
        value: Number(value.rate),
        currency: 'USD',
      },
      trial: {
        value: Number(value.trialRate),
        currency: 'USD',
      },
      trialEnabled: true,
      schedule: value.schedule.reduce((arr, day) => {
        return arr.concat(day.items.map((item) => ({ day: day.day, ...item })));
      }, []),
      ageCategories: value.ageCategories
        .filter((c) => c.selected)
        .map((c) => c.type),
      maxChildrenCount: value.maxChildrenCount,
      documents: value.documents,
      features: value.features,
      districts: value.districts
        .filter(({ isSelected }) => isSelected)
        .map(({ id }) => id),
    };
    this.save.next(profile);
  }

  updatePhoto($event: Event) {
    const { target } = $event as unknown as { target: HTMLInputElement };
    if (!target.files.length) return;
    const file = target.files[0];
    target.value = null;
    if (this.photoUploadSubscription) {
      this.photoUploadSubscription.unsubscribe();
    }
    this.photoUploading = true;
    this.photoUploadSubscription = this.upload(file, 'photo').subscribe(
      (src) => {
        this.photoUploading = false;
        console.log(src);
        this.form.patchValue({
          photo: src,
        });
      }
    );
  }

  toggleAllDistricts($event: Event) {
    const target = $event.target as HTMLInputElement;
    const districts = this.form.get('districts').value.map(({ id, title }) => ({
      id,
      title,
      isSelected: target.checked,
    }));
    this.form.patchValue({ districts });
  }

  private scheduleToControl(scheduleRaw) {
    return [1, 2, 3, 4, 5, 6, 7].map((day) => ({
      day,
      items: scheduleRaw
        .filter((item) => item.day === day)
        .map(({ from, to }) => ({ from, to })),
    }));
  }

  private handleSave() {
    this.save
      .pipe(
        takeUntil(this.destroy),
        exhaustMap((formData) =>
          this.profile.save(formData).pipe(catchError(() => EMPTY))
        )
      )
      .subscribe(() => {
        this.router.navigate(['/']);
      });
  }

  decMaxChildrenCount() {
    const control = this.form.get('maxChildrenCount');
    control.patchValue(Math.max(1, control.value - 1));
  }

  private requireDocumentForFeature(
    form: FormGroup,
    featureName: string,
    documentName: string
  ) {
    const featureControl = form.get('features').get(featureName);
    featureControl.valueChanges
      .pipe(
        startWith(featureControl.value),
        distinctUntilChanged(),
        takeUntil(this.destroy)
      )
      .subscribe((value: boolean) => {
        const documents = form.get('documents') as FormGroup;
        if (!value) {
          documents.removeControl(documentName);
          return;
        }
        if (documents.contains(documentName)) return;
        const documentControl = this.formBuilder.control(
          '',
          Validators.required
        );
        documents.addControl(documentName, documentControl);
      });
  }

  private upload(file: File, resourceType: string) {
    return this.apollo
      .mutate({
        mutation: gql`
          mutation upload($file: Upload!, $resourceType: String) {
            uploadFile(file: $file, resourceType: $resourceType) {
              public_url
            }
          }
        `,
        variables: { file, resourceType },
        context: {
          useMultipart: true,
        },
      })
      .pipe(pluck('data', 'uploadFile', 'public_url'));
  }

  private buildForm(): FormGroup {
    const form = this.formBuilder.group({
      first_name: '',
      birth_date: '',
      phone: '',
      about: '',
      photo: '',
      schedule: this.formBuilder.array(this.scheduleToControl([])),
      rate: 0,
      trialRate: 0,
      maxChildrenCount: 1,
      ageCategories: this.formBuilder.array(
        AgeCategories.map((child) =>
          this.formBuilder.group({
            type: child,
            selected: true,
          })
        )
      ),
      documents: this.formBuilder.group({
        krk: '',
      }),
      features: this.formBuilder.group({
        nosmoking: true,
        first_aid: false,
        driving_licence: false,
        car: false,
        education: true,
      }),
      districts: this.formBuilder.array([]),
    });

    this.requireDocumentForFeature(form, 'first_aid', 'first_aid_diploma');
    this.requireDocumentForFeature(form, 'driving_licence', 'driving_licence');
    this.requireDocumentForFeature(form, 'education', 'education_certificate');

    return form;
  }

  incMaxChildrenCount() {
    const control = this.form.get('maxChildrenCount');
    control.patchValue(+control.value + 1);
  }

  private preloadProfile() {
    zip(this.districts$, this.profile.load())
      .pipe(
        first(),
        map(([, profile]) => profile),
        takeUntil(this.destroy)
      )
      .subscribe(
        (
          profile: NannyProfile & {
            documents: string;
            features: string;
            districts: string[];
          }
        ) => {
          if (!profile || !profile.id) return;
          const formValue: Record<any, any> = {};
          formValue.first_name = profile.name;
          formValue.birth_date = profile.birthDate;
          formValue.phone = profile.phone;
          formValue.about = profile.description;
          formValue.rate = profile.price.value;
          formValue.trialRate = profile.trial.value;
          formValue.photo = profile.photo;

          formValue.schedule = this.scheduleToControl(profile.schedule);
          formValue.maxChildrenCount = profile.maxChildrenCount;
          formValue.ageCategories = AgeCategories.map((c) => {
            return {
              type: c,
              selected: profile.ageCategories.includes(c),
            };
          });
          if (profile.features) {
            formValue.features = profile.features;
          }
          if (profile.documents) {
            formValue.documents = profile.documents;
          }
          if (profile.districts) {
            formValue.districts = this.districts.value.map(({ id, title }) => ({
              id,
              title,
              isSelected: profile.districts.includes(id),
            }));
          }
          this.form.patchValue(formValue);
        }
      );
  }
}
