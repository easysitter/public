import { Component, HostListener, OnInit } from '@angular/core';
import { DealService } from '../../services/deals/deal.service';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { count } from '../../utils/operators';
import { Observable } from 'rxjs';
import { Deal } from '../../models/deal';

type VisitFilter = 'active' | 'pending review' | 'closed'

@Component({
  selector: 'na-visits-page',
  templateUrl: './visits-page.component.html',
  styleUrls: ['./visits-page.component.css']
})
export class VisitsPageComponent implements OnInit {

  public readonly active$ = this.dealService.getActive().pipe(count());
  public readonly history$ = this.dealService.getHistory().pipe(count());
  public readonly pending$ = this.dealService.getPendingReview().pipe(count());

  public readonly deals$ = this.route.params.pipe(
    switchMap(({ filter }) => this.switchStrategy(filter))
  );

  constructor(
    private route: ActivatedRoute,
    private readonly dealService: DealService,
    private readonly router: Router
  ) {
  }

  ngOnInit() {
  }

  private switchStrategy(filter: VisitFilter): Observable<Deal[]> {
    const strategies = {
      'active': this.dealService.getActive(),
      'review': this.dealService.getPendingReview(),
      'history': this.dealService.getHistory()
    };
    return strategies[filter];
  }

  @HostListener('swiperight')
  navigateToConversations() {
    this.router.navigateByUrl('/conversations');
  }
}
