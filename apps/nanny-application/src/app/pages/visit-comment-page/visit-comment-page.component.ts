import { Component, Inject, OnInit } from '@angular/core';
import { HEADER, LayoutHeader } from '@es/layout';

@Component({
  selector: 'na-visit-comment-page',
  templateUrl: './visit-comment-page.component.html',
  styleUrls: ['./visit-comment-page.component.css'],
})
export class VisitCommentPageComponent implements OnInit {
  constructor(
    @Inject(HEADER)
    private header: LayoutHeader
  ) {}

  ngOnInit() {
    this.header.showBack(true);
  }
}
