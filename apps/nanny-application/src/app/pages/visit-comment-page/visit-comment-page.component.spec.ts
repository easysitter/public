import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitCommentPageComponent } from './visit-comment-page.component';

describe('VisitCommentPageComponent', () => {
  let component: VisitCommentPageComponent;
  let fixture: ComponentFixture<VisitCommentPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VisitCommentPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitCommentPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
