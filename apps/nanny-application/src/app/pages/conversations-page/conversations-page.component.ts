import { Component, OnInit } from '@angular/core';
import { RequestService } from '../../services/requests/requests.service';
import { DealService } from '../../services/deals/deal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'na-conversations-page',
  templateUrl: './conversations-page.component.html',
  styleUrls: ['./conversations-page.component.css']
})
export class ConversationsPageComponent implements OnInit {

  public readonly source$ = this.requestService.getConversations();

  constructor(
    private requestService: RequestService,
    private dealService: DealService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  navigateToVisits() {
    this.router.navigateByUrl('/visits');
  }
}
