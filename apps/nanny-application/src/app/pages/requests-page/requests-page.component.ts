import { Component, OnInit } from '@angular/core';
import { RequestService } from '../../services/requests/requests.service';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BehaviorSubject, combineLatest } from 'rxjs';

@Component({
  selector: 'na-requests-page',
  templateUrl: './requests-page.component.html',
  styleUrls: ['./requests-page.component.css']
})
export class RequestsPageComponent implements OnInit {
  hasAction = new BehaviorSubject(false);
  public readonly source$ = this.requestService.getRequests();

  allResolved = combineLatest([
    this.source$,
    this.hasAction
  ]).pipe(
    filter(([, hasAction]) => hasAction),
    filter(([requests]) => requests.length === 0)
  );

  constructor(
    private requestService: RequestService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.allResolved.subscribe(() => this.navigateToConversations());
  }

  navigateToConversations() {
    console.log('swipeleft');
    return this.router.navigate(['/conversations']);
  }
}
