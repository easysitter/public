import { Component, Input } from '@angular/core';
import { Deal } from '../../models/deal';

@Component({
  selector: 'na-deal-time-range',
  templateUrl: './deal-time-range.component.html',
  styleUrls: ['./deal-time-range.component.css']
})
export class DealTimeRangeComponent {
  @Input() deal: Deal;

}
