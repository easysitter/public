import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListsLayoutComponent } from './lists-layout.component';

describe('NannyListsNavigationComponent', () => {
  let component: ListsLayoutComponent;
  let fixture: ComponentFixture<ListsLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListsLayoutComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListsLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
