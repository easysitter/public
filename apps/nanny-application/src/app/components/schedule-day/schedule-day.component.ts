import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'na-schedule-day',
  templateUrl: './schedule-day.component.html',
  styleUrls: ['./schedule-day.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ScheduleDayComponent),
    multi: true
  }]
})
export class ScheduleDayComponent implements OnInit, ControlValueAccessor {
  day: number;
  items: { from: string, to: string }[] = [];

  editorMode = false;
  private _onTouched: (_: any) => void;
  private _onChange: (_: any) => void;

  ngOnInit(): void {
  }


  registerOnChange(fn: (_: any) => void): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: (_: any) => void): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: any): void {
    const { day, items } = obj;
    this.day = day;
    this.items = items;
  }

  edit() {
    this.editorMode = true;
  }

  remove(item) {
    this.items = this.items.filter(i => i !== item);
    this._onChange({ day: this.day, items: this.items });
  }

  addTimeSpan({ from, to }) {
    if (this.isFullDay({from, to})) {
      this.items = [{ from, to }];
    } else {
      this.items.push({ from, to });
    }
    this._onChange({ day: this.day, items: this.items });
    this.editorMode = false;
  }

  isFullDay({from, to}) {
    return from === "00:00" && to === "00:00"
  }
}
