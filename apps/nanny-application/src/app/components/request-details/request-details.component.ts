import { Component, Input, OnInit } from '@angular/core';
import { NannyRequest, ParentRequest } from '@es/requests';

@Component({
  selector: 'na-request-details',
  templateUrl: './request-details.component.html',
  styleUrls: ['./request-details.component.css']
})
export class RequestDetailsComponent implements OnInit {
  @Input()
  details: ParentRequest & { district: { title: string, isOwn: boolean } };

  @Input()
  id: NannyRequest['request']['id'];

  constructor() {
  }

  ngOnInit() {
  }
}
