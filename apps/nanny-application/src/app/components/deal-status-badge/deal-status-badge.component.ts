import { Component, Input } from '@angular/core';
import { Deal } from '../../models/deal';

@Component({
  selector: 'na-deal-status-badge',
  templateUrl: './deal-status-badge.component.html',
  styleUrls: ['./deal-status-badge.component.css']
})
export class DealStatusBadgeComponent {
  @Input() deal: Deal;
}
