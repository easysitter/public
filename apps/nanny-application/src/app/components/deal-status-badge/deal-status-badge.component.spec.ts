import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealStatusBadgeComponent } from './deal-status-badge.component';

describe('DealStatusBadgeComponent', () => {
  let component: DealStatusBadgeComponent;
  let fixture: ComponentFixture<DealStatusBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DealStatusBadgeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealStatusBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
