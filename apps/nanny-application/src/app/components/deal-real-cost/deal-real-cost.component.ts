import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { parse } from 'date-fns';

import { Calculator, CalculatorTracer } from '@es/calculator';

import { Deal } from '../../models/deal';

@Component({
  selector: 'na-deal-real-cost',
  template: `{{cost | currency:'PLN':'symbol':'1.0-0'}}`
})
export class DealRealCostComponent implements OnChanges {
  @Input() deal: Deal;

  private _cost;
  get cost() {
    return this._cost;
  }

  constructor(
    private calculator: Calculator,
    private calculatorTracer: CalculatorTracer,
  ) {
  }

  async ngOnChanges(changes: SimpleChanges) {
    if ('deal' in changes) {
      const { deal } = this;
      if (deal.isActive || deal.tag === 'suspended' || deal.tag === 'cancelled') {
        this._cost = null;
        return;
      }

      const date = new Date(deal.date);
      const from = parse(`${deal.fromTime}+02`, 'HH:mmX', date);
      const to = parse(`${deal.toTime}+02`, 'HH:mmX', date);
      this._cost = await this.calculator.getPrice({
        from,
        to,
        children: deal.children,
        finishedAt: new Date(deal.finishedAt),
        startedAt: new Date(deal.startedAt)
      }, this.calculatorTracer);
    }
  }
}
