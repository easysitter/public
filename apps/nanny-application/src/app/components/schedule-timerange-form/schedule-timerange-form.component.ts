import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { NgxMaterialTimepickerTheme } from "ngx-material-timepicker";
import { FormControl, FormGroup } from "@angular/forms";
import { merge, of } from "rxjs";

@Component({
  selector: "na-schedule-timerange-form",
  templateUrl: "./schedule-timerange-form.component.html",
  styleUrls: ["./schedule-timerange-form.component.css"]
})
export class ScheduleTimerangeFormComponent implements OnInit {
  @Output() add = new EventEmitter();
  theme: NgxMaterialTimepickerTheme = {
    clockFace: {
      clockFaceBackgroundColor: "var(--white)",
      clockHandColor: "var(--secondary)",
      clockFaceTimeActiveColor: "var(--white)"
      // clockFaceBackgroundColor: '#5fa1cb'
    },
    dial: {
      dialBackgroundColor: "var(--secondary)"
    },
    container: {
      buttonColor: "var(--secondary)"
    }
  };
  anyTimeControl = new FormControl(true);

  readonly DEFAULT_FROM = '8:00';
  readonly DEFAULT_TO = '21:00';

  timeRangeGroup = new FormGroup({
    from: new FormControl(this.DEFAULT_FROM),
    to: new FormControl(this.DEFAULT_TO)
  });

  constructor() {
  }

  ngOnInit() {
    merge(
      this.anyTimeControl.valueChanges,
      of(this.anyTimeControl.value)
    ).subscribe(
      checked => {
        if (checked) {
          this.timeRangeGroup.patchValue({
            from: "0:00",
            to: "24:00"
          });
        } else {
          this.timeRangeGroup.patchValue({
            from: this.DEFAULT_FROM,
            to: this.DEFAULT_TO
          });
        }
      }
    );
  }

  handleClick(from: string, to: string) {
    this.add.emit({ from, to });
  }
}
