import { InjectionToken } from '@angular/core';
import { CanActivate } from '@nestjs/common';

export const AUTH_GUARD = new InjectionToken<CanActivate>('AUTH_GUARD');
