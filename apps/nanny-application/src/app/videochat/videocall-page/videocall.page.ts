import {Apollo, gql} from 'apollo-angular';
import { Component, ElementRef, OnInit } from '@angular/core';
import { LayoutService } from '@es/layout';
import { ActivatedRoute, Router } from '@angular/router';
import { map, pluck, switchMap } from 'rxjs/operators';



@Component({
  selector: 'na-videocall-page',
  templateUrl: './videocall.page.html',
  styleUrls: ['./videocall.page.css']
})
export class VideocallPage implements OnInit {

  call = this.route.queryParamMap.pipe(
    map(params => params.get('id')),
    switchMap(id => {
      return this.apollo.query<{ videochat: { token: { token: string, session: string } } }>({
        query: gql`
          query videochat($id: ID!) {
            videochat(id: $id) {
              token{
                session
                token
              }
            }
          }
        `,
        variables: { id }
      }).pipe(
        pluck('data', 'videochat', 'token')
      );
    })
  );

  constructor(
    private layout: LayoutService,
    private elRef: ElementRef,
    private route: ActivatedRoute,
    private apollo: Apollo,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.layout.makeScreen(this.elRef);
  }

  onCallEnded() {
    const { queryParamMap } = this.route.snapshot;
    let next = '/';
    if (queryParamMap.has('next')) {
      next = queryParamMap.get('next');
    }
    this.router.navigateByUrl(next);
  }
}
