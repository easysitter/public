import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { VideochatModule } from '@es/videochat';

import { VideocallPage } from './videocall.page';

@NgModule({
  declarations: [VideocallPage],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: VideocallPage
    }]),
    VideochatModule
  ]
})
export class VideocallModule {

}
