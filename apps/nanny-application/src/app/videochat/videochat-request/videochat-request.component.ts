import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'na-videochat-request',
  templateUrl: './videochat-request.component.html',
  styleUrls: ['./videochat-request.component.css']
})
export class VideochatRequestComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA)
    private data: { id: string },
    private ref: MatDialogRef<VideochatRequestComponent>
  ) {
    console.log(route.toString());
  }

  ngOnInit(): void {

  }

  accept() {

    let next = location.href.replace(location.origin, '');
    if (this.route.snapshot.queryParamMap.has('next')) {
      next = this.route.snapshot.queryParamMap.get('next');
    }
    this.router.navigate(['/videochat'], { queryParams: { id: this.data.id, next } });
    this.ref.close();
  }
}
