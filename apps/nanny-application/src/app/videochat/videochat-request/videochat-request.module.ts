import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { VideochatRequestComponent } from './videochat-request.component';

@NgModule({
    declarations: [
        VideochatRequestComponent
    ],
    exports: [
        VideochatRequestComponent
    ],
    imports: [
        MatDialogModule
    ]
})
export class VideochatRequestModule {

}
