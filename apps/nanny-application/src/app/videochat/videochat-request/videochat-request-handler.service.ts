import { Apollo, gql } from 'apollo-angular';
import { Injectable, OnDestroy } from '@angular/core';
import { EMPTY, Subject, Subscription } from 'rxjs';
import { AuthenticationService } from '@es/authentication';

import { MatDialog } from '@angular/material/dialog';
import {
  catchError,
  concatMap,
  filter,
  mergeMap,
  pluck,
  retry,
  switchMap,
  switchMapTo,
} from 'rxjs/operators';

import { VideochatRequestComponent } from './videochat-request.component';

@Injectable({
  providedIn: 'root',
})
export class VideochatRequestHandlerService implements OnDestroy {
  private subscription: Subscription;
  private sessions = new Subject<number | string>();

  constructor(
    private auth: AuthenticationService,
    private apollo: Apollo,
    private dialog: MatDialog
  ) {}

  initialize() {
    this.subscription = new Subscription();
    const apolloSubscription = this.auth
      .isLoggedIn()
      .pipe(
        filter((isLoggedIn) => !!isLoggedIn),
        switchMap(() =>
          this.apollo.subscribe<{ incomingVideochat: { id: string } }>({
            query: gql`
              subscription incomingVideochat {
                incomingVideochat {
                  id
                }
              }
            `,
          })
        ),
        pluck('data', 'incomingVideochat', 'id'),
        retry()
      )
      .subscribe((id) => this.handle(id));

    const dialogSubscription = this.sessions
      .pipe(
        concatMap((sessionId) => {
          const dialog = this.openDialog(sessionId);
          return dialog.afterClosed().pipe(
            filter((accepted): accepted is false => !accepted),
            mergeMap(() => {
              return this.refuseVideochat(sessionId);
            }),
            catchError(() => EMPTY)
          );
        })
      )
      .subscribe();
    this.subscription.add(apolloSubscription);
    this.subscription.add(dialogSubscription);
  }

  private refuseVideochat(sessionId) {
    return this.apollo
      .mutate({
        mutation: gql`
          mutation refuseVideochat($sessionId: ID!) {
            refuseVideochat(sessionId: $sessionId)
          }
        `,
        variables: {
          sessionId: sessionId.toString(),
        },
      })
      .pipe(switchMapTo(EMPTY));
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  public handle(sessionId: string) {
    this.sessions.next(sessionId);
  }

  private openDialog(sessionId) {
    return this.dialog.open<
      VideochatRequestComponent,
      { id: number | string },
      boolean
    >(VideochatRequestComponent, {
      closeOnNavigation: true,
      hasBackdrop: false,
      position: {
        bottom: '5rem',
      },
      data: {
        id: sessionId,
      },
    });
  }
}
