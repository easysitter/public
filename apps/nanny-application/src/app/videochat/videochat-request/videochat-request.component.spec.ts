import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideochatRequestComponent } from './videochat-request.component';

describe('VideochatRequestComponent', () => {
  let component: VideochatRequestComponent;
  let fixture: ComponentFixture<VideochatRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VideochatRequestComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideochatRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
