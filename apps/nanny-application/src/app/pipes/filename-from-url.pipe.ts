import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filenameFromUrl',
  pure: true
})
export class FilenameFromUrlPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const url = new URL(value);
    return url.pathname.split('/').pop();
  }

}
