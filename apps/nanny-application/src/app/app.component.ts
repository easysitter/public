import {Apollo, gql} from 'apollo-angular';
import { Component } from '@angular/core';
import { AuthenticationService } from '@es/authentication';
import { SwUpdate } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { filter, switchMap, take } from 'rxjs/operators';


import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { NotificationsProvider } from '@es/notifications/core';
import { NotificationsTapSubscriber } from '@es/notifications/core';
import { NannyProfileService } from '@es/nanny';
import { VideochatRequestHandlerService } from './videochat/videochat-request/videochat-request-handler.service';

@Component({
  selector: 'na-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  profile = this.profileService.load();

  constructor(
    private auth: AuthenticationService,
    private profileService: NannyProfileService,
    private oAuthService: OAuthService,
    private notificationsProvider: NotificationsProvider,
    private notificationTapSubscriber: NotificationsTapSubscriber,
    private update: SwUpdate,
    private apollo: Apollo,
    private router: Router,
    private videochatRequestHandler: VideochatRequestHandlerService
  ) {
    if (environment.native) {
      this.oAuthService.events
        .pipe(filter(({ type }) => type === 'token_expires'))
        .subscribe(() => {
          console.log('manual refresh');
          this.oAuthService.refreshToken();
        });
    }
    update.available
      .pipe(switchMap(() => update.activateUpdate()))
      .subscribe(() => {
        console.log('reload for update');
        document.location.reload();
      });
    this.initializeNotifications();
    this.initializeVideochatRequestHandler();
  }

  async logout() {
    await this.auth.logout();
    await this.router.navigateByUrl('/login');
  }

  private initializeNotifications() {
    this.auth
      .isLoggedIn()
      .pipe(
        filter((isLoggedIn) => !!isLoggedIn),
        take(1),
        switchMap(() => this.notificationsProvider.initialize()),
        switchMap((token) =>
          this.apollo.mutate({
            mutation: gql`
              mutation saveNotificationToken($token: String) {
                saveNotificationToken(token: $token)
              }
            `,
            variables: {
              token,
            },
          })
        )
      )
      .subscribe(() => {
        this.notificationTapSubscriber.subscribe(this.notificationsProvider);
      });

    this.notificationsProvider.subscribeNotifications().subscribe((n) => {
      console.group('notification');
      console.log(n);
      console.groupEnd();
      if (n.data.type === 'videochat') {
        this.videochatRequestHandler.handle(n.data.sessionId);
      }
    });
  }

  private initializeVideochatRequestHandler() {
    this.videochatRequestHandler.initialize();
  }
}
