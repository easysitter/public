import {
  Directive,
  EmbeddedViewRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { ParentProfile, ParentProfileService } from '../services/parent-profile/parent-profile.service';
import { Subscription } from 'rxjs';

interface ParentProfileContext {
  $implicit: ParentProfile
}

@Directive({
  selector: '[naParentProfile]'
})
export class ParentProfileDirective implements OnChanges, OnDestroy {

  @Input('naParentProfileId')
  id: string;
  private context: ParentProfileContext;
  private subscription: Subscription;
  private viewRef: EmbeddedViewRef<ParentProfileContext>;

  constructor(
    private parentProfileService: ParentProfileService,
    private templateRef: TemplateRef<ParentProfileContext>,
    private vcr: ViewContainerRef
  ) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('id' in changes) {
      this.init();
    }
  }

  ngOnDestroy() {
    this.destroy();
  }

  private init() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = this.parentProfileService.load(this.id).subscribe(
      profile => {
        if (!this.viewRef) {
          this.context = { $implicit: profile };
          this.viewRef = this.vcr.createEmbeddedView(this.templateRef, this.context);
        } else {
          this.context.$implicit = profile;
        }
      }
    );
  }

  private destroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.viewRef) {
      this.viewRef.destroy();
    }
  }
}
