import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectChatListPage } from './direct-chat-list.page';

describe('DirectChatListPage', () => {
  let component: DirectChatListPage;
  let fixture: ComponentFixture<DirectChatListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectChatListPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectChatListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
