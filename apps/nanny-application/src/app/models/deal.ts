export interface Review {
  id: string;
  rating: number;
  pros: string;
  cons: string;

}

export interface Deal {
  id: number;

  parent: { id: number, name: string, photo: string };

  date: Date;
  fromTime: string;
  toTime: string;

  children: number[];
  address: string;
  wishes: string;
  trial: boolean;
  rate: number;

  isActive: boolean;
  state: 'new' | 'pending review' | 'closed';
  tag: string;

  startedAt?: Date
  finishedAt?: Date

  review: Review
}
