export interface Collection<T> {
  has(value: Partial<T>): boolean;

  add(value: T): Collection<T>;

  remove(value: Partial<T>): Collection<T>;

  asArray(): T[]
}

export class ArrayCollection<T extends { id: any }> implements Collection<T> {
  private value: T[];

  constructor(sourceValue: T[]) {
    this.value = [...sourceValue];
  }

  has(value: Partial<T>): boolean {
    return !!this.value.find(iteratee => iteratee.id === value.id);
  }

  add(value: T) {
    this.value = [...this.value, value];
    return this;
  }

  remove(value: Partial<T>) {
    this.value = this.value.filter(iteratee => iteratee.id !== value.id);
    return this;
  }

  asArray() {
    return this.value;
  }
}
