import {ApolloQueryResult} from '@apollo/client/core';
import { MonoTypeOperatorFunction, OperatorFunction, pipe } from 'rxjs';
import { map, pluck, shareReplay, startWith } from 'rxjs/operators';


export function filterArray<T>(predicate: (item: T) => boolean): MonoTypeOperatorFunction<T[]> {
  return source => source.pipe(map(values => values.filter(predicate)));
}

export function count(): OperatorFunction<Array<any>, number> {
  return source => source.pipe(
    map(r => r.length),
    startWith(0)
  );
}

export function shareQuery(key: string): OperatorFunction<ApolloQueryResult<any>, any> {
  return pipe(
    pluck('data', key),
    startWith([]),
    shareReplay({ refCount: false, bufferSize: 1 })
  );
}
