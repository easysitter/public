import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OnboardingService {

  constructor() {
    const onboardingStatus = localStorage.getItem('nanny onboarding');
    if (onboardingStatus && onboardingStatus === 'done') {
      this._isCompleted = true;
    }
  }

  private _isCompleted = false;

  get isCompleted() {
    return this._isCompleted;
  }

  complete() {
    localStorage.setItem('nanny onboarding', 'done');
    this._isCompleted = true;
  }
}
