import { Component, ElementRef, OnInit } from '@angular/core';
import { LayoutService } from '@es/layout';
import { OnboardingService } from './onboarding.service';

@Component({
  selector: 'na-onboarding-page',
  templateUrl: './onboarding.page.html',
  styleUrls: ['./onboarding.page.css']
})
export class OnboardingPage implements OnInit {
  tutorialFinished = false;

  constructor(
    private readonly elRef: ElementRef,
    private readonly layout: LayoutService,
    private readonly onboarding: OnboardingService
  ) {
  }

  ngOnInit() {
    this.layout.makeScreen(this.elRef);
  }


  navigated($event: { idx: number; last: boolean }) {
    if ($event.last) {
      this.tutorialFinished = true;
      this.onboarding.complete();
    }
  }

  skip() {
    this.onboarding.complete();
  }
}
