import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SliderModule } from '@es/slider';

import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingPage } from './onboarding.page';


@NgModule({
  declarations: [OnboardingPage],
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    SliderModule
  ]
})
export class OnboardingModule {
}
