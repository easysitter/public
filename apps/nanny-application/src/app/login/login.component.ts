import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BehaviorSubject, EMPTY, from, Subject } from 'rxjs';
import { catchError, exhaustMap, filter, map, switchMap } from 'rxjs/operators';
import { AuthenticationService, isInvalidGrantError } from '@es/authentication';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'na-login',
  templateUrl: './login.component.html',
  styles: [],
})
export class LoginComponent implements OnInit {
  loading = new BehaviorSubject(false);
  submitLogin = new Subject();
  form = this.fb.group({
    login: ['', Validators.required],
    password: ['', Validators.required],
  });
  tryLogin = this.submitLogin.pipe(
    map(() => this.form.value),
    exhaustMap(({ login, password }) => {
      this.loading.next(true);
      this.error = null;
      return from(this.authService.login({ login, password })).pipe(
        catchError((err) => {
          this.loading.next(false);
          if (isInvalidGrantError(err)) {
            this.error = 'Неверный логин или пароль';
          } else {
            this.error = 'Произошла ошибка. Попробуйте еще раз';
          }
          return EMPTY;
        })
      );
    }),
    switchMap(() => this.authService.isLoggedIn()),
    filter((loggedId) => loggedId),
    map(() =>
      this.route.snapshot.queryParamMap.has('next')
        ? this.route.snapshot.queryParamMap.get('next')
        : ''
    ),
    map((nextUrl) => nextUrl || ''),
    map((nextUrl) => `/${nextUrl}`)
  );
  error?: string;

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.tryLogin.subscribe((next) => this.router.navigateByUrl(next));
  }

  login() {
    if (this.form.invalid) return;
    this.submitLogin.next();
  }
}
