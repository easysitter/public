import { Router } from '@angular/router';
import { NotificationTapHandler } from '@es/notifications/core';
import { Injectable } from '@angular/core';

@Injectable()
export class RequestNotificationTapHandler implements NotificationTapHandler {
  constructor(private router: Router) {}

  handle(data: any) {
    const url = '/request/' + data.request;
    this.router.navigateByUrl(url);
  }
}
