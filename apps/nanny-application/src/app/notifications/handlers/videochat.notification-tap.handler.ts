import { NotificationTapHandler } from '@es/notifications/core';
import { VideochatRequestHandlerService } from '../../videochat/videochat-request/videochat-request-handler.service';

export class VideochatNotificationTapHandler implements NotificationTapHandler {
  constructor(
    private videochatRequestHandler: VideochatRequestHandlerService
  ) {}

  handle(data: any) {
    this.videochatRequestHandler.handle(data.sessionId);
  }
}
