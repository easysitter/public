import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationTapHandler } from '@es/notifications/core';

@Injectable()
export class NewChatMessageNotificationTapHandler
  implements NotificationTapHandler {
  constructor(private router: Router) {}

  handle(data): void {
    const { chat_type, request, chat_users } = data;
    let url;
    if (chat_type === 'direct') {
      const user = chat_users.split(':').find((id) => id.startsWith('p'));
      url = '/parent/' + (user || '').replace(/^p/i, '') + '/chat';
    } else {
      url = '/conversations/' + request + '/chat';
    }
    this.router.navigateByUrl(url);
  }
}
