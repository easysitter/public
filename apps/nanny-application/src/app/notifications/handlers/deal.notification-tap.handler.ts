import { Router } from '@angular/router';
import { NotificationTapHandler } from '@es/notifications/core';
import { Injectable } from '@angular/core';

@Injectable()
export class DealNotificationTapHandler implements NotificationTapHandler {
  constructor(private router: Router) {}

  handle(data: any) {
    const url = '/visits/details/' + data.deal;
    this.router.navigateByUrl(url);
  }
}
