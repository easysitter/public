import {Query, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';


import { NannyRequest } from '@es/requests';

interface Response {
  request: NannyRequest
}

@Injectable({
  providedIn: 'root'
})
export class RequestQuery extends Query<Response> {
  readonly document = gql`query request($id: String!){
    request(id: $id) {
      id
      status
      rate
      request {
        id
        date
        fromTime
        toTime
        children
        wishes
        trial
        address
        district {
          id
          title
          isOwn
        }
        parent {
          id
          name
          photo
        }
      }
    }
  }`;
}
