import {Subscription, gql} from 'apollo-angular';


import { ParentRequest } from '@es/requests';
import { Injectable } from '@angular/core';

interface Result {
  requestsStatusUpdate: { request: { id: ParentRequest['id'] } }
}

@Injectable({
  providedIn: 'root'
})
export class RequestStatusUpdates extends Subscription<Result> {
  document = gql`
    subscription requestsStatusUpdate{
      requestsStatusUpdate {
        id
        status
        rate
        request {
          id
          date
          fromTime
          toTime
          children
          trial
          district {
            id
            title
            isOwn
          }
          parent {
            id
            name
            photo
          }
        }
      }
    }
  `;
}
