import { Injectable } from "@angular/core";
import { interval, Observable } from "rxjs";
import { map, pluck, shareReplay, switchMap } from "rxjs/operators";

import { NannyRequest, NannyRequestStatus } from "@es/requests";

import { RequestService } from "./requests.service";

import { RequestsQuery } from "./requests.query";
import { RequestQuery } from "./request.query";
import { RequestStatusUpdates } from "./request-status-updates";
import { formatTime } from "@es/custom-date";

function handleRequestUpdate(
  prev: { requests: NannyRequest[] },
  subscriptionData: { data: { requestsStatusUpdate: NannyRequest } }
) {
  let next = prev;
  const { data } = subscriptionData;
  if (!data) {
    return next;
  }
  const updatedRequest = data.requestsStatusUpdate;
  const isNew = updatedRequest.status === "new";
  if (isNew) {
    next = {
      ...prev,
      requests: [
        updatedRequest,
        ...prev.requests
      ]
    };
  }
  return next;
}

@Injectable()
export class GqlRequestsService extends RequestService {
  requestsQueryRef = this.requestsQuery.watch();
  requests$ = this.requestsQueryRef.valueChanges.pipe(
    pluck("data", "requests"),
    map(requests => requests.map((nRequest) => {
      return {
        ...nRequest,
        request: {
          ...nRequest.request,
          fromTime: formatTime(nRequest.request.fromTime, nRequest.request.date),
          toTime: formatTime(nRequest.request.toTime, nRequest.request.date)
        }
      };
    }))
  );
  private statusUpdates;//: Observable<ParentRequest['id']>;

  constructor(
    private requestsQuery: RequestsQuery,
    private requestQuery: RequestQuery,
    private statusUpdatesSubscription: RequestStatusUpdates
  ) {
    super();
    interval(10000)
      .pipe(
        switchMap(() => this.requestsQueryRef.refetch())
      )
      .subscribe();
    this.statusUpdates = this.statusUpdatesSubscription
      .subscribe()
      .pipe(
        pluck("data", "requestsStatusUpdate"),
        shareReplay({ bufferSize: 1, refCount: true })
      );
    this.requestsQueryRef.subscribeToMore<{ requestsStatusUpdate: NannyRequest }>({
      document: this.statusUpdatesSubscription.document,
      updateQuery: (
        prev,
        { subscriptionData }
      ) => handleRequestUpdate(prev, subscriptionData)
    });
  }

  getConversations(): Observable<NannyRequest[]> {
    return this.requests$
      .pipe(
        map(
          requests => requests.filter(
            request => ![NannyRequestStatus.New, NannyRequestStatus.Closed].includes(request.status)
          )
        )
      );
  }

  getRequestDetails(id: any): Observable<NannyRequest> {
    const queryRef = this.requestQuery.watch({ id });
    queryRef.subscribeToMore({
      document: this.statusUpdatesSubscription.document
    });
    return queryRef.valueChanges.pipe(
      pluck("data", "request"),
      map((nRequest) => {
        return {
          ...nRequest,
          request: {
            ...nRequest.request,
            fromTime: formatTime(nRequest.request.fromTime, nRequest.request.date),
            toTime: formatTime(nRequest.request.toTime, nRequest.request.date)
          }
        };
      })
    );
  }

  getRequests(): Observable<NannyRequest[]> {
    return this.requests$.pipe(map(requests => requests.filter(request => request.status === "new")));
  }
}
