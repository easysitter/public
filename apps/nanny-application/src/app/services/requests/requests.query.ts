import {Query, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';


import { NannyRequest } from '@es/requests';

interface RequestsResponse {
  requests: NannyRequest[]
}

@Injectable({
  providedIn: 'root'
})
export class RequestsQuery extends Query<RequestsResponse> {
  readonly document = gql`query requests{
    requests {
      id
      status
      rate
      request {
        id
        date
        fromTime
        toTime
        children
        trial
        district {
          id
          title
          isOwn
        }
        parent {
          id
          name
          photo
        }
      }
    }
  }`;
}
