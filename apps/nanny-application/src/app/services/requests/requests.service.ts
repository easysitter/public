import { Observable } from 'rxjs';
import { NannyRequest } from '@es/requests';

export abstract class RequestService {
  abstract getRequests(): Observable<NannyRequest[]>;

  abstract getConversations(): Observable<NannyRequest[]>;

  abstract getRequestDetails(id: NannyRequest['id']): Observable<NannyRequest>;

}
