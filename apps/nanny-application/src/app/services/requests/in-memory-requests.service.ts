import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError, timer } from 'rxjs';
import { RequestService } from './requests.service';
import { switchMap, tap } from 'rxjs/operators';
import { filterArray } from '../../utils/operators';
import { NannyRequest, NannyRequestStatus } from '@es/requests';

const wishes = '<p>У нас двое детей: Саша - 1,5 года и Андрей - 4 года.</p>\n' +
  '\n' +
  '    <p>Саша: аллергия на кошек! Любит смотреть мультики, ест детские смеси. Засыпает только рядом с маминой\n' +
  '      кофтой.</p>\n' +
  '\n' +
  '    <p>Андрей: любит слушать музыку, гулять и танцевать. Ест супы (все практически). Засыпает сам после еды.</p>\n' +
  '\n' +
  '    <p>\n' +
  '      13:00 - дневной сон<br>\n' +
  '      14:00 - прогулка.<br>\n' +
  '      15:00 - перекус<br>\n' +
  '      17:00 - прогулка<br>\n' +
  '      18:00 - ужин<br>\n' +
  '      21:00 - вечерний сон\n' +
  '    </p>\n';
const requests: NannyRequest[] = [{
  id: '0000-1-asd',
  rate: 9,
  request: {
    id: 1,
    trial: true,
    date: new Date('2019-10-01'),
    fromTime: '8:00',
    toTime: '10:00',
    parent: {
      id: 1,
      name: 'Xela Kalginu',
      photo: 'assets/images/funny-bday-cat.jpg'
    },
    address: '',
    children: [1, 2, 5, 0],
    wishes
  },
  status: NannyRequestStatus.New
}, {
  id: '0000-2-asd',
  rate: 9,
  request: {
    id: 2,
    trial: false,
    date: new Date('2019-10-01'),
    fromTime: '8:00',
    toTime: '10:00',
    parent: {
      id: 1,
      name: 'Elxa Kagluni',
      photo: 'assets/images/funny-bday-cat.jpg'
    },
    address: '',
    children: [1, 2],
    wishes
  },
  status: NannyRequestStatus.Accepted
}, {
  id: '0000-3-asd',
  rate: 9,
  request: {
    id: 3,
    trial: false,
    date: new Date('2019-10-01'),
    fromTime: '8:00',
    toTime: '10:00',
    parent: {
      id: 1,
      name: 'Xael Kinluga',
      photo: 'assets/images/funny-bday-cat.jpg'
    },
    address: '',
    children: [1, 3],
    wishes
  },
  status: NannyRequestStatus.New
}];

@Injectable({
  providedIn: 'root'
})
export class InMemoryRequestsService implements RequestService {
  private requests = new BehaviorSubject<NannyRequest[]>(requests);

  accept(request: NannyRequest): Observable<NannyRequest> {
    return this.changeStatus(request.id, NannyRequestStatus.Accepted);
  }

  createOffer(request: NannyRequest): Observable<NannyRequest> {
    return undefined;
  }

  getRequestDetails(id: NannyRequest['id']): Observable<NannyRequest> {
    return this.requests.pipe(
      filterArray(r => r.id === id),
      switchMap(arr => arr.length ? of(arr[0]) : throwError(new Error('not found')))
    );
  }

  getRequests(): Observable<NannyRequest[]> {
    return this.requests.pipe(
      filterArray(r => r.status === 'new')
    );
  }

  getConversations(): Observable<NannyRequest[]> {
    return this.requests.pipe(
      filterArray(r => r.status === 'accepted')
    );
  }

  refuse(request: NannyRequest): Observable<NannyRequest> {
    return this.changeStatus(request.id, NannyRequestStatus.Closed);
  }

  private changeStatus(id: NannyRequest['id'], status: NannyRequest['status']) {
    const currentRequests = this.requests.getValue().map(
      r => {
        if (r.id === id) {
          return { ...r, status } as NannyRequest;
        }
        return r;
      }
    );
    return timer(Math.random() * 300).pipe(
      tap(() => this.requests.next(currentRequests)),
      switchMap(() => this.getRequestDetails(id))
    );
  }
}

