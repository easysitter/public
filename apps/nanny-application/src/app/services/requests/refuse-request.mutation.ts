import {Mutation, gql} from 'apollo-angular';


import { Injectable } from '@angular/core';
import { NannyRequest } from '@es/requests';

interface Response {
  refuseRequest: Pick<NannyRequest, 'id' | 'status'> & { __typename: 'NannyRequest' }
}

@Injectable({
  providedIn: 'root'
})
export class RefuseRequestMutation extends Mutation<Response> {
  readonly document = gql`mutation refuseRequest($id: ID) {
    refuseRequest(id: $id) {
      id
      status
    }
  }`;
}
