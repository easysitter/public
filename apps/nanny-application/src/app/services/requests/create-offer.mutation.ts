import {Mutation, gql} from 'apollo-angular';


import { Injectable } from '@angular/core';

interface Response {
  createOffer: any
}

@Injectable({
  providedIn: 'root'
})
export class CreateOffer extends Mutation<Response> {
  readonly document = gql`mutation createOffer($offer: CreateOfferInput) {
    createOffer(offer: $offer) {
      id
      date
      fromTime
      toTime
      trial
      rate
      offerActionRequired
    }
  }`;
}
