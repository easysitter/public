import {Mutation, gql} from 'apollo-angular';


import { Injectable } from '@angular/core';
import { NannyRequest } from '@es/requests';

interface Response {
  acceptRequest: Pick<NannyRequest, 'id' | 'status'> & { __typename: 'NannyRequest' }
}

@Injectable({
  providedIn: 'root'
})
export class AcceptRequestMutation extends Mutation<Response> {
  readonly document = gql`mutation acceptRequest($id: ID) {
    acceptRequest(id: $id) {
      id
      status
    }
  }`;
}
