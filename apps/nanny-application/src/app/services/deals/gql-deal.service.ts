import {Apollo} from 'apollo-angular';
import { DealsQuery } from "./deals.query";
import { interval, merge, Observable } from "rxjs";
import { Deal } from "../../models/deal";
import { DealService } from "./deal.service";
import { Injectable } from "@angular/core";
import { filter, map, pluck, shareReplay, switchMap } from "rxjs/operators";

import {
  DEAL_QUERY,
  DEAL_UPDATES_SUBSCRIPTION,
  FINISH_VISIT_MUTATION,
  START_VISIT_MUTATION,
  SUBMIT_REVIEW
} from "./deal-gql-definistions";
import { AuthenticationService } from "@es/authentication";
import { formatTime } from "@es/custom-date";

@Injectable()
export class GqlDealService extends DealService {
  private activeQueryRef = this.query({ isActive: true });
  private active = this.auth.isLoggedIn().pipe(
    filter(loggedIn => !!loggedIn),
    switchMap(() => this.activeQueryRef.valueChanges),
    pluck("data", "deals"),
    map(deals => deals.map(({ fromTime, toTime, ...rest }) => {
      return {
        ...rest,
        fromTime: formatTime(fromTime, rest.date),
        toTime: formatTime(toTime, rest.date)
      };
    }))
  );

  private inactiveQueryRef = this.query({ isActive: false });
  private inactiveQueryValues = this.auth.isLoggedIn().pipe(
    filter(loggedIn => !!loggedIn),
    switchMap(() => this.inactiveQueryRef.valueChanges)
  );

  private pendingReview = this.inactiveQueryValues
    .pipe(
      pluck("data", "deals"),
      map(deals => deals.filter(deal => !deal.review && !["suspended", "cancelled"].includes(deal.tag))),
      map(deals => deals.map(({ fromTime, toTime, ...rest }) => {
        return {
          ...rest,
          fromTime: formatTime(fromTime, rest.date),
          toTime: formatTime(toTime, rest.date)
        };
      })),
      shareReplay({ refCount: true, bufferSize: 1 })
    );
  private closed = this.inactiveQueryValues
    .pipe(
      pluck("data", "deals"),
      map(deals => deals.filter(deal => !!deal.review || ["suspended", "cancelled"].includes(deal.tag))),
      map(deals => deals.map(({ fromTime, toTime, ...rest }) => {
        return {
          ...rest,
          fromTime: formatTime(fromTime, rest.date),
          toTime: formatTime(toTime, rest.date)
        };
      })),
      shareReplay({ refCount: true, bufferSize: 1 })
    );

  constructor(
    private apollo: Apollo,
    private dealsQuery: DealsQuery,
    private auth: AuthenticationService
  ) {
    super();

    const refetchActive = interval(10000).pipe(
      switchMap(() => this.activeQueryRef.refetch())
    );

    const refetchInactive = interval(10000).pipe(
      switchMap(() => this.inactiveQueryRef.refetch())
    );
    const updatesSubscription = this.apollo.subscribe({
      query: DEAL_UPDATES_SUBSCRIPTION
    });

    merge(refetchActive, refetchInactive, updatesSubscription).subscribe();
    this.activeQueryRef.subscribeToMore<{ dealStatusUpdate: Deal }>({
      document: DEAL_UPDATES_SUBSCRIPTION,
      updateQuery: (
        previousQueryResult,
        { subscriptionData }
      ) => {
        return handleDealUpdate(previousQueryResult, subscriptionData);
      }
    });
  }

  getActive(): Observable<Deal[]> {
    return this.active;
  }

  getHistory(): Observable<Deal[]> {
    return this.closed;
  }

  getPendingReview(): Observable<Deal[]> {
    return this.pendingReview;
  }

  load(id): Observable<Deal> {
    const queryRef = this.apollo.watchQuery<{ deal: Deal }>({
      query: DEAL_QUERY,
      variables: { id }
    });
    queryRef.subscribeToMore({
      document: DEAL_UPDATES_SUBSCRIPTION
    });
    return queryRef.valueChanges.pipe(
      pluck("data", "deal"),
      map(({ fromTime, toTime, ...rest }) => {
        return {
          ...rest,
          fromTime: formatTime(fromTime, rest.date),
          toTime: formatTime(toTime, rest.date)
        };
      })
    );
  }

  startVisit(id): Observable<any> {
    return this.apollo.mutate({
      mutation: START_VISIT_MUTATION,
      variables: {
        id
      },
      optimisticResponse: optimisticResponseFor("startVisit", {
        id: id,
        startedAt: new Date()
      })
    }).pipe(
      pluck("data", "startVisit")
    );
  }

  finishVisit(deal: Deal): Observable<any> {
    return this.apollo.mutate({
      mutation: FINISH_VISIT_MUTATION,
      variables: {
        id: deal.id
      },
      optimisticResponse: optimisticResponseFor("finishVisit", {
        id: deal.id,
        startedAt: deal.startedAt,
        finishedAt: new Date(),
        isActive: false
      })
    }).pipe(
      pluck("data", "finishVisit")
    );
  }

  submitReview(deal, review): Observable<any> {
    return this.apollo.mutate({
      mutation: SUBMIT_REVIEW,
      variables: {
        deal, review
      },
      optimisticResponse: optimisticResponseFor("submitReview", {
        id: deal,
        review: {
          __typename: "Review",
          id: `${Date.now()}`,
          ...review
        }
      })
    }).pipe(
      pluck("data", "submitReview")
    );
  }


  private query(params: { isActive: boolean }) {
    const queryRef = this.dealsQuery.watch({
      filter: {
        isActive: params.isActive
      }
    });

    queryRef.subscribeToMore({
      document: DEAL_UPDATES_SUBSCRIPTION
    });
    return queryRef;
  }

}

function optimisticResponseFor(mutation, value) {
  return {
    __typename: "Mutation",
    [mutation]: {
      __typename: "Deal",
      ...value
    }
  };
}

function handleDealUpdate(
  prev: { deals: Deal[] },
  subscriptionData: { data: { dealStatusUpdate: Deal } }
) {
  let next = prev;
  const { data } = subscriptionData;
  if (!data) {
    return next;
  }
  const updatedRequest = data.dealStatusUpdate;
  const isNew = updatedRequest.state === "new";
  if (isNew) {
    next = {
      ...prev,
      deals: [
        updatedRequest,
        ...prev.deals
      ]
    };
  }
  return next;
}
