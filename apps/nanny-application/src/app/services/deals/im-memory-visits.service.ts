import { EMPTY, Observable, of } from 'rxjs';
import { Deal } from '../../models/deal';
import { DealService } from './deal.service';
import { filterArray } from '../../utils/operators';
import { shareReplay } from 'rxjs/operators';
import { Injectable } from '@angular/core';

const visits: any[] = [{
  id: 1,
  date: new Date('2019-10-11'),
  fromTime: '12:00',
  toTime: '15:00',
  rate: 0,
  parent: {
    id: 1,
    name: 'Alex Nigaluk',
    photo: 'assets/images/funny-bday-cat.jpg'
  },
  children: [1, 2, 3],
  wishes: '',
  address: '',
  trial: false,
  isActive: true,
  state: 'new'
}, {
  id: 2,
  date: new Date('2019-10-11'),
  fromTime: '12:00',
  toTime: '15:00',
  rate: 0,
  parent: {
    id: 1,
    name: 'Alex Nigaluk',
    photo: 'assets/images/funny-bday-cat.jpg'
  },
  children: [4],
  wishes: '',
  address: '',
  trial: false,
  isActive: true,
  state: 'new'
}, {
  id: 3,
  date: new Date('2019-10-11'),
  fromTime: '12:00',
  toTime: '15:00',
  rate: 0,
  parent: {
    id: 1,
    name: 'Alex Nigaluk',
    photo: 'assets/images/funny-bday-cat.jpg'
  },
  children: [4],
  wishes: '',
  address: '',
  trial: false,
  isActive: false,
  state: 'pending review'
}, {
  id: 4,
  date: new Date('2019-10-11'),
  fromTime: '12:00',
  toTime: '15:00',
  rate: 0,
  parent: {
    id: 1,
    name: 'Alex Nigaluk',
    photo: 'assets/images/funny-bday-cat.jpg'
  },
  children: [4],
  wishes: '',
  address: '',
  trial: false,
  isActive: false,
  state: 'closed'
}];

@Injectable()
export class InMemoryDealService extends DealService {
  private visits$ = of(visits);
  private active$ = this.visits$.pipe(
    filterArray(v => v.isActive),
    shareReplay(1)
  );

  private history$: Observable<Deal[]> = this.visits$.pipe(
    filterArray(v => !v.isActive && v.state === 'closed'),
    shareReplay(1)
  );

  private pendingReview$: Observable<Deal[]> = this.visits$.pipe(
    filterArray(v => !v.isActive && v.state === 'pending review'),
    shareReplay(1)
  );

  getActive(): Observable<Deal[]> {
    return this.active$;
  }


  getHistory(): Observable<Deal[]> {
    return this.history$;
  }

  getPendingReview(): Observable<Deal[]> {
    return this.pendingReview$;
  }

  accept(offer): Observable<any> {
    return EMPTY;
  }

  refuse(offer): Observable<any> {
    return EMPTY;
  }

  load(id): Observable<Deal> {
    return EMPTY;
  }

  finishVisit(id): Observable<any> {
    return EMPTY;
  }

  startVisit(id): Observable<any> {
    return EMPTY;
  }

  submitReview(deal: number, review: any): Observable<any> {
    return EMPTY;
  }

}
