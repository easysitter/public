import {Query, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';


import { Deal } from '../../models/deal';
import { DEAL_DYNAMIC_FRAGMENT, DEAL_STATIC_FRAGMENT } from './deal-gql-definistions';

interface Response {
  deals: Deal[]
}

@Injectable({
  providedIn: 'root'
})
export class DealsQuery extends Query<Response> {
  readonly document = gql`
    query deals($filter: DealsQueryInput!){
      deals(filter: $filter) {
        ...DealStatic
        ...DealDynamic
      }
    }
    ${DEAL_STATIC_FRAGMENT}
    ${DEAL_DYNAMIC_FRAGMENT}
  `;
}
