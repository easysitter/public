import {gql} from 'apollo-angular';


export const DEAL_STATIC_FRAGMENT = gql`
  fragment DealStatic on Deal {
    id
    date
    fromTime
    toTime
    rate
    address
    parent {
      name
      photo
      id
    }
    children
    wishes
  }
`;
export const DEAL_DYNAMIC_FRAGMENT = gql`
  fragment DealDynamic on Deal {
    startedAt
    finishedAt
    isActive
    tag
    state
    review {
      id
      rating
      pros
      cons
    }
  }
`;


export const SUBMIT_REVIEW = gql`
  mutation submitReview ($deal:ID, $review: ReviewInput){
    submitReview(deal: $deal, review: $review){
      id
      review {
        id
        rating
        pros
        cons
      }
    }
  }
`;

export const DEAL_UPDATES_SUBSCRIPTION = gql`
  subscription dealUpdate {
    dealStatusUpdate {
      id
      ...DealStatic
      ...DealDynamic
    }
  }
  ${DEAL_DYNAMIC_FRAGMENT}
  ${DEAL_STATIC_FRAGMENT}
`;
export const FINISH_VISIT_MUTATION = gql`
  mutation finishVisit($id: ID) {
    finishVisit(id: $id) {
      id
      startedAt
      finishedAt
      isActive
    }
  }
`;
export const START_VISIT_MUTATION = gql`
  mutation startVisit($id: ID) {
    startVisit(id: $id) {
      id
      startedAt
    }
  }
`;
export const DEAL_QUERY = gql`
  query deal($id: ID){
    deal(id: $id) {
      ...DealStatic
      ...DealDynamic
    }
  }
  ${DEAL_STATIC_FRAGMENT}
  ${DEAL_DYNAMIC_FRAGMENT}
`;
