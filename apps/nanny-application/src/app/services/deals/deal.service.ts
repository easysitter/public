import { Observable } from 'rxjs';
import { Deal } from '../../models/deal';

export abstract class DealService {
  abstract getActive(): Observable<Deal[]>

  abstract getPendingReview(): Observable<Deal[]>;

  abstract getHistory(): Observable<Deal[]>;

  abstract load(id): Observable<Deal>

  abstract startVisit(id): Observable<any>

  abstract finishVisit(id): Observable<any>

  abstract submitReview(deal: Deal['id'], review: any): Observable<any>
}


