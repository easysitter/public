import {Apollo, gql} from 'apollo-angular';
import { Injectable } from '@angular/core';


import { pluck } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ParentProfileService {

  constructor(
    private apollo: Apollo
  ) {
  }

  load(id) {
    return this.apollo.query<{ parent: ParentProfile }>({
      query: PARENT_PROFILE_QUERY,
      variables: { id }
    }).pipe(
      pluck('data', 'parent')
    );
  }
}

export interface ParentProfile {
  id: number;
  name: string;
  photo: string;
  children: number[];
  description: string;
}

const PARENT_PROFILE_QUERY = gql`
  query parent($id: ID) {
    parent(id: $id) {
      id
      name
      photo
      children
      description
    }
  }
`;

