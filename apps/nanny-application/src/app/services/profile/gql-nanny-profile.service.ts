import {Apollo, gql} from 'apollo-angular';
import { NannyProfile, NannyProfileService } from '@es/nanny';
import { EMPTY, Observable } from 'rxjs';


import { catchError, filter, map, pluck, retry, shareReplay, switchMap } from 'rxjs/operators';
import { AuthenticationService } from '@es/authentication';

const NANNY_PROFILE_QUERY = gql`query me{
  me {
    profile {
      id
      name
      birthDate
      phone
      photo
      description
      districts
      schedule {
        day
        from
        to
      }
      price {
        value
        currency
      }
      trial {
        value
        currency
      }
      trialEnabled
      ageCategories
      maxChildrenCount
      documents
      features
    }
  }
}`;

export class GqlNannyProfileService extends NannyProfileService {
  private profile = this.auth.isLoggedIn().pipe(
    filter(loggedIn => !!loggedIn),
    switchMap(() => this.apollo
      .watchQuery<{ me: { profile: NannyProfile & { documents: string, features: string } } }>({
        query: NANNY_PROFILE_QUERY
      })
      .valueChanges
      .pipe(
        pluck('data', 'me', 'profile'),
        map(({ documents, features, ...profile }) => ({
          ...profile,
          documents: JSON.parse(documents) as Record<string, string>,
          features: JSON.parse(features) as Record<string, string>
        })),
        retry(3),
        catchError(err => EMPTY)
      )
    ),
    shareReplay({ bufferSize: 1, refCount: false })
  );

  constructor(
    private apollo: Apollo,
    private auth: AuthenticationService
  ) {
    super();
  }

  load(): Observable<NannyProfile> {
    return this.profile;
  }

  save({
         documents,
         features,
         ...data
       }: Partial<NannyProfile>): Observable<any> {
    return this.apollo.mutate<{ saveProfile: Partial<NannyProfile> }>({
      mutation: gql`mutation saveProfile($profile: NannyProfileInput!){
        saveProfile(profile: $profile) {
          id
          name
          birthDate
          phone
          photo
          description
          districts
          schedule {
            day
            from
            to
          }
          price {
            value
            currency
          }
          trial {
            value
            currency
          }
          trialEnabled
          ageCategories
          maxChildrenCount
          documents
          features
        }
      }`,
      variables: {
        profile: {
          ...data,
          documents: JSON.stringify(documents),
          features: JSON.stringify(features)
        }
      }
    }).pipe(pluck('data'));
  }
}
