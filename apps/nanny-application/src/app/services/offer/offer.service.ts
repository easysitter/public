import { Observable } from 'rxjs';


class OfferDetails {
  date: Date;
  fromTime: string;
  toTime: string;
  children: number[];

  rate: number;
  trial: boolean;
  safePayment: boolean;

  wishes: string;
  address: string;
}

export class OfferDto {
  requestId: string;
  trackingId?: string;
  parent: number;
  details: OfferDetails;
}

export abstract class OfferService {
  abstract create(data: OfferDto): Observable<void>
}
