import { OfferDto, OfferService } from './offer.service';
import { BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class InMemoryOfferService extends OfferService {
  offers$ = new BehaviorSubject([]);

  create(data: OfferDto): Observable<void> {
    this.offers$.next(this.offers$.getValue().concat([data]));
    return EMPTY;
  }
}
