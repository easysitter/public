import { ErrorHandler, Injectable } from '@angular/core';
import * as Sentry from '@sentry/browser';
import { environment } from '../../environments/environment';

if (environment.production) {
  Sentry.init({
    dsn: 'https://b1b317c31799458699c8c4c79e1e0285@o388284.ingest.sentry.io/5225488'
  });
}

@Injectable()
export class SentryErrorHandler extends ErrorHandler {
  handleError(error: any): void {
    if (environment.production) {
      const eventId = Sentry.captureException(error.originalError || error);
    }
    super.handleError(error);
    // Sentry.showReportDialog({ eventId });
  }
}
