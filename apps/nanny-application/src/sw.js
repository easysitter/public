try {
  importScripts('./ngsw-worker.js');
} catch (e) {
  // console.error('Failed to load ngsw');
}
self.addEventListener('notificationclick', event => {
  /**
   * @var notification Notification
   */
  const notification = event.notification;
  notification.close();
  console.log('This is custom service worker notificationclick method.');
  console.log('Notification details: ', notification);
  // Write the code to open
  if (clients.openWindow) {
    let url;
    try {
      switch (notification.data.type) {
        case 'chat-message':
          const { chat_type, request, chat_users } = notification.data;
          if (chat_type === 'direct') {
            let user = chat_users.split(':').find(id => id.startsWith('p'));
            if (user) {
              user = user.replace(/^p/i, '');
            }
            url = '/parent/' + user + '/chat';
            break;
          }
          url = '/conversations/' + request + '/chat';
          break;
        case 'new-request':
          url = '/request/' + notification.data.request;
          break;
        case 'new-offer':
          url = '/request/' + notification.data.request;
          break;
        case 'new-deal':
          url = '/visits/details/' + notification.data.deal;
          break;
        case 'deal-reminder':
          url = '/visits/details/' + notification.data.deal;
          break;
        case 'videochat':
          url = '/videochat?id=' + notification.data.sessionId;
          break;
        default:
          url = notification.data.url || '/requests';
      }
    } catch (e) {

    }
    event.waitUntil(clients.openWindow(url));
  }
});
