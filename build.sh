#!/usr/bin/env bash

#!/usr/bin/env bash
declare -a services=(
  "nanny-public"
  "parent-public"
  "nanny-webapi"
  "parent-webapi"
)
CMD="echo Building services"
COMMIT_ID=$(git rev-parse --short HEAD)

for service in "${services[@]}"; do
  DOCKER_TAG_LATEST="registry.gitlab.com/easysitter/public/${service}:latest"
  DOCKER_TAG_COMMIT="registry.gitlab.com/easysitter/public/${service}:${COMMIT_ID}"
  CMD="${CMD} && docker build --file ${service}.dockerfile -t ${DOCKER_TAG_LATEST} -t ${DOCKER_TAG_COMMIT} ."
  CMD="${CMD} && docker push $DOCKER_TAG_LATEST"
  CMD="${CMD} && docker push $DOCKER_TAG_COMMIT"
done

CMD="${CMD} && kubectl.exe delete deployment -l tier=backend-for-frontend -n easysitter"
CMD="${CMD} && kubectl.exe delete deployment -l tier=frontend -n easysitter"
CMD="${CMD} && cat configs/deployments.yaml | kubectl.exe apply -f -"

eval $CMD
